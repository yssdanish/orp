import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../../shared';
import { PasswordResetInitComponent } from './password-reset-init.component';

export const passwordResetInitRoute: Route = {
    path: 'password/reset',
    component: PasswordResetInitComponent,
    data: {
        authorities: [],
        pageTitle: 'Border Roads Organization'
    },
    canActivate: [UserRouteAccessService]
};
