import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './candidate/login/login.component';
import { NewCandidateRegistrationComponent } from './candidate/new-candidate-registration/new-candidate-registration.component';
import { RegistrationsuccessComponent } from './candidate/registrationsuccess/registrationsuccess.component';
import { CandidateRegisterComponent } from './candidate/candidate-register/candidate-register.component';
import { PreviewComponent } from './candidate/preview/preview.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './home/pageNotFound';
import { JobApplyComponent } from './job-apply/job-apply.component';
import { ContactComponent } from './contact/contact.component';
import { ResultComponent } from './candidate/result/result.component';
import { PaymentComponent } from './candidate/payment/payment.component';
import { AboutComponent } from './about/about.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { JobPostComponent } from './admin/job-post/job-post.component';
import { JobFieldComponent } from './admin/job-field/job-field.component';
import { CandidateDetailsComponent } from './admin/candidate-details/candidate-details.component';
import { ReportComponent } from './admin/report/report.component';
import { ExamComponent } from './admin/exam/exam.component';
import { CenterComponent } from './admin/center/center.component';
import { AdminRegisterComponent } from './admin/admin-register/admin-register.component';
import { admitcardComponent } from './admin/admit-card/admit-card.component';
import { UserRouteAccessService } from '../app/shared';
import { CenterAllocationComponent } from './admin/center-allocation/center-allocation.component';
import { ScannerComponent } from './admin/scanner/scanner.component';
import { BordmemberComponent } from './admin/bordmember/bordmember.component';
import { CandidateQualificationComponent } from './candidate/candidate-qualification/candidate-qualification.component';

const appRoutes: Routes = [
   
    {
        path: 'registation/success', component: RegistrationsuccessComponent
    },
    
    {
        path: 'newcandidate/registration', component: NewCandidateRegistrationComponent
    
    },

    {
        path: 'candidate/login', component: LoginComponent,
        data: {
            authorities: []
        }

    }, {
        path: 'candidate/preview', component: PreviewComponent,
        data: {
            authorities: ['ROLE_USER']
        },
        children: [{
            path: 'candidate/payment', component: PaymentComponent,
        }
        ],
        canActivate: [UserRouteAccessService],
    },
    {
        path: 'candidate/result', component: ResultComponent,
        data: {
            authorities: []
        }, canActivate: [UserRouteAccessService],
    },
    // {
    //     path: 'candidate/register', component: CandidateRegisterComponent,
    //     data: {
    //         authorities: []
    //     }
    // },
    {
        path:'candidate/qualification',component: CandidateQualificationComponent,
        data:{
            authorities:[]
        }
      },
    {
        path: 'candidate/payment', component: PaymentComponent,
        data: {
            authorities: ['ROLE_USER']
        },
        children: [{
            path: 'candidate/payment', component: PaymentComponent,
        }
        ],
         canActivate: [UserRouteAccessService],
    },
    {
        path: 'admin/register', component: AdminRegisterComponent,
        data: {
            authorities: []
        }
    },

    {
        path: 'admin/admitcard', component: admitcardComponent,
        data: {
            authorities: []
        }
    },
    {
        path: 'admin/login', component: AdminLoginComponent,
        data: {
            authorities: []
        }
    },
    {
        path: 'admin/job/post', component: JobPostComponent,
        children: [{
            path: 'admin/job/fields', component: JobFieldComponent
        },
        {
            path: 'admin/job/report', component: ReportComponent,
        },
        {
            path: 'admin/job/marks', component: ExamComponent,
        },
        {
            path: 'admin/candidate/list', component: CandidateDetailsComponent,
        },
       
        ],
        data: {
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    },
    {
        path: 'admin/job/fields', component: JobFieldComponent,
        data: {
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    },
    {
        path: 'admin/job/report', component: ReportComponent,
        data: {
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    }, {
        path: 'admin/job/marks', component: ExamComponent,
        data: {
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    }, {
        path: 'admin/candidate/list', component: CandidateDetailsComponent,
        data: {
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    },

    {
        path: 'admin/create/center', component: CenterComponent,
        data: {
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    },

    {
        path: 'admin/allocate/center', component: CenterAllocationComponent,
        data: {
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    },{
        
       path: 'admin/candidate/scanner', component: ScannerComponent,
       
        data: {
         
            authorities: ['ROLE_ADMIN']
        }, canActivate: [UserRouteAccessService],
    },
    { path: 'admin/board/member', component: BordmemberComponent },
    
    { path: 'home', component: HomeComponent },

    { path: 'apply', component: JobApplyComponent },

    { path: 'about', component: AboutComponent },

    { path: 'contact', component: ContactComponent },
   

    { path: '', redirectTo: 'home', pathMatch: 'full' },

    { path: '**', component: PageNotFoundComponent },
    
{
    path: 'registation/success', component: RegistrationsuccessComponent
},

{
    path: 'newcandidate/registration', component: NewCandidateRegistrationComponent

},
]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });