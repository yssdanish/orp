import { Component, ViewChild, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { PostService, SharedService, GetService } from '../../shared/service/index';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})

export class ResultComponent implements OnInit {

  ngOnInit() {
  }
  public result: any = {};
  public sharingRegisterId: any;
  user: any = {};
  username: string;
  minDateDob = new Date(1992, 5, 10);
  maxDateDob = new Date();
  public personal = [];
  public registrationId: any;

  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = "theme-red";

  constructor(private sharedService: SharedService, private router: Router, private getService: GetService, ) {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    // this.resultForm = fb.group({
    //     nameControl: new FormControl('', Validators.required),
    //     dobControl: new FormControl('', Validators.required),
    //     fatherNameControl: new FormControl('', Validators.required),
    // });


  }


  serachResult(registrationId) {

    this.user.username = registrationId;
    this.getService.getPersonalDetails(this.user.username).subscribe(
      data => {
        console.log("getPersonalDetails :", data);
        console.log(" id", data.registrationId);
        this.sharingRegisterId = data.registrationId;
        this.gotopage();

      }, error => {

        this.router.navigate(['/accessdenied']);

      });


  }

  gotopage() {
    if (this.sharingRegisterId > 0) {
      this.sharedService.saveRegisterData(this.sharingRegisterId);

      this.router.navigate(['/candidate/preview']);

    }

  }


}
