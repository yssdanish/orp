import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCandidateRegistrationComponent } from './new-candidate-registration.component';

describe('NewCandidateRegistrationComponent', () => {
  let component: NewCandidateRegistrationComponent;
  let fixture: ComponentFixture<NewCandidateRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCandidateRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCandidateRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
