import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { ModalComponent } from '../../shared/model/shared-modal.component';
import { NgForm } from '@angular/forms'



import {
  FormsModule,
  FormControl,
  ReactiveFormsModule, FormGroup, Validators
} from '@angular/forms'


@Component({
  selector: 'app-new-candidate-registration',
  templateUrl: './new-candidate-registration.component.html',
  styleUrls: ['./new-candidate-registration.component.css']
})
export class NewCandidateRegistrationComponent implements OnInit {

  @ViewChild(ModalComponent) popUp: ModalComponent;

  minDate: Date;
  maxDate: Date;


  constructor(public postService: PostService,
    private getService: GetService,
    private decimalPipe: DecimalPipe, private route: ActivatedRoute,
    public sharedService: SharedService, private router: Router,
    private datePipe: DatePipe) {

    this.user.gender = 0;
    this.user.nationality = 0;
  }


  @ViewChild(NgForm) userForm:NgForm;

  public user: any = {};
  public calyear = {};

  public createSuccess: boolean = false;
  public loading = false;
  public createError: boolean;
  public nationcheck: boolean;
  public nationFileSize: boolean = false;
  public nationFileType: boolean = false;
  public nationRequired: boolean = false;

  ngOnInit() {
  }


  getAge(value) {

    var now = new Date(); //Todays Date   

    let birthday: any = this.datePipe.transform(value, 'yyyy-MM-dd');
    birthday = birthday.split('-');
    var dobyear = birthday[0];
    var dobmonth = birthday[1];
    var dobday = birthday[2];

    var nowyear = now.getFullYear();
    var nowmonth = now.getMonth() + 1;
    var nowday = now.getDay();

    var ageyear = nowyear - dobyear;
    var agemonth = nowmonth - dobmonth;
    var ageday = nowday - dobday;

    if (agemonth < 0) {
      ageyear--;
      agemonth = (12 + agemonth);
    }
    if (nowday < dobday) {
      agemonth--;
      ageday = 30 + ageday;
    }

    this.user.age = ageyear + " years, " + agemonth + " months, " + ageday + " days";
    this.calyear = ageyear;

    // console.log(this.user.age);
  }


  checkNation() {
    if (this.user.nationality.length > 5) {
      return this.nationcheck = true;
    } else {
      return this.nationcheck = false;
    }
  }


  nationfileChange(event) {
    this.nationFileType = false;
    this.nationFileSize = false;
    this.nationRequired = false;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      // this.nationRequired = true;
      let file: File = fileList[0];
      let fileType = file.type.split('/')[1];
      let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
      // this.user.nationCertificateDoc = file;
      if (25600 < file.size && file.size < 30720) {
        if (matchType) {
          this.user.nationCertificateDoc = file;
        }
        else {
          return this.nationFileType;
        }
      }
      else {
        return this.nationFileSize && this.nationRequired;
      }
    }

    return this.nationRequired = true;
  }


  newCandidateRegister() {
    let now = new Date().getTime().toString().slice(7);
    if (this.user.mobileNumber > 0) {
      let mobile = (this.user.mobileNumber).slice(6)
      this.user.registrationId = now + mobile;
      // console.log(this.user.registrationId);
    }

    let valuedob: any = this.user.dateOfBirth;
    let dob: any = this.datePipe.transform(valuedob, 'dd-MM-yyyy');
    var dobSplit = dob.split('-');


    this.user.dateOfBirth = dob;

    var sYear = dobSplit[2];
    var mob = this.user.mobileNumber;
    var gndr = this.user.gender;
    var natnlty = this.user.nationality;

    sYear = sYear.toString().substr(1, 2);
    mob = mob.toString().substr(1, 2);
    gndr = gndr.toString().substr(1, 2);
    natnlty = natnlty.toString().substr(1, 2);

    this.user.password = sYear + gndr + natnlty + mob;

    this.postService.newRegistration(this.user).subscribe(
      data => {
        console.log("Saved New Candidate data", data);
        if (data.status == 201) {
          // this.createSuccess = true;
          this.loading = false;
          setTimeout(() => {
            this.userForm.resetForm();
            this.router.navigate(['/registation/success']);
        }, 3000)
         

        }
        else{
          this.createError=true;
        }
      },
      error => {
        this.createSuccess = false;
        this.createError = true;
        this.loading = false;
        setTimeout(() => {
            this.createError=false;
            this.userForm.resetForm();
        }, 3000)
      });
  }



  public genders: any = [
    { id: 0, name: "Male", value: "Male" },
    { id: 1, name: "Female", value: "Female" },
  ];

  public nationalities: any = [
    { id: 0, name: "Indian" },
    { id: 1, name: "Nepali" }
  ];


}
