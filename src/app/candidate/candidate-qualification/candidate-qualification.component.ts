import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { ModalComponent } from '../../shared/model/shared-modal.component';

@Component({
    selector: 'app-candidate-qualification',
    templateUrl: './candidate-qualification.component.html',
    styleUrls: ['./candidate-qualification.component.css']
})
export class CandidateQualificationComponent implements OnInit {
    @ViewChild(ModalComponent) popUp: ModalComponent;






    public essentialDetails: boolean = false;
    public personalDetails: boolean = true;

    public allEducation: boolean = false;
    public allWeightage: boolean = false;
    public photoSign: boolean = false;

    public activePersonal: boolean = true;
    public activeEssential: boolean = false;
    public activeGraduation: boolean = false;
    public activeTechnical: boolean = false;
    public activeWeightage: boolean = false;
    public activeExperience: boolean = false;
    public activeDriving: boolean = false;
    public activeUpload: boolean = false;
    public activePreview: boolean = false;
    public activePayment: boolean = false;

    public adharCheck: boolean = true;
    public cplCheck: boolean = true;
    public communityCheck: boolean = true;
    public sameAddressCheck: boolean = false;
    public samePostalAddress: boolean = false;
    public exServiceCheck: boolean = true;
    public serviceCheck: boolean = true;
    public createError: boolean;


    public hindiStenographyCheck: boolean = true;
    public englishStenographyCheck: boolean = true;
    public nccBCheck: boolean = true;
    public nccCCheck: boolean = true;
    public sportsCheck: boolean = true;
    public degreeCheck: boolean = true;
    public itiCheck: boolean = true;
    public empExBroCheck: boolean = true;
    public broGREFCheck: boolean = true;
    public sonGREFCheck: boolean = true;
    public sonGPSCheck: boolean = true;
    public reapptCheck: boolean = true;
    public highGradeCheck: boolean = true;
    public interGradeCheck: boolean = true;
    public hmvLicenseCheck: boolean = true;
    public roadRollerCheck: boolean = true;
    public hindiTypingCheck: boolean = true;
    public englishTypingCheck: boolean = true;
    public highFileSize: boolean = false;
    public highFileType: boolean = false;
    public interFileSize: boolean = false;
    public interFileType: boolean = false;
    public gradFileSize: boolean = false;
    public gradFileType: boolean = false;
    public postGradFileType: boolean = false;
    public postGradFileSize: boolean = false;
    public techFileSize: boolean = false;
    public techFileType: boolean = false;
    public itiFileSize: boolean = false;
    public itiFileType: boolean = false;
    public armedFileType: boolean = false;
    public armedFileSize: boolean = false;

    public totalCheck: boolean = false;
    public obtainCheck: boolean = false;
    public intertotalCheck: boolean = false;
    public interobtainCheck: boolean = false;
    public photoSize: boolean = false;
    public photoType: boolean = false;
    public signSize: boolean = false;
    public signType: boolean = false;
    public expFileSize: boolean = false;
    public expFileType: boolean = false;

    public drivingFileType: boolean = false;
    public drivingFileSize: boolean = false;
    public licenseFileType: boolean = false;
    public licenseFileSize: boolean = false;

    public categoryFileSize: boolean = false;
    public categoryFileType: boolean = false;
    public nationFileSize: boolean = false;
    public nationFileType: boolean = false;
    public aadhaarFileSize: boolean = false;
    public aadhaarFileType: boolean = false;
    public cplFileSize: boolean = false;
    public cplFileType: boolean = false;
    public panFileSize: boolean = false;
    public panFileType: boolean = false;
    public centralFileSize: boolean = false;
    public centralFileType: boolean = false;
    public nccbFileSize: boolean = false;
    public nccbFileType: boolean = false;
    public ncccFileSize: boolean = false;
    public ncccFileType: boolean = false;
    public unitFileSize: boolean = false;
    public unitFileType: boolean = false;
    public lastUnitFileSize: boolean = false;
    public lastUnitFileType: boolean = false;
    public dischargeFileType: boolean = false;
    public dischargeFileSize: boolean = false;
    public sportsFileType: boolean = false;
    public sportsFileSize: boolean = false;
    public addFileType: boolean = false;
    public addFileSize: boolean = false;
    public aidFileType: boolean = false;
    public aidFileSize: boolean = false;


    public diplomaFileType: boolean = false;
    public diplomaFileSize: boolean = false;
    public error: string;
    public errorEmailExists: string;
    public errorUserExists: string;
    public success: boolean;
    public previewButton: boolean = true;

    public graduationDetails: boolean = false;
    public technicalDetails: boolean = false;
    public weightageDetails: boolean = false;
    public experienceDetails: boolean = false;
    public drivingDetails: boolean = false;
    public paymentDetails: boolean = false;
    public basicDetails: boolean = true;
    public finalSubmit: boolean = false;
    public uploadDetails: boolean = false;
    public nationRequired: boolean = false;
    public categoryRequired: boolean = false;
    public aadhaarfileRequired: boolean = false;
    public cplfileRequired: boolean = false;
    public panRequired: boolean = false;
    public centralfileRequired: boolean = false;
    public armedfileRequired: boolean = false;
    public nccbfileRequired: boolean = false;
    public nccfileRequired: boolean = false;
    public unitfileRequired: boolean = false;
    public lastUnitfileRequired: boolean = false;
    public sportsfileRequired: boolean = false;
    public dischargefileRequired: boolean = false;
    public highRequired: boolean = false;
    public interRequired: boolean = false;
    public additionalRequired: boolean = false;
    public firstAidRequired: boolean = false;

    public gradRequired: boolean = false;
    public postGradRequired: boolean = false;
    public degreeRequired: boolean = false;
    public itiRequired: boolean = false;
    public diplomaRequired: boolean = false;
    public photoRequired: boolean = false;
    public signRequired: boolean = false;
    public experienceRequired: boolean = false;
    public drivingRequired: boolean = false;
    public licenseRequired: boolean = false;
    public highObtainGreater: boolean = false;
    public interObtainGreater: boolean = false;
    public ugObtainGreater: boolean = false;
    public pgObtainGreater: boolean = false;
    public diplomaObtainGreater: boolean = false;
    public degreeObtainGreater: boolean = false;
    public itiObtainGreater: boolean = false;
    public emailExists: boolean = false;
    public createSuccess: boolean = false;
    public loading = false;
    public categoryCer: boolean;
    public nationcheck: boolean;
    public ageInvalid: boolean;
    public ageMessage: string;

    public invalidexperience: boolean;
    public experienceMessage: string;

    public invaliddlmv: boolean;
    public invalidroad: boolean;
    public invalidoeg: boolean;

    public dlmvMessage: string;
    public roadMessage: string;
    public oegMessage: string;

    public invaliddhmv: boolean;
    public dhmvMessage: string;

    public user: any = {};
    public states: any = [];
    public cities: any = [];
    public postalcities: any = [];
    public boards: any = [];
    public universities: any = [];
    public gradcourses: any = [];
    public postcourses: any = [];
    public dipcourses: any = [];
    public degcourses: any = [];
    public iticourses: any = [];
    public personals: any = [];
    public jobs: any;
    public formFields: any = {};
    public amountFields: any = {};
    public categories: any = [];
    public years: any = [];
    public relaxAge: any; // admin given relaxation
    public appliedAge: any;// candidate age -relaxation age
    public jobmaxAge: any; //apply max age by admin
    public jobminAge: any; // apply min age by admin
    public highMessage: string;
    public interMessage: string;
    public ugMessage: string;
    public pgMessage: string;
    public highPercentFlag: boolean;
    public interPercentFlag: boolean;
    public pgPercentFlag: boolean;
    public ugPercentFlag: boolean;
    public diplomaMessage: string;
    public degreeMessage: string;
    public itiMessage: string;
    public degreePercentFlag: boolean;
    public itiPercentFlag: boolean;
    public diplomaPercentFlag: boolean;

    public highNull: boolean;
    public interNull: boolean;
    public ugNull: boolean;
    public pgNull: boolean;
    public degreeNull: boolean;
    public itiNull: boolean;
    public diplomaNull: boolean;
    public hgmvNull: boolean;
    public lmvNull: boolean;
    public rrNull: boolean;
    public oegNull: boolean;
    public companyNull: boolean;
    public zoneNames: any = {};
    public zoneRequired: boolean;
    public zoneMessage: string;


    minDate: Date;
    maxDate: Date;

    minDateservedfrom = new Date(1992, 5, 10);
    maxDateservedfrom = new Date();
    minDateservedto = new Date(1992, 5, 10);
    maxDateservedto = new Date();

    minDatearmedfrom = new Date(1950, 0, 1);
    maxDatearmedfrom = new Date();
    minDatearmedto = new Date(1950, 0, 1);
    maxDatearmedto = new Date();

    minDateExempfrom = new Date(1950, 0, 1);
    maxDateExempfrom = new Date();
    minDateExempto = new Date(1950, 0, 1);
    maxDateExempto = new Date();


    minDateExpFrom = new Date(1992, 5, 10);
    maxDateExpFrom = new Date();
    minDateExpTO = new Date(1992, 5, 10);
    maxDateExpTo = new Date();

    minDateDlmvFrom = new Date(1992, 5, 10);
    maxDateDlmvFrom = new Date();
    minDateDlmvTO = new Date();
    maxDateDlmvTo = new Date(2100, 5, 12);

    minDateRrFrom = new Date(1992, 5, 10);
    maxDateRrFrom = new Date();
    minDateRrTo = new Date();
    maxDateRrTo = new Date(2100, 5, 12);

    minDateOegFrom = new Date(1992, 5, 10);
    maxDateOegFrom = new Date();
    minDateOegTO = new Date();
    maxDateOegTo = new Date(2100, 5, 12);

    minDateDhgmvFrom = new Date(1992, 5, 10);
    maxDateDhgmvFrom = new Date();
    minDateDhgmvTO = new Date();
    maxDateDhgmvTo = new Date(2100, 5, 12);

    public invalidDob: boolean;

    public invalidPercentage: boolean;

    bsConfig: Partial<BsDatepickerConfig>;
    colorTheme = "theme-red";
    public calyear = {};





    constructor(public postService: PostService,
        private getService: GetService,
        private decimalPipe: DecimalPipe, private route: ActivatedRoute,
        public sharedService: SharedService, private router: Router,
        private datePipe: DatePipe) {
        this.user.category = 0;
        this.user.communityName = 0;
        this.user.sportsLevel = 0;
        this.user.nationality = 0;
        this.user.maritalStatus = 0;
        this.user.permanentState = 0;
        this.user.permanentCity = 0;
        this.user.postalState = 0;
        this.user.postalCity = 0;
        this.user.gender = 0;
        this.user.highYear = 0;
        this.user.highBoard = 0;
        this.user.highCourseType = 0;
        this.user.interYear = 0;
        this.user.interCourseType = 0;
        this.user.interBoard = 0;
        this.user.ugCourse = 0;
        this.user.ugPassingYear = 0;
        this.user.ugUniversity = 0;
        this.user.ugCourseType = 0;
        this.user.pgCourse = 0;
        this.user.pgPassingYear = 0;
        this.user.pgUniversity = 0;
        this.user.pgCourseType = 0;
        this.user.diplomaBranch = 0;
        this.user.diplomaYear = 0;
        this.user.diplomaUniversity = 0;
        this.user.diplomaCourseType = 0;
        this.user.degreeBranch = 0;
        this.user.degreeUniversity = 0;
        this.user.degreeYear = 0;
        this.user.degreeCourseType = 0;
        this.user.itiBranch = 0;
        this.user.itiUniversity = 0;
        this.user.itiYear = 0;
        this.user.itiCourseType = 0;
        this.sharedService = sharedService;
        this.jobs = sharedService.getData();

        this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    }

    ngOnInit() {
        if (this.jobs == undefined) {
            this.popUp.getError("Please select job first");
        }
        this.getState();
        this.getUniversity();
        this.getBoard();
        this.getCourse();
        this.getPostCourse();
        this.getDiplomaCourse();
        this.getDegreeCourse();
        this.getItiCourse();
        this.getCategory();
        this.getYears();
        this.getFormFields();
        this.createSuccess = false;
    }


    personalDetail() {
        this.basicDetails = true;
        this.essentialDetails = false;
        this.allEducation = false;
        this.allWeightage = false;
        this.activePersonal = true;
        if (this.user.category == 0 || this.user.permanentState == 0 || this.user.permanentCity == 0) {
            return;
        }
        if (!this.samePostalAddress && (this.user.postalState == 0 || this.user.postalCity == 0)) {
            return;
        }
        if (!this.panRequired && !this.panFileType && !this.panFileSize) {
            return;
        }

        if (!this.communityCheck && this.user.communityName == 0) {
            return;
        }

        if (!this.cplCheck && !this.cplfileRequired && !this.cplFileType && !this.cplFileSize) {
            return;
        }

        for (let i = 0; i < this.amountFields.length; i++) {
            if (this.amountFields[i].categoryName == "SC") {

                if (this.user.category == "SC") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if ((finalYear < minValue || finalYear > maxValue)) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }

            if (this.amountFields[i].categoryName == "ST") {
                if (this.user.category == "ST") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if (finalYear < minValue || finalYear > maxValue) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }

            if (this.amountFields[i].categoryName == "OBC") {
                if (this.user.category == "OBC") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if (finalYear < minValue || finalYear > maxValue) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }
            if (this.amountFields[i].categoryName == "UR") {
                if (this.user.category == "General") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if (finalYear < minValue || finalYear > maxValue) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }
        }

        if (this.samePostalAddress) {
            this.user.postalAddress = this.user.permanentAddress;
            this.user.postalState = this.user.permanentState;
            this.user.postalCity = this.user.permanentCity;
            this.user.postalPincode = this.user.permanentPincode;
        }
        this.basicDetails = false;
        this.essentialDetails = true;
        this.allEducation = false;
        this.allWeightage = false;
        this.activePersonal = true;

    }


    essentialDetail() {

        this.basicDetails = false;
        this.essentialDetails = true;
        this.allEducation = false;
        this.allWeightage = false;
        this.activeEssential = true;

        if (this.formFields.highSchoolForm) {
            if (this.formFields.high.highCourseTypeView == true) {
                if (this.user.highCourseType == 0) {
                    return;
                }
            }
            if(this.formFields.high.highYearView == true)
            {
                if (this.user.highYear == 0) {
                    return;
    
                }
            }
       if(this.formFields.high.highBoard == true)
       {
        if (this.user.highBoard == 0) {
            return;
        }
    }
        if (!this.highRequired && !this.highFileType && !this.highFileSize && this.formFields.high.highCertificateView) {
            return;
        }



         this.highPercentFlag = false;
         this.interPercentFlag = false;
        if (this.formFields.high.highPercentageView) {
          if (this.user.highPercentage < this.formFields.high.highMinPercent) {
                 this.highPercentFlag = true;
                 this.highMessage = "Your are not fullfilling criteria";
                 return this.highPercentFlag;

          }
        }
    }

    if (this.formFields.intermediateForm) {
        
        if (this.formFields.inter.interYearView == true) {
            if (this.user.interYear == 0) {
                return;

            }
        }
        if (this.formFields.inter.interCourseTypeView == true) {
            if (this.user.interCourseType == 0) {
                return;
            }
        }
        if (this.formFields.inter.interBoardView == true) {
            if (this.user.interBoard == 0) {
                return;
            }
        }
        if (!this.interRequired && !this.interFileType && !this.interFileSize && this.formFields.inter.interCertificateView) {
            return;
        }

   if (this.formFields.inter.interPercentageView) {
        if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
            
            this.interPercentFlag = true;
            this.interMessage = "Your are not fullfilling criteria";
            
            return this.interPercentFlag;
        }

    }


this.allEducationDetail();}
else
{
    if (this.formFields.highSchoolForm) {
        if (this.formFields.high.highCourseTypeView == true) {
            if (this.user.highCourseType == 0) {
                return;
            }
        }
        if(this.formFields.high.highYearView == true)
        {
            if (this.user.highYear == 0) {
                return;

            }
        }
   if(this.formFields.high.highBoard == true)
   {
    if (this.user.highBoard == 0) {
        return;
    }
}
    if (!this.highRequired && !this.highFileType && !this.highFileSize && this.formFields.high.highCertificateView) {
        return;
    }

       this.highPercentFlag = false;
       this.interPercentFlag = false;
       if (this.formFields.high.highPercentageView) {
     if (this.user.highPercentage < this.formFields.high.highMinPercent) {
   
         this.highPercentFlag = true;
         this.highMessage = "Your are not fullfilling criteria";
        return this.highPercentFlag;

      }
    }
}

    if (this.formFields.intermediateForm) {
        if (this.formFields.inter.interYearView == true) {
            if (this.user.interYear == 0) {
                return;

            }
        }
        if (this.formFields.inter.interCourseTypeView == true) {
            if (this.user.interCourseType == 0) {
                return;
            }
        }
        if (this.formFields.inter.interBoardView == true) {
            if (this.user.interBoard == 0) {
                return;
            }
        }
        if (!this.interRequired && !this.interFileType && !this.interFileSize && this.formFields.inter.interCertificateView) {
            return;
        }
        if (this.formFields.inter.interPercentageView) {
        if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
            this.interPercentFlag = true;
            this.interMessage = "Your are not fullfilling criteria";
           
            return this.interPercentFlag;
        }
    }
}
   

    

        this.basicDetails = false;
        this.essentialDetails = false;
        this.allEducation = true;
        this.allWeightage = false;
        this.activeEssential = true;
}
    }


    allEducationDetail() {


        this.basicDetails = false;
        this.essentialDetails = false;
        this.allEducation = true;
        this.allWeightage = false;
        this.activeGraduation = true;

        if (this.formFields.graduationForm) {
            if (this.formFields.graduation.ugCourseView == true) {
                if (this.user.ugCourse == 0) {
                    return;
                }
            }
            if (this.formFields.graduation.ugYearView == true) {
                if (this.user.ugPassingYear == 0) {
                    return;
                }
            } if (this.formFields.graduation.ugUniversityView == true) {
                if (this.user.ugUniversity == 0) {
                    return;
                }
            } if (this.formFields.graduation.ugCourseTypeView == true) {
                if (this.user.ugCourseType == 0) {
                    return;

                }
            }
            if (!this.gradRequired && !this.gradFileType && !this.gradFileSize && this.formFields.graduation.ugCertificateView) {
                return;
            }

            this.ugPercentFlag = false;
            this.pgPercentFlag = false;
            if (this.formFields.graduation.ugPercentageView) {
                if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
                    this.ugPercentFlag = true;
                
                    this.ugMessage = "Your are not fullfilling criteria";
                    return this.ugPercentFlag;

                }
            }
        }
        if (this.formFields.postGraduationForm) {
            if (this.formFields.pgraduation.pgCourseView == true) {
                if (this.user.pgCourse == 0) {
                    return;
                }
            } if (this.formFields.pgraduation.pgYearView == true) {
                if (this.user.pgPassingYear == 0) {
                    return;
                }
            } if (this.formFields.pgraduation.pgUniversityView == true) {
                if (this.user.pgUniversity == 0) {
                    return;
                }
            } if (this.formFields.pgraduation.pgCourseTypeView == true) {
                if (this.user.pgCourseType == 0) {
                    return;
                }
            }
            if (!this.postGradRequired && !this.postGradFileType && !this.postGradFileSize && this.formFields.pgraduation.pgCertificateView) {
                return;
            }
            if (this.formFields.pgraduation.pgPercentageView) {
                if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
                    
                    this.pgPercentFlag = true;

                    this.pgMessage = "Your are not fullfilling criteria";
                    return this.pgPercentFlag;

                }
            }
        }
        if (this.formFields.diplomaForm) {

            if (this.formFields.diploma.diplomaBranchView == true) {
                if (this.user.diplomaBranch == 0) {
                    return;
                }
            }
            if (this.formFields.diploma.diplomaYearView == true) {
                if (this.user.diplomaYear == 0) {
                    return;
                }
            }
            if (this.formFields.diploma.diplomaUniversityView == true) {
                if (this.user.diplomaUniversity == 0) {
                    return;
                }
            }
            if (this.formFields.diploma.diplomaCourseTypeView == true) {
                if (this.user.diplomaCourseType == 0) {
                    return;

                }
            }
            if (!this.diplomaRequired && !this.diplomaFileType && !this.diplomaFileSize && this.formFields.diploma.diplomaCertificateView) {
                return;
            }
            if (this.formFields.diploma.diplomaPercentageView) {
                if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
                    this.diplomaPercentFlag = true;
                    this.diplomaMessage = "Your are not fullfilling criteria";
                    return this.diplomaPercentFlag;

                }
            }
        }
        if (this.formFields.degreeForm) {
           
            if (this.formFields.degree.degreeBranchView == true) {
                if (this.user.degreeBranch == 0) {
                    return;
                }
            }
            if (this.formFields.degree.degreeYearView == true) {
                if (this.user.degreeYear == 0) {
                    return;
                }
            }
            if (this.formFields.degree.degreeUniversityView == true) {
                if (this.user.degreeUniversity == 0) {
                    return;
                }
            }
            if (this.formFields.degree.degreeCourseTypeView == true) {
                if (this.user.degreeCourseType == 0) {
                    return;

                }
            }
            if (this.formFields.degree.degreePercentageView) {
                if (!this.degreeRequired && !this.techFileType && !this.techFileSize && this.formFields.degree.degreeCertificateView) {
                    return;
                }
                if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
                    this.degreePercentFlag = true;
                    this.degreeMessage = "Your are not fullfilling criteria";
                    return this.degreePercentFlag;
                }
            }
        }
        if (this.formFields.itiForm) {
           
            if (this.formFields.iti.itiBranchView == true) {

                if (this.user.itiBranch == 0) {
                    return;
                }
            }
            if (this.formFields.iti.itiYearView == true) {
                if (this.user.itiYear == 0) {
                    return;
                }
            }
            if (this.formFields.iti.itiUniversityView == true) {
                if (this.user.itiUniversity == 0) {
                    return;
                }
            }
            if (this.formFields.iti.itiCourseTypeView == true) {
                if (this.user.itiCourseType == 0) {
                    return;
                }
            }
            if (this.formFields.iti.itiPercentageView) {
                if (!this.itiRequired && !this.itiFileType && !this.itiFileSize && this.formFields.iti.itiCertificateView) {
                    return;
                }

                if (this.user.itiPercentage < this.formFields.iti.itiMinPercent) {
                    this.itiPercentFlag = true;
                    this.itiMessage = "Your are not fullfilling criteria";
                    return this.itiPercentFlag;
                }
            }
          
        }
        
        if (this.formFields.experienceForm) {
            if (!this.formFields.experienceCertificateForm) {
                if (!this.experienceRequired && !this.expFileType && !this.expFileSize && this.formFields.experienceCertificateForm) {
                    return;
                }
            }

            this.invalidexperience = false;
            if (this.user.employmentFrom >= this.user.employmentTo) {
                this.invalidexperience = true;
                this.experienceMessage = "Invalid Period";
                return this.invalidexperience;

            }
        }

        if (this.formFields.drivingLicenseForm) {

            if (!this.drivingRequired && !this.drivingFileType && !this.drivingFileSize && this.formFields.drivingExpUploadView) {
                return;
            }
            if (!this.licenseRequired && !this.licenseFileSize && !this.licenseFileType && this.formFields.drivingUploadView) {
                return;
            }

            this.invaliddlmv = false;
            if (this.user.issueDateLmv >= this.user.validUptoLmv) {
                this.invaliddlmv = true;
                this.dlmvMessage = "Invalid dLMV";
                return this.invaliddlmv;
            }

            this.invaliddhmv = false;
            if (this.user.issueDateHgmv >= this.user.validUptoHgmv) {
                this.invaliddhmv = true;
                this.dhmvMessage = "Invalid dHMV";
                return this.invaliddhmv;
            }
            this.invalidroad = false;
            if (this.user.issueDateRoad >= this.user.validUptoRoad) {
                this.invalidroad = true;
                this.roadMessage = "Invalid rr";
                return this.invalidroad;
            }
            this.invalidoeg = false;
            if (this.user.issueDateOeg >= this.user.validUptoOeg) {
                this.invalidoeg = true;
                this.oegMessage = "Invalid oeg";
                return this.invalidoeg;
            }

         this.weightageDetail();  
        }
else{
    this.ugPercentFlag = false;
    this.pgPercentFlag = false;

    if (this.formFields.graduationForm) {
       

        if (this.formFields.graduation.ugCourseView == true) {
            if (this.user.ugCourse == 0) {
                return;
            }
        }
        if (this.formFields.graduation.ugYearView == true) {
            if (this.user.ugPassingYear == 0) {
                return;
            }
        } if (this.formFields.graduation.ugUniversityView == true) {
            if (this.user.ugUniversity == 0) {
                return;
            }
        } if (this.formFields.graduation.ugCourseTypeView == true) {
            if (this.user.ugCourseType == 0) {
                return;

            }
        }
        if (!this.gradRequired && !this.gradFileType && !this.gradFileSize && this.formFields.graduation.ugCertificateView) {
            return;
        }

        if (this.formFields.graduation.ugPercentageView) {
            if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
                this.ugPercentFlag = true;
                
                this.ugMessage = "Your are not fullfilling criteria";
                return this.ugPercentFlag;

            }
        }
    }

    if (this.formFields.postGraduationForm) {
      
        if (this.formFields.pgraduation.pgCourseView == true) {
            if (this.user.pgCourse == 0) {
                return;
            }
        } if (this.formFields.pgraduation.pgYearView == true) {
            if (this.user.pgPassingYear == 0) {
                return;
            }
        } if (this.formFields.pgraduation.pgUniversityView == true) {
            if (this.user.pgUniversity == 0) {
                return;
            }
        } if (this.formFields.pgraduation.pgCourseTypeView == true) {
            if (this.user.pgCourseType == 0) {
                return;
            }
        }
        if (!this.postGradRequired && !this.postGradFileType && !this.postGradFileSize && this.formFields.pgraduation.pgCertificateView) {
            return;
        }
        if (this.formFields.pgraduation.pgPercentageView) {
            if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
             
                this.pgPercentFlag = true;

                this.pgMessage = "Your are not fullfilling criteria";
                return this.pgPercentFlag;

            }
        }
    }
    if (this.formFields.diplomaForm) {
       
        if (this.formFields.diploma.diplomaBranchView == true) {
            if (this.user.diplomaBranch == 0) {
                return;
            }
        }
        if (this.formFields.diploma.diplomaYearView == true) {
            if (this.user.diplomaYear == 0) {
                return;
            }
        }
        if (this.formFields.diploma.diplomaUniversityView == true) {
            if (this.user.diplomaUniversity == 0) {
                return;
            }
        }
        if (this.formFields.diploma.diplomaCourseTypeView == true) {
            if (this.user.diplomaCourseType == 0) {
                return;

            }
        }
        if (!this.diplomaRequired && !this.diplomaFileType && !this.diplomaFileSize && this.formFields.diploma.diplomaCertificateView) {
            return;
        }
        if (this.formFields.diploma.diplomaPercentageView) {
            if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
                this.diplomaPercentFlag = true;
                this.diplomaMessage = "Your are not fullfilling criteria";
                return this.diplomaPercentFlag;

            }
        }
    }
    if (this.formFields.degreeForm) {
       
        if (this.formFields.degree.degreeBranchView == true) {
            if (this.user.degreeBranch == 0) {
                return;
            }
        }
        if (this.formFields.degree.degreeYearView == true) {
            if (this.user.degreeYear == 0) {
                return;
            }
        }
        if (this.formFields.degree.degreeUniversityView == true) {
            if (this.user.degreeUniversity == 0) {
                return;
            }
        }
        if (this.formFields.degree.degreeCourseTypeView == true) {
            if (this.user.degreeCourseType == 0) {
                return;

            }
        }
        if (this.formFields.degree.degreePercentageView) {
            if (!this.degreeRequired && !this.techFileType && !this.techFileSize && this.formFields.degree.degreeCertificateView) {
                return;
            }

            if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
                this.degreePercentFlag = true;
                this.degreeMessage = "Your are not fullfilling criteria";
                return this.degreePercentFlag;
            }
        }
    }
    if (this.formFields.itiForm) {
     
        if (this.formFields.iti.itiBranchView == true) {

            if (this.user.itiBranch == 0) {
                return;
            }
        }
        if (this.formFields.iti.itiYearView == true) {
            if (this.user.itiYear == 0) {
                return;
            }
        }
        if (this.formFields.iti.itiUniversityView == true) {
            if (this.user.itiUniversity == 0) {
                return;
            }
        }
        if (this.formFields.iti.itiCourseTypeView == true) {
            if (this.user.itiCourseType == 0) {
                return;
            }
        }
        if (this.formFields.iti.itiPercentageView) {
            if (!this.itiRequired && !this.itiFileType && !this.itiFileSize && this.formFields.iti.itiCertificateView) {
                return;
            }

            if (this.user.itiPercentage < this.formFields.iti.itiMinPercent) {
                this.itiPercentFlag = true;
                this.itiMessage = "Your are not fullfilling criteria";
                return this.itiPercentFlag;
            }
        }
    }
    if (this.formFields.experienceForm) {
        if (!this.formFields.experienceCertificateForm) {
            if (!this.experienceRequired && !this.expFileType && !this.expFileSize && this.formFields.experienceCertificateForm) {
                return;
            }
        }

        this.invalidexperience = false;
        if (this.user.employmentFrom >= this.user.employmentTo) {
            this.invalidexperience = true;
            this.experienceMessage = "Invalid Period";
            return this.invalidexperience;

        }
    }
    if (this.formFields.drivingLicenseForm) {

        if (!this.drivingRequired && !this.drivingFileType && !this.drivingFileSize && this.formFields.drivingExpUploadView) {
            return;
        }
        if (!this.licenseRequired && !this.licenseFileSize && !this.licenseFileType && this.formFields.drivingUploadView) {
            return;
        }

        this.invaliddlmv = false;
        if (this.user.issueDateLmv >= this.user.validUptoLmv) {
            this.invaliddlmv = true;
            this.dlmvMessage = "Invalid dLMV";
            return this.invaliddlmv;
        }

        this.invaliddhmv = false;
        if (this.user.issueDateHgmv >= this.user.validUptoHgmv) {
            this.invaliddhmv = true;
            this.dhmvMessage = "Invalid dHMV";
            return this.invaliddhmv;
        }
        this.invalidroad = false;
        if (this.user.issueDateRoad >= this.user.validUptoRoad) {
            this.invalidroad = true;
            this.roadMessage = "Invalid rr";
            return this.invalidroad;
        }
        this.invalidoeg = false;
        if (this.user.issueDateOeg >= this.user.validUptoOeg) {
            this.invalidoeg = true;
            this.oegMessage = "Invalid oeg";
            return this.invalidoeg;
        }

       
    }

    this.basicDetails = false;
    this.essentialDetails = false;
    this.allEducation = false;
    this.allWeightage = true;
    this.activeGraduation = true;
    
}
        

    }

    weightageDetail() {


        this.basicDetails = false;
        this.essentialDetails = false;
        this.allEducation = false;
        this.allWeightage = true;
        this.activeWeightage = true;


    }

    documentDetail() {


        this.basicDetails = false;
        this.essentialDetails = false;
        this.allEducation = false;
        this.allWeightage = false;
        this.photoSign = true;

    }


    getFormFields() {
        if (this.jobs != undefined) {
            this.jobs.jobId = this.jobs.id;
        }
        this.getService.getFormFields(this.jobs.jobId).subscribe(
            data => {
                this.formFields = data;
                console.log("Form fields :", data);

                this.getService.getAmountFields(this.formFields.id).subscribe(
                    data => {
                        this.amountFields = data;
                        if (this.amountFields.length > 0) {
                            this.zoneNames = this.amountFields[0].viewBean.zone;
                        }
                        console.log("Amount fields :", data);
                    },
                    error => {

                    });
            },
            error => {

            });
    }

    // ------------------------------------------------Get Percent--------------------------------

    getHighPercentage() {
        this.highObtainGreater = false;
        if (this.user.highObtainMarks !== null && this.user.highTotalMarks !== null && this.user.highObtainMarks !== undefined && this.user.highTotalMarks !== undefined) {
            if (parseInt(this.user.highObtainMarks) > parseInt(this.user.highTotalMarks)) {
                return this.highObtainGreater = true;
            } else {
                this.user.highPercentage = (this.user.highObtainMarks / this.user.highTotalMarks) * 100;
            }

            this.user.highPercentage = this.decimalPipe.transform(this.user.highPercentage, '1.2-2');

        }

    }

    getInterPercentage() {
        this.interObtainGreater = false;
        if (this.user.interObtainMarks !== null && this.user.interTotalMarks !== null && this.user.interObtainMarks !== undefined && this.user.interTotalMarks !== undefined) {
            if (parseInt(this.user.interObtainMarks) > parseInt(this.user.interTotalMarks)) {
                return this.interObtainGreater = true;
            } else {
                this.user.interPercentage = (this.user.interObtainMarks / this.user.interTotalMarks) * 100;
            }

            this.user.interPercentage = this.decimalPipe.transform(this.user.interPercentage, '1.2-2');

        }

    }

    getUgPercentage() {
        this.ugObtainGreater = false;
        if (this.user.ugObtainMarks !== null && this.user.ugTotalMarks !== null && this.user.ugObtainMarks !== undefined && this.user.ugTotalMarks !== undefined) {
            if (parseInt(this.user.ugObtainMarks) > parseInt(this.user.ugTotalMarks)) {
                return this.ugObtainGreater = true;
            } else {
                this.user.ugPercentage = (this.user.ugObtainMarks / this.user.ugTotalMarks) * 100;
            }

            this.user.ugPercentage = this.decimalPipe.transform(this.user.ugPercentage, '1.2-2');
        }
    }

    getPgPercentage() {
        this.pgObtainGreater = false;
        if (this.user.pgObtainMarks !== null && this.user.pgTotalMarks !== null && this.user.pgObtainMarks !== undefined && this.user.pgTotalMarks !== undefined) {
            if (parseInt(this.user.pgObtainMarks) > parseInt(this.user.pgTotalMarks)) {
                return this.pgObtainGreater = true;
            } else {
                this.user.pgPercentage = (this.user.pgObtainMarks / this.user.pgTotalMarks) * 100;
            }

            this.user.pgPercentage = this.decimalPipe.transform(this.user.pgPercentage, '1.2-2');
        }
    }

    getDiplomaPercentage() {
        this.diplomaObtainGreater = false;
        if (this.user.diplomaObtainMarks !== null && this.user.diplomaTotalMarks !== null && this.user.diplomaObtainMarks !== undefined && this.user.diplomaTotalMarks !== undefined) {
            if (parseInt(this.user.diplomaObtainMarks) > parseInt(this.user.diplomaTotalMarks)) {
                return this.diplomaObtainGreater = true;
            } else {
                this.user.diplomaPercentage = (this.user.diplomaObtainMarks / this.user.diplomaTotalMarks) * 100;
            }

            this.user.diplomaPercentage = this.decimalPipe.transform(this.user.diplomaPercentage, '1.2-2');
        }
    }

    getDegreePercentage() {
        if (this.user.degreeMarksObtain !== null && this.user.degreeTotalMarks !== null && this.user.degreeMarksObtain !== undefined && this.user.degreeTotalMarks !== undefined) {
            if (parseInt(this.user.degreeMarksObtain) > parseInt(this.user.degreeTotalMarks)) {
                return this.degreeObtainGreater = true;
            } else {
                this.user.degreePercentage = (this.user.degreeMarksObtain / this.user.degreeTotalMarks) * 100;
            }

            this.user.degreePercentage = this.decimalPipe.transform(this.user.degreePercentage, '1.2-2');
        }
    }
    getItiPercentage() {
        if (this.user.itiMarksObtain !== null && this.user.itiTotalMarks !== null && this.user.itiMarksObtain !== undefined && this.user.itiTotalMarks !== undefined) {
            if (parseInt(this.user.itiMarksObtain) > parseInt(this.user.itiTotalMarks)) {
                return this.itiObtainGreater = true;
            } else {
                this.user.itiPercentage = (this.user.itiMarksObtain / this.user.itiTotalMarks) * 100;
            }

            this.user.itiPercentage = this.decimalPipe.transform(this.user.itiPercentage, '1.2-2');
        }
    }

    // ----------------------------------------------All Get State City course board university-------------

    getState() {
        this.getService.getState().subscribe(
            data => {
                this.states = data;
            },
            error => {

            });
    }

    getCity(value: any) {
        this.getService.getCity(value).subscribe(
            data => {
                this.cities = data;
                this.user.permanentCity = data[0].name;
            },
            error => {

            });
    }

    getPostalCity(value: any) {
        this.getService.getCity(value).subscribe(
            data => {
                this.postalcities = data;
            },
            error => {

            });
    }

    getBoard() {
        this.getService.getBoard().subscribe(
            data => {
                this.boards = data;
            },
            error => {

            });
    }

    getUniversity() {
        this.getService.getUniversity().subscribe(
            data => {
                this.universities = data;
            },
            error => {

            });
    }

    getCourse() {
        let value = "Graduation";
        this.getService.getCourse(value).subscribe(
            data => {
                this.gradcourses = data;
            },
            error => {

            });
    }

    getPostCourse() {
        let value = "Post_Graduation";
        this.getService.getCourse(value).subscribe(
            data => {
                this.postcourses = data;
            },
            error => {

            });

    }

    getDiplomaCourse() {
        let value = "Diploma";
        this.getService.getCourse(value).subscribe(
            data => {
                this.dipcourses = data;
            },
            error => {

            });

    }

    getDegreeCourse() {
        let value = "Degree";
        this.getService.getCourse(value).subscribe(
            data => {
                this.degcourses = data;
            },
            error => {

            });

    }
    getItiCourse() {
        let value = "Iti";
        this.getService.getCourse(value).subscribe(
            data => {
                this.iticourses = data;
            },
            error => {

            });

    }

    getCategory() {

        this.getService.getCategory().subscribe(
            data => {
                this.categories = data;
            },
            error => {

            });
    }

    getYears() {

        this.getService.getYears().subscribe(
            data => {
                this.years = data;
            },
            error => {

            });
    }


    // ---------------------------------------------Change Check-------------------------------------

    checkCategory() {
        if (this.user.category.length > 6) {
            return this.categoryCer = true;
        } else {
            return this.categoryCer = false;
        }
    }
    addressChange() {
        if (this.sameAddressCheck) {
            this.samePostalAddress = true;
        }
        else {
            this.samePostalAddress = false;
        }
    }




    cplChange(value: boolean) {
        if (value) {
            this.cplCheck = false;

        } else {
            this.cplCheck = true;
            this.user.casualplabour = '';

        }
    }

    communityChange(value: boolean) {
        if (value) {
            this.communityCheck = false;
        } else {
            this.communityCheck = true;
            this.user.communityName = 0;
        }
    }

    reapptCandidateChange(value: boolean) {
        if (value) {
            this.reapptCheck = false;
        } else {
            this.reapptCheck = true;
            this.user.reapptCanidateGs = '';
            this.user.reaaptCandidateRank = '';
            this.user.reapptCandidateName = '';
            this.user.reapptCandidateUnit = '';
        }
    }

    nccBChange(value: boolean) {
        if (value) {
            this.nccBCheck = false;
        } else {
            this.nccBCheck = true;
            this.user.nccB = '';
        }
    }

    nccCChange(value: boolean) {
        if (value) {
            this.nccCCheck = false;
        } else {
            this.nccCCheck = true;
            this.user.nccC = '';
        }
    }
    sportsChange(value: boolean) {
        if (value) {
            this.sportsCheck = false;
        } else {
            this.sportsCheck = true;
            this.user.sportsman = '';
        }
    }

    exServiceChange(value: boolean) {
        if (value) {
            this.exServiceCheck = false;
        } else {
            this.exServiceCheck = true;
            this.user.armyNumber = '';
            this.user.armyRank = '';
            this.user.armyLastUnit = '';
            this.user.armyName = '';
        }
    }
    serviceChange(value: boolean) {
        if (value) {
            this.serviceCheck = false;
        } else {
            this.serviceCheck = true;
        }
    }
    sonGPSChange(value: boolean) {
        if (value) {
            this.sonGPSCheck = false;
        } else {
            this.sonGPSCheck = true;
            this.user.sonGpsRank = '';
            this.user.sonGpsName = '';
            this.user.sonUnitName = '';
            this.user.sonGsNumber = '';
        }
    }
    gradeChange(value: boolean) {
        if (value) {
            this.highGradeCheck = false;
            this.totalCheck = true;
            this.obtainCheck = true;
            this.user.highTotalMarks='';
            this.user.highObtainMarks='';
            
        }
        else {
            this.highGradeCheck = true;
            this.totalCheck = false;
            this.obtainCheck = false;
            this.user.highGrade='';
           
        }
    }

    iGradeChange(value: boolean) {
        if (value) {
            this.interGradeCheck = false;
            this.intertotalCheck = true;
            this.interobtainCheck = true;
            this.user.interTotalMarks='';
            this.user.interObtainMarks='';
            this.user.interPercentage='';        }
        else {
            this.interGradeCheck = true;
            this.intertotalCheck = false;
            this.interobtainCheck = false;

        }

    }




    // ---------------------------------------For All Document-------------------------------

    
    categoryfileChange(event) {
        this.categoryFileType = false;
        this.categoryFileSize = false;
        this.categoryRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.categoryRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;


            //between 25 to 30k
            if (25600 < file.size && file.size < 30720) {

                if (matchType) {
                    this.user.categoryCertificateDoc = file;

                }
                else {
                    return this.categoryFileType = true;
                }
            }
            else {
                return this.categoryFileSize = true;
            }
        }
    }

    panfileChange(event) {
        this.panFileType = false;
        this.panFileSize = false;
        this.panRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.panRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            
             //between 25 to 30k
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.panCertificateDoc = file;
                }
                else {
                    return this.panFileType = true;
                }
            }
            else {
                return this.panFileSize = true;
            }
        }
    }

    cplfileChange(event) {
        this.cplFileType = false;
        this.cplFileSize = false;
        this.cplfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
             this.cplfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            
             //between 25 to 30k
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.cplCertificateDoc = file;
                }
                else {
                    return this.cplFileType = true;
                }
            }
            else {
                return this.cplFileSize = true;
            }
        }
    }

    highfileChange(event) {
        this.highFileType = false;
        this.highFileSize = false;
        this.highRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
              this.highRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
           
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.highCertificateDoc = file;
                }
                else {
                    return this.highFileType = true;
                }
            }
            else {
                return this.highFileSize = true;
            }
        }
    }


    interfileChange(event) {
        this.interFileType = false;
        this.interFileSize = false;
        this.interRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
              this.interRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
          
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.interCertificateDoc = file;
                }
                else {
                    return this.interFileType = true;
                }
            }
            else {
                return this.interFileSize = true;
            }
        }
    }

    gradfileChange(event) {
        this.gradFileType = false;
        this.gradFileSize = false;
        this.gradRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
             this.gradRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.ugCertificateDoc = file;
                }
                else {
                    return this.gradFileType = true;
                }
            }
            else {
                return this.gradFileSize = true;
            }
        }
    }

    postGradfileChange(event) {
        this.postGradFileType = false;
        this.postGradFileSize = false;
        this.postGradRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.postGradRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.pgCertificateDoc = file;
                }
                else {
                    return this.postGradFileType = true;
                }
            }
            else {
                return this.postGradFileSize = true;
            }
        }
    }

    techfileChange(event) {
        this.techFileType = false;
        this.techFileSize = false;
        this.degreeRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
              this.degreeRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
          
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.degreeCertificateDoc = file;
                }
                else {
                    return this.techFileType = true;
                }
            }
            else {
                return this.techFileSize = true;
            }
        }
    }

    itifileChange(event) {
        this.itiFileType = false;
        this.itiFileSize = false;
        this.itiRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
              this.itiRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
           
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.itiCertificateDoc = file;
                }
                else {
                    return this.itiFileType = true;
                }
            }
            else {
                return this.itiFileSize = true;
            }
        }
    }


    diplomafileChange(event) {
        this.diplomaFileType = false;
        this.diplomaFileSize = false;
        this.diplomaRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
              this.diplomaRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.diplomaCertificateDoc = file;
                }
                else {
                    return this.diplomaFileType = true;
                }
            }
            else {
                return this.diplomaFileSize = true;
            }
        }
    }


    signChange(event) {
        this.signType = false;
        this.signSize = false;
        this.signRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.signRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.browseSignatureDoc = file;
                }
                else {
                    return this.signType = true;
                }
            }
            else {
                return this.signSize = true;
            }
        }
    }

    photoChange(event) {
        this.photoType = false;
        this.photoSize = false;
        this.photoRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
             this.photoRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
           
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.browsePhotoDoc = file;
                }
                else {
                    return this.photoType = true;
                }
            }
            else {
                return this.photoSize = true;
            }
        }
    }



    experienceChange(event) {
        this.expFileType = false;
        this.expFileSize = false;
        this.experienceRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
             this.experienceRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
           
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.experienceCertificateDoc = file;
                }
                else {
                    return this.expFileType = true;
                }
            }
            else {
                return this.expFileSize = true;
            }
        }
    }



    drivingChange(event) {
        this.drivingFileType = false;
        this.drivingFileSize = false;
        this.drivingRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
             this.drivingRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.drivingExperienceDoc = file;
                }
                else {
                    return this.drivingFileType = true;
                }
            }
            else {
                return this.drivingFileSize = true;
            }
        }
    }



    licenseChange(event) {
        this.licenseFileSize = false;
        this.licenseFileType = false;
        this.licenseRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.licenseRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
           
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.drivingLicenseDoc = file;
                }
                else {
                    return this.licenseFileType = true;
                }
            }
            else {
                return this.licenseFileSize = true;
            }
        }
    }

       
    // -----------------------------------Percentage check------------------------------------
    chekPercentage() {


        this.highPercentFlag = false;
        this.interPercentFlag = false;
        if (this.user.highPercentage < this.formFields.high.highMinPercent) {
      
           this.highPercentFlag = true;
           this.highMessage = "Your are not fullfilling criteria";
          return this.highPercentFlag;

         }

       if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
           this.interPercentFlag = true;
           this.interMessage = "Your are not fullfilling criteria";
           
           return this.interPercentFlag;
       }

       this.diplomaPercentFlag = false;
       this.degreePercentFlag = false;
       this.itiPercentFlag = false;
       if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
           this.diplomaPercentFlag = true;
          
           this.diplomaMessage = "Your are not fullfilling criteria";
           return this.diplomaPercentFlag = true;

       }

       if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
           this.degreePercentFlag = true;
          
           this.degreeMessage = "Your are not fullfilling criteria";
           return this.degreePercentFlag = true;

       }
       if (this.user.itiPercentage < this.formFields.iti.itiMinPercent) {
           this.itiPercentFlag = true;
          
           this.itiMessage = "Your are not fullfilling criteria";
           return this.itiPercentFlag = true;

       }

       this.ugPercentFlag = false;
       this.pgPercentFlag = false;
       if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
           this.ugPercentFlag = true;
          
           this.ugMessage = "Your are not fullfilling criteria";
           return this.invalidPercentage = true;

       }

       if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
         
           this.pgPercentFlag = true;

           this.pgMessage = "Your are not fullfilling criteria";
           return this.invalidPercentage = true;

       }
   }


    // ---------------------------------------------------Restrict Number-------------------------
    restrictNumeric(event) {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }

    // -------------------------------------for all Select not come from Database------------------------------------- 

    public communities: any = [
        { id: 0, name: "Muslim" },
        { id: 1, name: "Christians" },
        { id: 2, name: "Sikh" },
        { id: 3, name: "Budh" },
        { id: 4, name: "Zoroas(Parsis)" }
    ];
    public coursetypes: any = [
        { id: 0, name: "Regular", value: "Regular" },
        { id: 1, name: "Private", value: "Private" },
        { id: 2, name: "Vocational", value: "Vocational" },
    ]

    public courses: any = [
        { id: 0, name: "Private", value: "Private" },
        { id: 1, name: "Regular", value: "Regular" },
    ];

}

