import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateQualificationComponent } from './candidate-qualification.component';

describe('CandidateQualificationComponent', () => {
  let component: CandidateQualificationComponent;
  let fixture: ComponentFixture<CandidateQualificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateQualificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateQualificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
