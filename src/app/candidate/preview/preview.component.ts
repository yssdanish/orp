import { Component, ViewChild, OnInit } from '@angular/core';
import { PostService, SharedService, GetService } from '../../shared/service/index';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { DecimalPipe, DatePipe } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as jsPDF from 'jspdf'
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';

@Component({
    templateUrl: './preview.component.html',
    providers: []
})


export class PreviewComponent {

    private registerId: any;
    public personals: any = {};
    public interNull: boolean;
    public ugNull: boolean;
    public pgNull: boolean;
    public degreeNull: boolean;
    public diplomaNull: boolean;
    public itiNull: boolean;
    public hgmvNull: boolean;
    public oegNull: boolean;
    public rrNull: boolean;
    public lmvNull: boolean;
    public companyNull: boolean;
    public loading = false;
    public candidateList = [];
    public centreList = [];
    public user: any = {};
    marqueeFlag: boolean

    constructor(private postService: PostService,
        private sharedService: SharedService,
        private datePipe: DatePipe,
        private getService: GetService,
        private router: Router,
        private authServerProvider: AuthServerProvider,
        private principal: Principal) {
        this.user.registrationIdTo = 0;
        this.user.registrationIdFrom = 0;
        this.user.centerName = 0;
        this.user.zoneName = 0;
        this.registerId = this.sharedService.getRegisterId();
        if (this.registerId == undefined) {
            this.router.navigate(['/candidate/login']);
        }

    }

    ngOnInit() {
        this.getPersonalDetails();
        this.getShortListCandidate();
        this.getAllocateCentre();
    }

    getPersonalDetails() {
        // let value = "3631721324";
        this.loading = true;
        this.interNull = false;
        this.ugNull = false;
        this.pgNull = false;
        this.degreeNull = false;
        this.itiNull = false;
        this.diplomaNull = false;
        this.hgmvNull = false;
        this.lmvNull = false;
        this.oegNull = false;
        this.rrNull = false;
        this.companyNull = false;
        this.getService.getPersonalDetails(this.registerId).subscribe(
            data => {
                console.log("getPersonalDetails :", data);
                this.loading = false;
                if (!data.payment) {
                    // this.router.navigate(['/candidate/payment']);
                }
                this.personals = data;

                if (data.essential.interInstitution == "null") {
                    this.interNull = true;
                }
                if (data.graduation.ugInstitution == "null") {
                    this.ugNull = true;
                }
                if (data.graduation.pgInstitution == "null") {
                    this.pgNull = true;
                }
                if (data.technical.diplomaSpecialization == "null") {
                    this.diplomaNull = true;
                }
                if (data.technical.degreeSpecialization == "null") {
                    this.degreeNull = true;
                }
                if (data.technical.itiSpecialization == "null" || data.technical.itiSpecialization === null) {
                    this.itiNull = true;
                }
                if (data.experience.companyName == "null") {
                    this.companyNull = true;
                }
                if (data.driving.issuingAuthorityLmv == "null") {
                    this.lmvNull = true;
                }
                if (data.driving.issuingAuthorityHlmv == "null") {
                    this.hgmvNull = true;
                }
                if (data.driving.issuingAuthorityRoad === null || data.driving.issuingAuthorityRoad == "null") {
                    this.rrNull = true;
                }
                if (data.driving.issuingAuthorityOeg === null || data.driving.issuingAuthorityOeg == "null") {
                    this.oegNull = true;
                }
            },
            error => {
                // this.alertService.error(error);
                this.loading = false;
            });
    }

    getShortListCandidate() {
        this.marqueeFlag = false;
        this.getService.getCandidates().subscribe(
            data => {
                this.candidateList = data;
                console.log("Candidate list: ", this.candidateList);
                if (this.candidateList.some(x => x.registrationId === this.registerId)) {
                    this.marqueeFlag = true;
                }
            }, error => {

            });

    }


    getAllocateCentre() {
        this.marqueeFlag = false;
        this.getService.getAllocatedCenters().subscribe(
            data => {
                this.centreList = data;
                console.log("Centre list: ", this.centreList);
                if (this.centreList.some(x => x.registrationId === this.registerId)) {
                    this.marqueeFlag = true;
                }
            }, error => {
                this.marqueeFlag = false;
            });



    }

    generateAdmitCard() {
        var doc = new jsPDF();
        // var imgData = 'data:image/jpeg;base64,'+ Base64.encode('Koala.jpeg');
        var imgData = new Image;
        // imgData.src = "../../../assets/images/lion.png";

        //    imgData.src = "../../../assets/images/headofAdmit.png";
        //    doc.addImage(imgData, 'jpg', 15,5, 150, 60);


        imgData.src = "../../../assets/images/admitCardImages/left.png";
        doc.addImage(imgData, 'png', 15, 5, 25, 25);

        doc.setFontSize(16);
        doc.setFontType("bold")
        
        doc.text(65, 10, 'BORDER ROADS ORGANIZATION');

        doc.setFontSize(10);
        doc.text(77, 18, 'Creates, Connect & Cares...');

        doc.setFontSize(10);
        
        doc.text(77, 25, 'GREF & Record Centre, Pune');

        imgData.src = "../../../assets/images/admitCardImages/right.png";
        doc.addImage(imgData, 'png', 170, 5, 25, 25);
        // doc.text(20, 20, 'New page added');

      
        doc.setFontSize(12);
        //candidate photo
        imgData.src = "../../../assets/images/" + this.personals.registrationId + "/" + this.personals.certificate.browsePhoto;
        doc.addImage(imgData, 'JPG', 140, 65, 40, 40);

        doc.text(" Post: " + this.personals.jobDetails.postName, 90, 60);
        //candidate sig
        imgData.src = "../../../assets/images/" + this.personals.registrationId + "/" + this.personals.certificate.browseSignature;
        doc.addImage(imgData, 'JPG', 140, 110, 40, 15);


        // for office use only
        imgData.src = "../../../assets/images/box.png";
        doc.addImage(imgData, 'PNG', 140, 135, 40, 50);


        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text("Zone Name :  " + this.centreList[0].zoneName, 22, 64);
        doc.text("Roll No :  " + this.personals.registrationId, 22, 70);
        doc.text("Register Id :  " + this.personals.registrationId, 22, 76);
        doc.text("Candidate Name :  " + this.personals.firstName + "" + this.personals.middleName + " " + this.personals.lastName, 22, 82);
        doc.text("Father's Name :  " + this.personals.fatherName, 22, 88);
        // doc.text("Mother's Name: " + this.personals.motherName,25,90);
        doc.text("Category :  " + this.personals.Category, 22, 94);
        doc.text("Date Of Birth :  " + this.datePipe.transform(this.personals.dateOfBirth, 'dd-MM-yyyy'), 22, 100);
        doc.text("Address :  " + this.personals.postalAddress, 22, 106);
        doc.text("State :  " + this.personals.postalState, 22, 112);
        doc.text("City :  " + this.personals.postalCity, 22, 118);
        doc.text("PinCode :  " + this.personals.postalPincode, 22, 124);
        doc.text("Nationlity :  " + this.personals.nationality, 22, 130);
        doc.text("Exam Center :  " + this.centreList[0].centerName, 22, 136);
        doc.text("Exam Date :  "+this.centreList[0].examDate, 22, 142);
        doc.text("Reporting Time : "+ this.centreList[0].examTime , 22, 148);
        doc.text("Exam Time :  "+ this.centreList[0].examTime, 85, 148);
        doc.text("Duration Time :  "+ this.centreList[0].duration, 85, 148);
        

        doc.setFontType("bold")
        doc.text("Full signature of candidate              Sign at the time of physical test       Sign at the time of practical test", 10, 200);

        doc.text("Sign at the time of joining service      Sign at the time of written test        Left hand thumb impression", 10, 235);


        doc.addPage();
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text(11, 20, '1. Candidates should bring the following items with them at the time of appearing for the examination:');
        doc.text(15, 25, '(a) Admit Card');
        doc.text(15, 30, '(b) Ball Pen (Black Ink), HB Pencil, Scale, Eraser etc.');
        doc.text(15, 35, '(c) Admit card duly filled and signed at the space provided');
        doc.text(15, 40, '(d) Photo Identity proof (Driving License, Voters ID, passport, PAN card etc.) in original for verification.');
        doc.text(11, 46, '2.  Candidates will not be allowed into examination hall without admit card & photo identity proof in original.');
        doc.text(11, 52, '3.  Candidates should reach the exam centre one hour before the scheduled time of examination.');
        doc.text(11, 58, '4.  The written examination will be of Objective-type questions consisting of General Knowledge, General');
        doc.text(15, 64, ' Arithmetic and Basic Science and would be conducted in OMR sheet. Instructions given in the OMR sheet shall ');
        doc.text(15, 70, 'be read before starting to mark the answers.');
        doc.text(11, 76, '5.  Candidates are not allowed to carry any textual material, printed or written bits of paper, mobile phone, electronic');
        doc.text(15, 82, 'device or any other material.');
        doc.text(11, 88, '6.  Ensure that all the pages in the question booklet are available. In case of discrepancy, the candidate should');
        doc.text(15, 94, 'immediately report the matter to the invigilator for the replacement of booklet.');
        doc.text(11, 100, '7.  HB pencil hall be used for shading the answers in OMR sheet.');
        doc.text(11, 106, '8.  Only name and signature should be made in pen at the space provided in the OMR sheet.');
        doc.text(11, 112, '9.  Separate sheet will be given for rough work. Candidate should not mark or write anything on the question booklet ');
        doc.text(15, 118, 'or tracing side of the OMR sheet. ');
        doc.text(11, 124, '10. Candidate should not leave the Examination Hall without handing over their original OMR/Answer sheet alongwith');
        doc.text(15, 130, 'Question booklet and rough sheets to the Invigilator on duty in the Room/Hall and sign the attendance sheet. ');
        doc.text(11, 136, '11. If candidates found committing any malpractice, action will be taken against them as per rules.');
        doc.text(11, 142, '12. Candidates are not allowed to leave the examination hall till the end of the session.');
        doc.text(11, 148, '13. Question paper will be in Hindi and English. Candidates have option to choose any question paper of any of the');
        doc.text(15, 154, 'above language.');
        doc.text(11, 160, '14. No request for postponement of Written Exam, Change of Venue, etc. will be entertained under any circumstances.');
        doc.text(11, 166, '15. Candidates who qualify the written examination will be called for Interview at a later date which will be intimated');
        doc.text(15, 172, 'separately.');
        doc.text(11, 178, '16. Result of the written examination will be published in our website only, hence candidates are advised to visit ');
        doc.text(15, 184, 'our website in regularly for further instructions / updates.');
        doc.save('AdmitCard' + this.personals.registrationId + '.pdf');

    }
    myprint() {
        window.print();
    }
    logout() {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
        this.router.navigate(['/home']);
    }
}