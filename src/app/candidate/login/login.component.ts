import { Component, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';

import { LoginService } from '../../shared/login/login.service'
import { StateStorageService } from '../../shared/auth/state-storage.service';
import { SharedService } from '../../shared/service/index'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [NgbActiveModal]
})
export class LoginComponent implements AfterViewInit {
  authenticationError: boolean;
  password: string;
  rememberMe: boolean;
  username: string;
  credentials: any;
  user: any = {};
  public loading = false;

  constructor(
    private eventManager: JhiEventManager,
    private loginService: LoginService,
    private stateStorageService: StateStorageService,
    private elementRef: ElementRef,
    private renderer: Renderer,
    private router: Router,
    public activeModal: NgbActiveModal,
    public sharedService: SharedService
  ) {
    this.credentials = {};
  }

  ngAfterViewInit() {
    this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []);
  }

  cancel() {
    this.credentials = {
      username: null,
      password: null,
      rememberMe: true
    };
    this.authenticationError = false;
    this.activeModal.dismiss('cancel');
  }

  login() {
    this.loading = true;
    this.loginService.login({
      username: this.user.username,
      password: this.user.password,
      rememberMe: this.user.rememberMe
    }).then(() => {
      this.loading = false;
      this.authenticationError = false;
      this.activeModal.dismiss('login success');
      if (this.router.url === '/register' || (/^\/activate\//.test(this.router.url)) ||
        (/^\/reset\//.test(this.router.url))) {
        this.router.navigate(['']);
      }

      this.eventManager.broadcast({
        name: 'authenticationSuccess',
        content: 'Sending Authentication Success'
      });

      // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
      // // since login is succesful, go to stored previousState and clear previousState
      const redirect = this.stateStorageService.getUrl();
      // if (redirect) {
      //   this.stateStorageService.storeUrl(null);
      //   this.router.navigate([redirect]);
      // }
      this.sharedService.saveRegisterData(this.user.username);
      this.router.navigate(['/candidate/preview']);
    }).catch(() => {
      this.authenticationError = true;
      this.loading = false;
    });
  }

  public resolved(captchaResponse: string) {
    // console.log(`Resolved captcha with response ${captchaResponse}:`);
  }

  register() {
    this.activeModal.dismiss('to state register');
    this.router.navigate(['/register']);
  }

  requestResetPassword() {
    this.activeModal.dismiss('to state requestReset');
    this.router.navigate(['/reset', 'request']);
  }
}
