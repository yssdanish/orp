import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { ModalComponent } from '../../shared/model/shared-modal.component';


@Component({
    selector: 'app-candidate-register',
    templateUrl: './candidate-register.component.html',
    styleUrls: ['./candidate-register.component.css']
})
export class CandidateRegisterComponent implements OnInit {
    @ViewChild(ModalComponent) popUp: ModalComponent;
    public essentialDetails: boolean = false;
    public personalDetails: boolean = true;
    public activePersonal: boolean = true;
    public activeEssential: boolean = false;
    public activeGraduation: boolean = false;
    public activeTechnical: boolean = false;
    public activeWeightage: boolean = false;
    public activeExperience: boolean = false;
    public activeDriving: boolean = false;
    public activeUpload: boolean = false;
    public activePreview: boolean = false;
    public activePayment: boolean = false;

    public adharCheck: boolean = true;
    public cplCheck: boolean = true;
    public disableCheck: boolean = true;
    public communityCheck: boolean = true;
    public sameAddressCheck: boolean = false;
    public samePostalAddress: boolean = false;
    public exServiceCheck: boolean = true;
    public serviceCheck:boolean = true;
    public createError: boolean;
    public centralCheck: boolean = true;
    public armedCheck: boolean = true;

    public hindiStenographyCheck:boolean = true;
    public englishStenographyCheck:boolean = true;
    public nccBCheck: boolean = true;
    public nccCCheck: boolean = true;
    public sportsCheck: boolean = true;
    public degreeCheck: boolean = true;
    public itiCheck: boolean = true;
    public empExBroCheck: boolean = true;
    public broGREFCheck: boolean = true;
    public sonGREFCheck: boolean = true;
    public sonGPSCheck: boolean = true;
    public reapptCheck: boolean = true;
    public highGradeCheck: boolean = true;
    public interGradeCheck: boolean = true;
    public hmvLicenseCheck:boolean = true;
    public roadRollerCheck:boolean = true;
    public hindiTypingCheck:boolean=true;
    public englishTypingCheck:boolean=true;
    public highFileSize: boolean = false;
    public highFileType: boolean = false;
    public interFileSize: boolean = false;
    public interFileType: boolean = false;
    public gradFileSize: boolean = false;
    public gradFileType: boolean = false;
    public postGradFileType: boolean = false;
    public postGradFileSize: boolean = false;
    public techFileSize: boolean = false;
    public techFileType: boolean = false;
    public itiFileSize: boolean = false;
    public itiFileType: boolean = false;
    public armedFileType: boolean = false;
    public armedFileSize: boolean = false;

    public totalCheck: boolean = false;
    public obtainCheck: boolean = false;
    public intertotalCheck: boolean = false;
    public interobtainCheck: boolean = false;
    public photoSize: boolean = false;
    public photoType: boolean = false;
    public signSize: boolean = false;
    public signType: boolean = false;
    public expFileSize: boolean = false;
    public expFileType: boolean = false;

    public drivingFileType: boolean = false;
    public drivingFileSize: boolean = false;
    public licenseFileType: boolean = false;
    public licenseFileSize: boolean = false;

    public categoryFileSize: boolean = false;
    public categoryFileType: boolean = false;
    public nationFileSize: boolean = false;
    public nationFileType: boolean = false;
    public aadhaarFileSize: boolean = false;
    public aadhaarFileType: boolean = false;
    public cplFileSize: boolean = false;
    public cplFileType: boolean = false;
    public panFileSize: boolean = false;
    public panFileType: boolean = false;
    public centralFileSize: boolean = false;
    public centralFileType: boolean = false;
    public nccbFileSize: boolean = false;
    public nccbFileType: boolean = false;
    public ncccFileSize: boolean = false;
    public ncccFileType: boolean = false;
    public unitFileSize: boolean = false;
    public unitFileType: boolean = false;
    public lastUnitFileSize: boolean = false;
    public lastUnitFileType: boolean = false;
    public dischargeFileType: boolean = false;
    public dischargeFileSize: boolean = false;
    public sportsFileType: boolean = false;
    public sportsFileSize: boolean = false;
    public addFileType: boolean = false;
    public addFileSize: boolean = false;
    public aidFileType: boolean = false;
    public aidFileSize: boolean = false;


    public diplomaFileType: boolean = false;
    public diplomaFileSize: boolean = false;
    public error: string;
    public errorEmailExists: string;
    public errorUserExists: string;
    public success: boolean;
    public previewButton: boolean = true;

    public graduationDetails: boolean = false;
    public technicalDetails: boolean = false;
    public weightageDetails: boolean = false;
    public experienceDetails: boolean = false;
    public drivingDetails: boolean = false;
    public paymentDetails: boolean = false;
    public basicDetails: boolean = true;
    public finalSubmit: boolean = false;
    public uploadDetails: boolean = false;
    public nationRequired: boolean = false;
    public categoryRequired: boolean = false;
    public aadhaarfileRequired: boolean = false;
    public cplfileRequired: boolean  = false;
    public panRequired: boolean = false;
    public centralfileRequired: boolean = false;
    public armedfileRequired: boolean = false;
    public nccbfileRequired: boolean = false;
    public nccfileRequired: boolean = false;
    public unitfileRequired: boolean = false;
    public lastUnitfileRequired: boolean = false;
    public sportsfileRequired: boolean = false;
    public dischargefileRequired: boolean = false;
    public highRequired: boolean = false;
    public interRequired: boolean = false;
    public additionalRequired: boolean = false;
    public firstAidRequired: boolean = false;

    public gradRequired: boolean = false;
    public postGradRequired: boolean = false;
    public degreeRequired: boolean = false;
    public itiRequired: boolean = false;
    public diplomaRequired: boolean = false;
    public photoRequired: boolean = false;
    public signRequired: boolean = false;
    public experienceRequired: boolean = false;
    public drivingRequired: boolean = false;
    public licenseRequired: boolean = false;
    public highObtainGreater: boolean = false;
    public interObtainGreater: boolean = false;
    public ugObtainGreater: boolean = false;
    public pgObtainGreater: boolean = false;
    public diplomaObtainGreater: boolean = false;
    public degreeObtainGreater: boolean = false;
    public itiObtainGreater: boolean = false;
    public emailExists: boolean = false;
    public createSuccess: boolean = false;
    public loading = false;
    public categoryCer: boolean;
    public nationcheck: boolean;
    public ageInvalid: boolean;
    public ageMessage: string;

    public invalidexperience: boolean;
    public experienceMessage: string;

    public invaliddlmv: boolean;
    public invalidroad: boolean;
    public invalidoeg: boolean;

    public dlmvMessage: string;
    public roadMessage: string;
    public oegMessage: string;

    public invaliddhmv: boolean;
    public dhmvMessage: string;

    public user: any = {};
    public states: any = [];
    public cities: any = [];
    public postalcities: any = [];
    public boards: any = [];
    public universities: any = [];
    public gradcourses: any = [];
    public postcourses: any = [];
    public dipcourses: any = [];
    public degcourses: any = [];
    public iticourses: any = [];
    public personals: any = [];
    public jobs: any;
    public formFields: any = {};
    public amountFields: any = {};
    public categories: any = [];
    public years: any = [];
    public relaxAge: any; // admin given relaxation
    public appliedAge: any;// candidate age -relaxation age
    public jobmaxAge: any; //apply max age by admin
    public jobminAge: any; // apply min age by admin
    public highMessage: string;
    public interMessage: string;
    public ugMessage: string;
    public pgMessage: string;
    public highPercentFlag: boolean;
    public interPercentFlag: boolean;
    public pgPercentFlag: boolean;
    public ugPercentFlag: boolean;
    public diplomaMessage: string;
    public degreeMessage: string;
    public itiMessage: string;
    public degreePercentFlag: boolean;
    public itiPercentFlag: boolean;
    public diplomaPercentFlag: boolean;

    public highNull:boolean;
    public interNull: boolean;
    public ugNull: boolean;
    public pgNull: boolean;
    public degreeNull: boolean;
    public itiNull: boolean;
    public diplomaNull: boolean;
    public hgmvNull: boolean;
    public lmvNull: boolean;
    public rrNull: boolean;
    public oegNull: boolean;
    public companyNull: boolean;
    public zoneNames: any = {};
    public zoneRequired: boolean;
    public zoneMessage: string;


    minDate: Date;
    maxDate: Date;

    minDateservedfrom = new Date(1992, 5, 10);
    maxDateservedfrom = new Date();
    minDateservedto = new Date(1992, 5, 10);
    maxDateservedto = new Date();

    minDatearmedfrom = new Date(1950, 0, 1);
    maxDatearmedfrom = new Date();
    minDatearmedto = new Date(1950, 0, 1);
    maxDatearmedto = new Date();

    minDateExempfrom = new Date(1950, 0, 1);
    maxDateExempfrom = new Date();
    minDateExempto = new Date(1950, 0, 1);
    maxDateExempto = new Date();


    minDateExpFrom = new Date(1992, 5, 10);
    maxDateExpFrom = new Date();
    minDateExpTO = new Date(1992, 5, 10);
    maxDateExpTo = new Date();

    minDateDlmvFrom = new Date(1992, 5, 10);
    maxDateDlmvFrom = new Date();
    minDateDlmvTO = new Date();
    maxDateDlmvTo = new Date(2100, 5, 12);

    minDateRrFrom = new Date(1992, 5, 10);
    maxDateRrFrom = new Date();
    minDateRrTo = new Date();
    maxDateRrTo = new Date(2100, 5, 12);

    minDateOegFrom = new Date(1992, 5, 10);
    maxDateOegFrom = new Date();
    minDateOegTO = new Date();
    maxDateOegTo = new Date(2100, 5, 12);

    minDateDhgmvFrom = new Date(1992, 5, 10);
    maxDateDhgmvFrom = new Date();
    minDateDhgmvTO = new Date();
    maxDateDhgmvTo = new Date(2100, 5, 12);

    public invalidDob: boolean;

    public invalidPercentage: boolean;

    bsConfig: Partial<BsDatepickerConfig>;
    colorTheme = "theme-red";
    public calyear = {};



    constructor(public postService: PostService,
        private getService: GetService,
        private decimalPipe: DecimalPipe, private route: ActivatedRoute,
        public sharedService: SharedService, private router: Router,
        private datePipe: DatePipe) {
        this.user.category = 0;
        this.user.communityName = 0;
        this.user.sportsLevel = 0;
        this.user.nationality = 0;
        this.user.maritalStatus = 0;
        this.user.permanentState = 0;
        this.user.permanentCity = 0;
        this.user.postalState = 0;
        this.user.postalCity = 0;
        this.user.gender = 0;
        this.user.highYear = 0;
        this.user.highBoard = 0;
        this.user.highCourseType = 0;
        this.user.interYear = 0;
        this.user.interCourseType = 0;
        this.user.interBoard = 0;
        this.user.ugCourse = 0;
        this.user.ugPassingYear = 0;
        this.user.ugUniversity = 0;
        this.user.ugCourseType = 0;
        this.user.pgCourse = 0;
        this.user.pgPassingYear = 0;
        this.user.pgUniversity = 0;
        this.user.pgCourseType = 0;
        this.user.diplomaBranch = 0;
        this.user.diplomaYear = 0;
        this.user.diplomaUniversity = 0;
        this.user.diplomaCourseType = 0;
        this.user.degreeBranch = 0;
        this.user.degreeUniversity = 0;
        this.user.degreeYear = 0;
        this.user.degreeCourseType = 0;
        this.user.itiBranch = 0;
        this.user.itiUniversity = 0;
        this.user.itiYear = 0;
        this.user.itiCourseType = 0;
        this.sharedService = sharedService;
        this.jobs = sharedService.getData();

        this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    }

    ngOnInit() {
        if (this.jobs == undefined) {
            this.popUp.getError("Please select job first");
        }
        this.getState();
        this.getUniversity();
        this.getBoard();
        this.getCourse();
        this.getPostCourse();
        this.getDiplomaCourse();
        this.getDegreeCourse();
        this.getItiCourse();
        this.getCategory();
        this.getYears();
        this.getFormFields();
        this.createSuccess = false;
    }
    getFormFields() {
        if (this.jobs != undefined) {
            this.jobs.jobId = this.jobs.id;
        }
        this.getService.getFormFields(this.jobs.jobId).subscribe(
            data => {
                this.formFields = data;
                console.log("Form fields :", data);

                this.getService.getAmountFields(this.formFields.id).subscribe(
                    data => {
                        this.amountFields = data;
                        if (this.amountFields.length > 0) {
                            this.zoneNames = this.amountFields[0].viewBean.zone;
                        }
                        console.log("Amount fields :", data);
                    },
                    error => {
                        // this.alertService.error(error);
                    });
            },
            error => {
                //  this.alertService.error(error);
            });
    }

    getState() {
        this.getService.getState().subscribe(
            data => {
                this.states = data;
            },
            error => {
                //this.alertService.error(error);
                // this.loading = false;
            });
    }

    getCity(value: any) {
        this.getService.getCity(value).subscribe(
            data => {
                this.cities = data;
                this.user.permanentCity = data[0].name;
            },
            error => {
                //   this.alertService.error(error);
                // this.loading = false;
            });
    }

    getPostalCity(value: any) {
        this.getService.getCity(value).subscribe(
            data => {
                this.postalcities = data;
            },
            error => {
                // this.alertService.error(error);
            });
    }

    getBoard() {
        this.getService.getBoard().subscribe(
            data => {
                this.boards = data;
            },
            error => {
                // this.loading = false;
            });
    }

    getUniversity() {
        this.getService.getUniversity().subscribe(
            data => {
                this.universities = data;
            },
            error => {
                //   this.alertService.error(error);
                // this.loading = false;
            });
    }

    getCourse() {
        let value = "Graduation";
        this.getService.getCourse(value).subscribe(
            data => {
                this.gradcourses = data;
            },
            error => {
                //    this.alertService.error(error);
                // this.loading = false;
            });
    }

    checkCategory() {
        if (this.user.category.length > 6) {
            return this.categoryCer = true;
        } else {
            return this.categoryCer = false;
        }
    }

    checkNation() {
        if (this.user.nationality.length > 5) {
            return this.nationcheck = true;
        } else {
            return this.nationcheck = false;
        }
    }

    getPostCourse() {
        let value = "Post_Graduation";
        this.getService.getCourse(value).subscribe(
            data => {
                this.postcourses = data;
            },
            error => {
                //  this.alertService.error(error);
                // this.loading = false;
            });

    }

    getDiplomaCourse() {
        let value = "Diploma";
        this.getService.getCourse(value).subscribe(
            data => {
                this.dipcourses = data;
            },
            error => {
                //  this.alertService.error(error);
                // this.loading = false;
            });

    }

    getDegreeCourse() {
        let value = "Degree";
        this.getService.getCourse(value).subscribe(
            data => {
                this.degcourses = data;
            },
            error => {
                //    this.alertService.error(error);
                // this.loading = false;
            });

    }
    getItiCourse() {
        let value = "Iti";
        this.getService.getCourse(value).subscribe(
            data => {
                this.iticourses = data;
            },
            error => {
                //    this.alertService.error(error);
                // this.loading = false;
            });

    }

    getPersonalDetails() {
        this.loading = true;
        this.highNull = false;
        this.interNull = false;
        this.ugNull = false;
        this.pgNull = false;
        this.degreeNull = false;
        this.itiNull = false;
        this.diplomaNull = false;
        this.hgmvNull = false;
        this.lmvNull = false;
        this.rrNull = false;
        this.oegNull = false;
        this.companyNull = false;

        this.getService.getPersonalDetails(this.user.registrationId).subscribe(
            data => {
                console.log("getPersonalDetails :", data);
                this.personals = data;

                if (data.high.highBoard =="null")
                {
this.highNull =true;
                }
                if (data.high.interBoard =="null")
                {
this.interNull =true;
                }
                // if (data.essential.interInstitution == "null") {
                //     this.interNull = true;
                // }
                if (data.graduation.ugInstitution == "null") {
                    this.ugNull = true;
                }
                if (data.graduation.pgInstitution == "null") {
                    this.pgNull = true;
                }
                if (data.technical.diplomaSpecialization == "null") {
                    this.diplomaNull = true;
                }
                if (data.technical.degreeSpecialization == "null") {
                    this.degreeNull = true;
                }
                if (data.technical.itiSpecialization == "null" || data.technical.itiSpecialization === null) {
                    this.itiNull = true;
                }
                if (data.experience.companyName == "null") {
                    this.companyNull = true;
                }
                if (data.driving.issuingAuthorityLmv == "null") {
                    this.lmvNull = true;
                }
                if (data.driving.issuingAuthorityRoad == "null" || data.driving.issuingAuthorityRoad === null) {
                    this.rrNull = true;
                }
                if (data.driving.issuingAuthorityOeg == "null" || data.driving.issuingAuthorityOeg === null) {
                    this.oegNull = true;
                }
                if (data.driving.issuingAuthorityHlmv == "null") {
                    this.hgmvNull = true;
                }
            },
            error => {
                this.loading = false;
                //  this.alertService.error(error);
                // this.loading = false;
            });
    }

    getCategory() {
        // let value = "3631721324";
        this.getService.getCategory().subscribe(
            data => {
                this.categories = data;
            },
            error => {
                //  this.alertService.error(error);
                // this.loading = false;
            });
    }

    getYears() {
        // let value = "3631721324";
        this.getService.getYears().subscribe(
            data => {
                this.years = data;
            },
            error => {
                // this.alertService.error(error);
                // this.loading = false;
            });
    }

    personalDetail() {

        this.basicDetails = true;
        this.personalDetails = true;
        this.essentialDetails = false;
        this.graduationDetails = false;
        this.technicalDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.weightageDetails = false;
        this.finalSubmit = false;
        this.uploadDetails = false;
        this.activePersonal = true;

    }

    checkZone() {
        this.zoneRequired = false;
    }

    essentialDetail() {
        // ---------------------------------Condition---------------------------------

        this.zoneRequired = false;
        this.ageInvalid = false;
        if (this.user.recruitmentZone == '' || this.user.recruitmentZone === undefined) {
            this.zoneMessage = 'Please Select Zone';
            return this.zoneRequired = true;
        }
        // if (this.user.category == 0 || this.user.nationality == 0 || this.user.permanentState == 0 || this.user.permanentCity == 0) {
        //     return;
        // }
        if (this.user.category == 0 || this.user.permanentState == 0 || this.user.permanentCity == 0) {
            return;
        }
        if (!this.samePostalAddress && (this.user.postalState == 0 || this.user.postalCity == 0)) {
            return;
        }
        if (!this.panRequired && !this.panFileType && !this.panFileSize) {
            return;
        }

        if (!this.communityCheck && this.user.communityName == 0) {
            return;
        }

         if (!this.cplCheck && !this.cplfileRequired && !this.cplFileType && !this.cplFileSize) {
            return;
        }
        // if (this.emailExists) {
        //     return;
        // }
        // if (!this.armedCheck && !this.armedfileRequired && !this.armedFileType && !this.armedFileSize) {
        //     return;
        // }
        // if (!this.centralCheck && !this.centralfileRequired && !this.centralFileType && !this.centralFileSize) {
        //     return;
        // }
        // if (!this.nccBCheck && !this.nccbfileRequired && !this.nccbFileSize && !this.nccbFileType) {
        //     return;
        // }
        // if (!this.nccCCheck && !this.nccfileRequired && !this.ncccFileType && !this.ncccFileSize) {
        //     return;
        // }
        // if (!this.sportsCheck && !this.sportsfileRequired && !this.sportsFileType && !this.sportsFileSize) {
        //     return;
        // }
        // if (!this.categoryCer && !this.categoryRequired && !this.categoryFileType && !this.categoryFileSize) {
        //     return;
        // }
        // if (!this.adharCheck && !this.aadhaarfileRequired && !this.aadhaarFileType && !this.aadhaarFileSize) {
        //     return;
        // }
        // if (!this.nationcheck && !this.nationRequired && !this.nationFileType && !this.nationFileSize) {
        //     return;
        // }

        // if (this.amountFields[i].categoryName == "UR") {
        //     if(categ == "General"){
        //     this.maxDate.setDate(this.amountFields[i].maxAgeDate);
        //     // this.maxDate = new Date(this.amountFields[i].maxAgeDate);
        //     this.minDate.setDate(this.amountFields[i].minAgeDate);
        //     // this.minDate = new Date(this.amountFields[i].minAgeDate);
        //     }
        // }

        // if (this.amountFields[i].categoryName == "SC") {
        //     if(categ == "SC"){
        //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
        //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
        //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
        //     this.minDate = new Date(this.amountFields[i].minAgeDate);
        //     }
        // }

        // if (this.amountFields[i].categoryName == "ST") {
        //     if(categ == "ST"){
        //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
        //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
        //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
        //     this.minDate = new Date(this.amountFields[i].minAgeDate);
        //     }
        // }

        // if (this.amountFields[i].categoryName == "OBC") {
        //     if(categ == "OBC"){
        //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
        //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
        //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
        //     this.minDate = new Date(this.amountFields[i].minAgeDate);
        //     }
        // }



        for (let i = 0; i < this.amountFields.length; i++) {
            if (this.amountFields[i].categoryName == "SC") {

                if (this.user.category == "SC") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if ((finalYear < minValue || finalYear > maxValue)) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }

            if (this.amountFields[i].categoryName == "ST") {
                if (this.user.category == "ST") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if (finalYear < minValue || finalYear > maxValue) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }

            if (this.amountFields[i].categoryName == "OBC") {
                if (this.user.category == "OBC") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if (finalYear < minValue || finalYear > maxValue) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }
            if (this.amountFields[i].categoryName == "UR") {
                if (this.user.category == "General") {
                    let minValue = this.amountFields[i].minAge;
                    let maxValue = this.amountFields[i].maxAge;

                    if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                        let relaxAge = this.amountFields[i].ageRelaxation;
                        let finalYear = this.calyear + relaxAge;

                        if (finalYear < minValue || finalYear > maxValue) {
                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    } else {

                        if (this.calyear < minValue || this.calyear > maxValue) {

                            this.ageMessage = "Your age is Invalid!"
                            return this.ageInvalid = true;
                        }
                    }
                }
            }
        }

        // if (this.user.category == "SC") {
        //     if (this.user.dateOfBirth > this.formFields.jobDetails.expiryDate) {

        //         alert("Pleas Enter Valid Date of Birth");
        //         //this.message=("invalid")
        //         return this.invalidDob = true;
        //     }


        //     if (this.user.age > this.formFields.maxAge) {
        //         alert("Invalid Age");
        //         return this.invalidDob = true;
        //     }
        // }

        // if (this.user.category == "GEN") {
        //     if (this.user.dateOfBirth > this.formFields.jobDetails.expiryDate) {
        //         //alert("Pleas Enter Valid Date of Birth");
        //         return this.invalidDob = true;
        //     }

        //     if (this.user.age > this.formFields.maxAge) {
        //         alert("Invalid Age");
        //         return this.invalidDob = true;
        //     }
        // }

        // if (this.user.category == "ST") {
        //     if (this.user.dateOfBirth > this.formFields.expiryDate) {
        //         alert("Pleas Enter Valid Date of Birth");
        //         return this.invalidDob = true;
        //     }
        //     if (this.user.age > this.formFields.maxAge) {
        //         alert("Invalid Age");
        //         return this.invalidDob = true;
        //     }
        // }

        // if (this.user.category == "OBC") {
        //     if (this.user.dateOfBirth > this.formFields.jobDetails.expiryDate) {

        //         alert("Invalid Date of birth");
        //         return this.invalidDob = true;
        //     }

        //     if (this.user.age > this.formFields.maxAge) {
        //         alert("Invalid Age");
        //         return this.invalidDob = true;
        //     }
        // }


        // if (this.user.category == "General") {
        //     this.relaxAge = 0;
        //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

        //         if(this.user.disablity == true){
        //                 this.relaxAge = this.relaxAge+2;
        //         }

        //         if(this.user.centralGovt == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
        //         }

        //         if(this.user.empExGref == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
        //         }

        //         this.appliedAge = this.user.age - this.relaxAge;
        //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
        //         {
        //             return this.invalidDob = false; 
        //         }
        //         else 
        //         return this.invalidDob = true;
        //     }
        // }


        // if (this.user.category == "SC") {
        //     this.relaxAge = 5;
        //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

        //         if(this.user.disablity == true){
        //                 this.relaxAge = this.relaxAge+2;
        //         }

        //         if(this.user.centralGovt == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
        //         }

        //         if(this.user.empExGref == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
        //         }

        //         this.appliedAge = this.user.age - this.relaxAge;
        //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
        //         {
        //             return this.invalidDob = false; 
        //         }
        //         else 
        //         return this.invalidDob = true;
        //     }
        // }

        // if (this.user.category == "ST") {
        //     this.relaxAge = 5;
        //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

        //         if(this.user.disablity == true){
        //                 this.relaxAge = this.relaxAge+2;
        //         }

        //         if(this.user.centralGovt == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
        //         }

        //         if(this.user.empExGref == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
        //         }

        //         this.appliedAge = this.user.age - this.relaxAge;
        //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
        //         {
        //             return this.invalidDob = false; 
        //         }
        //         else 
        //         return this.invalidDob = true;
        //     }
        // }

        // if (this.user.category == "OBC") {
        //         this.relaxAge = 2;
        //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

        //         if(this.user.disablity == true){
        //                 this.relaxAge = this.relaxAge+2;
        //         }

        //         if(this.user.centralGovt == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
        //         }

        //         if(this.user.empExGref == true) {
        //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
        //         }

        //         this.appliedAge = this.user.age - this.relaxAge;
        //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
        //         {
        //             return this.invalidDob = false; 
        //         }
        //         else 
        //         return this.invalidDob = true;
        //     }
        // }


        if (this.samePostalAddress) {
            this.user.postalAddress = this.user.permanentAddress;
            this.user.postalState = this.user.permanentState;
            this.user.postalCity = this.user.permanentCity;
            this.user.postalPincode = this.user.permanentPincode;
        }


        this.personalDetails = false;
        this.essentialDetails = true;
        this.graduationDetails = false;
        this.technicalDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.weightageDetails = false;
        this.finalSubmit = false;
        this.uploadDetails = false;
        this.activeEssential = true;
        this.paymentDetails = false;

    }
    graduationDetail() {

        if (!this.formFields.otherQualificationForm) {
            if (this.formFields.highSchoolForm) {
                if (this.formFields.high.highCourseTypeView == true) {
                    if (this.user.highCourseType == 0) {
                        return;
                    }
                }
                if(this.formFields.high.highYearView == true)
                {
                    if (this.user.highYear == 0) {
                        return;
        
                    }
                }
           if(this.formFields.high.highBoard == true)
           {
            if (this.user.highBoard == 0) {
                return;
            }
        }
            if (!this.highRequired && !this.highFileType && !this.highFileSize && this.formFields.high.highCertificateView) {
                return;
            }



             this.highPercentFlag = false;
             this.interPercentFlag = false;
            if (this.formFields.high.highPercentageView) {
              if (this.user.highPercentage < this.formFields.high.highMinPercent) {
                     this.highPercentFlag = true;
                     this.highMessage = "Your are not fullfilling criteria";
                     return this.highPercentFlag;

              }
            }
        }

            if (this.formFields.intermediateForm) {
                // if (this.user.interYear == 0 || this.user.interCourseType == 0 || this.user.interBoard == 0 ) {
                //     return;

                // }
                if (this.formFields.inter.interYearView == true) {
                    if (this.user.interYear == 0) {
                        return;

                    }
                }
                if (this.formFields.inter.interCourseTypeView == true) {
                    if (this.user.interCourseType == 0) {
                        return;
                    }
                }
                if (this.formFields.inter.interBoardView == true) {
                    if (this.user.interBoard == 0) {
                        return;
                    }
                }
                if (!this.interRequired && !this.interFileType && !this.interFileSize && this.formFields.inter.interCertificateView) {
                    return;
                }

           if (this.formFields.inter.interPercentageView) {
                if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
                    
                    this.interPercentFlag = true;
                    this.interMessage = "Your are not fullfilling criteria";
                    //alert("Invalid Percentage");
                    return this.interPercentFlag;
                }

            }

        }
            if (this.formFields.inter.additionalCertificateView) {
                if (!this.additionalRequired && !this.addFileType && !this.addFileSize && this.formFields.inter.additionalCertificateView) {
                    return;
                }
            }


            if (this.formFields.inter.firstaidCertificateView) {
                if (!this.firstAidRequired && !this.aidFileType && !this.aidFileSize && this.formFields.inter.firstaidCertificateView) {
                    return;
                }
            }

            this.technicalDetail();
        } else {
            if (this.formFields.highSchoolForm) {
                if (this.formFields.high.highCourseTypeView == true) {
                    if (this.user.highCourseType == 0) {
                        return;
                    }
                }
                if(this.formFields.high.highYearView == true)
                {
                    if (this.user.highYear == 0) {
                        return;
        
                    }
                }
           if(this.formFields.high.highBoard == true)
           {
            if (this.user.highBoard == 0) {
                return;
            }
        }
            if (!this.highRequired && !this.highFileType && !this.highFileSize && this.formFields.high.highCertificateView) {
                return;
            }

               this.highPercentFlag = false;
               this.interPercentFlag = false;
               if (this.formFields.high.highPercentageView) {
             if (this.user.highPercentage < this.formFields.high.highMinPercent) {
            //alert("Invalid Percentage");
                 this.highPercentFlag = true;
                 this.highMessage = "Your are not fullfilling criteria";
                return this.highPercentFlag;

              }
            }
        }

            if (this.formFields.intermediateForm) {
                if (this.formFields.inter.interYearView == true) {
                    if (this.user.interYear == 0) {
                        return;

                    }
                }
                if (this.formFields.inter.interCourseTypeView == true) {
                    if (this.user.interCourseType == 0) {
                        return;
                    }
                }
                if (this.formFields.inter.interBoardView == true) {
                    if (this.user.interBoard == 0) {
                        return;
                    }
                }
                if (!this.interRequired && !this.interFileType && !this.interFileSize && this.formFields.inter.interCertificateView) {
                    return;
                }
                if (this.formFields.inter.interPercentageView) {
                if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
                    this.interPercentFlag = true;
                    this.interMessage = "Your are not fullfilling criteria";
                    //alert("Invalid Percentage");
                    return this.interPercentFlag;
                }
            }
        }
            if (this.formFields.inter.additionalCertificateView) {
                if (!this.additionalRequired && !this.addFileType && !this.addFileSize && this.formFields.inter.additionalCertificateView) {
                    return;
                }
            }

            if (this.formFields.inter.firstaidCertificateView) {
                if (!this.firstAidRequired && !this.aidFileType && !this.aidFileSize && this.formFields.inter.firstaidCertificateView) {
                    return;
                }
            }

            this.graduationDetails = true;
            this.essentialDetails = false;
            this.technicalDetails = false;
            this.weightageDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeGraduation = true;
            this.paymentDetails = false;
        }
    }
    chekPercentage() {


         this.highPercentFlag = false;
         this.interPercentFlag = false;
         if (this.user.highPercentage < this.formFields.high.highMinPercent) {
        //alert("Invalid Percentage");
            this.highPercentFlag = true;
            this.highMessage = "Your are not fullfilling criteria";
           return this.highPercentFlag;

          }

        if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
            this.interPercentFlag = true;
            this.interMessage = "Your are not fullfilling criteria";
            //alert("Invalid Percentage");
            return this.interPercentFlag;
        }

        this.diplomaPercentFlag = false;
        this.degreePercentFlag = false;
        this.itiPercentFlag = false;
        if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
            this.diplomaPercentFlag = true;
            //alert("Invalid Percentage");
            this.diplomaMessage = "Your are not fullfilling criteria";
            return this.diplomaPercentFlag = true;

        }

        if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
            this.degreePercentFlag = true;
            //alert("Invalid Percentage");
            this.degreeMessage = "Your are not fullfilling criteria";
            return this.degreePercentFlag = true;

        }
        if (this.user.itiPercentage < this.formFields.iti.itiMinPercent) {
            this.itiPercentFlag = true;
            //alert("Invalid Percentage");
            this.itiMessage = "Your are not fullfilling criteria";
            return this.itiPercentFlag = true;

        }

        this.ugPercentFlag = false;
        this.pgPercentFlag = false;
        if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
            this.ugPercentFlag = true;
            // alert("Invalid Percentage");
            this.ugMessage = "Your are not fullfilling criteria";
            return this.invalidPercentage = true;

        }

        if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
            //alert("Invalid Percentage");
            this.pgPercentFlag = true;

            this.pgMessage = "Your are not fullfilling criteria";
            return this.invalidPercentage = true;

        }
    }

    technicalDetail() {

        if (!this.formFields.technicalForm) {
            if (this.formFields.graduationForm) {
                if (this.formFields.graduation.ugCourseView == true) {
                    if (this.user.ugCourse == 0) {
                        return;
                    }
                }
                if (this.formFields.graduation.ugYearView == true) {
                    if (this.user.ugPassingYear == 0) {
                        return;
                    }
                } if (this.formFields.graduation.ugUniversityView == true) {
                    if (this.user.ugUniversity == 0) {
                        return;
                    }
                } if (this.formFields.graduation.ugCourseTypeView == true) {
                    if (this.user.ugCourseType == 0) {
                        return;

                    }
                }
                if (!this.gradRequired && !this.gradFileType && !this.gradFileSize && this.formFields.graduation.ugCertificateView) {
                    return;
                }

                this.ugPercentFlag = false;
                this.pgPercentFlag = false;
                if (this.formFields.graduation.ugPercentageView) {
                    if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
                        this.ugPercentFlag = true;
                        // alert("Invalid Percentage");
                        this.ugMessage = "Your are not fullfilling criteria";
                        return this.ugPercentFlag;

                    }
                }
            }
            if (this.formFields.postGraduationForm) {
                if (this.formFields.pgraduation.pgCourseView == true) {
                    if (this.user.pgCourse == 0) {
                        return;
                    }
                } if (this.formFields.pgraduation.pgYearView == true) {
                    if (this.user.pgPassingYear == 0) {
                        return;
                    }
                } if (this.formFields.pgraduation.pgUniversityView == true) {
                    if (this.user.pgUniversity == 0) {
                        return;
                    }
                } if (this.formFields.pgraduation.pgCourseTypeView == true) {
                    if (this.user.pgCourseType == 0) {
                        return;
                    }
                }
                if (!this.postGradRequired && !this.postGradFileType && !this.postGradFileSize && this.formFields.pgraduation.pgCertificateView) {
                    return;
                }
                if (this.formFields.pgraduation.pgPercentageView) {
                    if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
                        //alert("Invalid Percentage");
                        this.pgPercentFlag = true;

                        this.pgMessage = "Your are not fullfilling criteria";
                        return this.pgPercentFlag;

                    }
                }
            }
            this.experienceDetail();
        } else {
            this.ugPercentFlag = false;
            this.pgPercentFlag = false;

            if (this.formFields.graduationForm) {
                // if (this.user.ugCourse == 0 || this.user.ugPassingYear == 0 || this.user.ugUniversity == 0 || this.user.ugCourseType == 0 ) {
                //     return;

                // }

                if (this.formFields.graduation.ugCourseView == true) {
                    if (this.user.ugCourse == 0) {
                        return;
                    }
                }
                if (this.formFields.graduation.ugYearView == true) {
                    if (this.user.ugPassingYear == 0) {
                        return;
                    }
                } if (this.formFields.graduation.ugUniversityView == true) {
                    if (this.user.ugUniversity == 0) {
                        return;
                    }
                } if (this.formFields.graduation.ugCourseTypeView == true) {
                    if (this.user.ugCourseType == 0) {
                        return;

                    }
                }
                if (!this.gradRequired && !this.gradFileType && !this.gradFileSize && this.formFields.graduation.ugCertificateView) {
                    return;
                }

                if (this.formFields.graduation.ugPercentageView) {
                    if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
                        this.ugPercentFlag = true;
                        // alert("Invalid Percentage");
                        this.ugMessage = "Your are not fullfilling criteria";
                        return this.ugPercentFlag;

                    }
                }
            }

            if (this.formFields.postGraduationForm) {
                // if (this.user.pgCourse == 0 || this.user.pgPassingYear == 0 || this.user.pgUniversity == 0 || this.user.pgCourseType == 0 ) {
                //     return;

                // }
                if (this.formFields.pgraduation.pgCourseView == true) {
                    if (this.user.pgCourse == 0) {
                        return;
                    }
                } if (this.formFields.pgraduation.pgYearView == true) {
                    if (this.user.pgPassingYear == 0) {
                        return;
                    }
                } if (this.formFields.pgraduation.pgUniversityView == true) {
                    if (this.user.pgUniversity == 0) {
                        return;
                    }
                } if (this.formFields.pgraduation.pgCourseTypeView == true) {
                    if (this.user.pgCourseType == 0) {
                        return;
                    }
                }
                if (!this.postGradRequired && !this.postGradFileType && !this.postGradFileSize && this.formFields.pgraduation.pgCertificateView) {
                    return;
                }
                if (this.formFields.pgraduation.pgPercentageView) {
                    if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
                        //alert("Invalid Percentage");
                        this.pgPercentFlag = true;

                        this.pgMessage = "Your are not fullfilling criteria";
                        return this.pgPercentFlag;

                    }
                }
            }

            this.technicalDetails = true;
            this.weightageDetails = false;
            this.essentialDetails = false;
            this.graduationDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeTechnical = true;
            this.paymentDetails = false;
        }
    }


    experienceDetail() {
        this.diplomaPercentFlag = false;
        this.degreePercentFlag = false;
        this.itiPercentFlag = false;

        if (!this.formFields.experienceForm) {
            if (this.formFields.diplomaForm) {

                if (this.formFields.diploma.diplomaBranchView == true) {
                    if (this.user.diplomaBranch == 0) {
                        return;
                    }
                }
                if (this.formFields.diploma.diplomaYearView == true) {
                    if (this.user.diplomaYear == 0) {
                        return;
                    }
                }
                if (this.formFields.diploma.diplomaUniversityView == true) {
                    if (this.user.diplomaUniversity == 0) {
                        return;
                    }
                }
                if (this.formFields.diploma.diplomaCourseTypeView == true) {
                    if (this.user.diplomaCourseType == 0) {
                        return;

                    }
                }
                if (!this.diplomaRequired && !this.diplomaFileType && !this.diplomaFileSize && this.formFields.diploma.diplomaCertificateView) {
                    return;
                }
                if (this.formFields.diploma.diplomaPercentageView) {
                    if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
                        this.diplomaPercentFlag = true;
                        this.diplomaMessage = "Your are not fullfilling criteria";
                        return this.diplomaPercentFlag;

                    }
                }
            }
            if (this.formFields.degreeForm) {
                // if (this.user.degreeBranch == 0 || this.user.degreeYear == 0 || this.user.degreeUniversity == 0 || this.user.degreeCourseType == 0 ) {
                //     return;

                // }
                if (this.formFields.degree.degreeBranchView == true) {
                    if (this.user.degreeBranch == 0) {
                        return;
                    }
                }
                if (this.formFields.degree.degreeYearView == true) {
                    if (this.user.degreeYear == 0) {
                        return;
                    }
                }
                if (this.formFields.degree.degreeUniversityView == true) {
                    if (this.user.degreeUniversity == 0) {
                        return;
                    }
                }
                if (this.formFields.degree.degreeCourseTypeView == true) {
                    if (this.user.degreeCourseType == 0) {
                        return;

                    }
                }
                if (this.formFields.degree.degreePercentageView) {
                    if (!this.degreeRequired && !this.techFileType && !this.techFileSize && this.formFields.degree.degreeCertificateView) {
                        return;
                    }
                    if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
                        this.degreePercentFlag = true;
                        this.degreeMessage = "Your are not fullfilling criteria";
                        return this.degreePercentFlag;
                    }
                }
            }
            if (this.formFields.itiForm) {
                // if (this.user.itiBranch == 0 || this.user.itiYear == 0 || this.user.itiUniversity == 0 || this.user.itiCourseType == 0 ) {
                //     return;

                // }
                if (this.formFields.iti.itiBranchView == true) {

                    if (this.user.itiBranch == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiYearView == true) {
                    if (this.user.itiYear == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiUniversityView == true) {
                    if (this.user.itiUniversity == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiCourseTypeView == true) {
                    if (this.user.itiCourseType == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiPercentageView) {
                    if (!this.itiRequired && !this.itiFileType && !this.itiFileSize && this.formFields.iti.itiCertificateView) {
                        return;
                    }

                    if (this.user.itiPercentage < this.formFields.iti.itiMinPercent) {
                        this.itiPercentFlag = true;
                        this.itiMessage = "Your are not fullfilling criteria";
                        return this.itiPercentFlag;
                    }
                }
            }

            this.drivingDetail();
        }
        else {
            if (this.formFields.diplomaForm) {
                // if (this.user.diplomaBranch == 0 || this.user.diplomaYear == 0 || this.user.diplomaUniversity == 0 || this.user.diplomaCourseType == 0 ) {
                //     return;

                // }
                if (this.formFields.diploma.diplomaBranchView == true) {
                    if (this.user.diplomaBranch == 0) {
                        return;
                    }
                }
                if (this.formFields.diploma.diplomaYearView == true) {
                    if (this.user.diplomaYear == 0) {
                        return;
                    }
                }
                if (this.formFields.diploma.diplomaUniversityView == true) {
                    if (this.user.diplomaUniversity == 0) {
                        return;
                    }
                }
                if (this.formFields.diploma.diplomaCourseTypeView == true) {
                    if (this.user.diplomaCourseType == 0) {
                        return;

                    }
                }
                if (!this.diplomaRequired && !this.diplomaFileType && !this.diplomaFileSize && this.formFields.diploma.diplomaCertificateView) {
                    return;
                }
                if (this.formFields.diploma.diplomaPercentageView) {
                    if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
                        this.diplomaPercentFlag = true;
                        this.diplomaMessage = "Your are not fullfilling criteria";
                        return this.diplomaPercentFlag;

                    }
                }
            }
            if (this.formFields.degreeForm) {
                // if (this.user.degreeBranch == 0 || this.user.degreeYear == 0 || this.user.degreeUniversity == 0 || this.user.degreeCourseType == 0 ) {
                //     return;

                // }
                if (this.formFields.degree.degreeBranchView == true) {
                    if (this.user.degreeBranch == 0) {
                        return;
                    }
                }
                if (this.formFields.degree.degreeYearView == true) {
                    if (this.user.degreeYear == 0) {
                        return;
                    }
                }
                if (this.formFields.degree.degreeUniversityView == true) {
                    if (this.user.degreeUniversity == 0) {
                        return;
                    }
                }
                if (this.formFields.degree.degreeCourseTypeView == true) {
                    if (this.user.degreeCourseType == 0) {
                        return;

                    }
                }
                if (this.formFields.degree.degreePercentageView) {
                    if (!this.degreeRequired && !this.techFileType && !this.techFileSize && this.formFields.degree.degreeCertificateView) {
                        return;
                    }

                    if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
                        this.degreePercentFlag = true;
                        this.degreeMessage = "Your are not fullfilling criteria";
                        return this.degreePercentFlag;
                    }
                }
            }
            if (this.formFields.itiForm) {
                // if (this.user.itiBranch == 0 || this.user.itiYear == 0 || this.user.itiUniversity == 0 || this.user.itiCourseType == 0 ) {
                //     return;

                // }
                if (this.formFields.iti.itiBranchView == true) {

                    if (this.user.itiBranch == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiYearView == true) {
                    if (this.user.itiYear == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiUniversityView == true) {
                    if (this.user.itiUniversity == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiCourseTypeView == true) {
                    if (this.user.itiCourseType == 0) {
                        return;
                    }
                }
                if (this.formFields.iti.itiPercentageView) {
                    if (!this.itiRequired && !this.itiFileType && !this.itiFileSize && this.formFields.iti.itiCertificateView) {
                        return;
                    }

                    if (this.user.itiPercentage < this.formFields.iti.itiMinPercent) {
                        this.itiPercentFlag = true;
                        this.itiMessage = "Your are not fullfilling criteria";
                        return this.itiPercentFlag;
                    }
                }
            }
            this.experienceDetails = true;
            this.essentialDetails = false;
            this.graduationDetails = false;
            this.technicalDetails = false;
            this.weightageDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeExperience = true;
            this.paymentDetails = false;
        }
    }

    drivingDetail() {
        if (!this.formFields.drivingLicenseForm) {
            if (this.formFields.experienceForm) {
                if (!this.formFields.experienceCertificateForm) {
                    if (!this.experienceRequired && !this.expFileType && !this.expFileSize && this.formFields.experienceCertificateForm) {
                        return;
                    }
                }

                this.invalidexperience = false;
                if (this.user.employmentFrom >= this.user.employmentTo) {
                    this.invalidexperience = true;
                    this.experienceMessage = "Invalid Period";
                    return this.invalidexperience;

                }
            }
            this.weightageDetail();
        }
        else {
            this.uploadDetails = false;
            this.graduationDetails = false;
            this.essentialDetails = false;
            this.technicalDetails = false;
            this.weightageDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = true;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.activeDriving = true;
            this.paymentDetails = false;
        }
    }

    weightageDetail()
    {
        if (this.formFields.drivingLicenseForm) {

            if (!this.drivingRequired && !this.drivingFileType && !this.drivingFileSize && this.formFields.drivingExpUploadView) {
                return;
            }
            if (!this.licenseRequired && !this.licenseFileSize && !this.licenseFileType && this.formFields.drivingUploadView) {
                return;
            }

            this.invaliddlmv = false;
            if (this.user.issueDateLmv >= this.user.validUptoLmv) {
                this.invaliddlmv = true;
                this.dlmvMessage = "Invalid dLMV";
                return this.invaliddlmv;
            }

            this.invaliddhmv = false;
            if (this.user.issueDateHgmv >= this.user.validUptoHgmv) {
                this.invaliddhmv = true;
                this.dhmvMessage = "Invalid dHMV";
                return this.invaliddhmv;
            }
            this.invalidroad = false;
            if (this.user.issueDateRoad >= this.user.validUptoRoad) {
                this.invalidroad = true;
                this.roadMessage = "Invalid rr";
                return this.invalidroad;
            }
            this.invalidoeg = false;
            if (this.user.issueDateOeg >= this.user.validUptoOeg) {
                this.invalidoeg = true;
                this.oegMessage = "Invalid oeg";
                return this.invalidoeg;
            }

            this.technicalDetails = false;
            this.weightageDetails = true;
            this.essentialDetails = false;
            this.graduationDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeWeightage = true;
            this.paymentDetails = false;
        } else {
            this.technicalDetails = false;
            this.weightageDetails = true;
            this.essentialDetails = false;
            this.graduationDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeWeightage = true;
            this.paymentDetails = false;
        }
    }
    addDocument() {
         if (!this.nccBCheck && !this.nccbfileRequired && !this.nccbFileSize && !this.nccbFileType) {
            return;
        }
        if (!this.nccCCheck && !this.nccfileRequired && !this.ncccFileType && !this.ncccFileSize) {
            return;
        }
        if (!this.sportsCheck && !this.sportsfileRequired && !this.sportsFileType && !this.sportsFileSize) {
            return;
        }
        if (!this.sonGPSCheck && !this.unitfileRequired && !this.unitFileType && !this.unitFileSize)
        {
            return;
        }
        if(!this.exServiceCheck && !this.lastUnitfileRequired && !this.lastUnitFileType && !this.lastUnitFileSize)
        {
            return;
        }
        if(!this.serviceCheck && !this.dischargefileRequired && !this.dischargeFileType && !this.dischargeFileSize)
        {
            return;
        }

        // if (this.formFields.drivingLicenseForm) {

        //     if (!this.drivingRequired && !this.drivingFileType && !this.drivingFileSize && this.formFields.drivingExpUploadView) {
        //         return;
        //     }
        //     if (!this.licenseRequired && !this.licenseFileSize && !this.licenseFileType && this.formFields.drivingUploadView) {
        //         return;
        //     }

        //     this.invaliddlmv = false;
        //     if (this.user.issueDateLmv >= this.user.validUptoLmv) {
        //         this.invaliddlmv = true;
        //         this.dlmvMessage = "Invalid dLMV";
        //         return this.invaliddlmv;
        //     }

        //     this.invaliddhmv = false;
        //     if (this.user.issueDateHgmv >= this.user.validUptoHgmv) {
        //         this.invaliddhmv = true;
        //         this.dhmvMessage = "Invalid dHMV";
        //         return this.invaliddhmv;
        //     }
        //     this.invalidroad = false;
        //     if (this.user.issueDateRoad >= this.user.validUptoRoad) {
        //         this.invalidroad = true;
        //         this.roadMessage = "Invalid rr";
        //         return this.invalidroad;
        //     }
        //     this.invalidoeg = false;
        //     if (this.user.issueDateOeg >= this.user.validUptoOeg) {
        //         this.invalidoeg = true;
        //         this.oegMessage = "Invalid oeg";
        //         return this.invalidoeg;
        //     }

            this.uploadDetails = true;
            this.graduationDetails = false;
            this.essentialDetails = false;
            this.technicalDetails = false;
            this.weightageDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.basicDetails = false;
            this.finalSubmit = false;
            this.activeUpload = true;
            this.paymentDetails = false;
       
    }

    preview() {

        this.loading = true;
        this.uploadDetails = false;
        this.graduationDetails = false;
        this.essentialDetails = false;
        this.technicalDetails = false;
        this.weightageDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.personalDetails = false;
        this.finalSubmit = true;
        this.basicDetails = false;
        this.activePreview = true;
        this.paymentDetails = false;
        this.getPersonalDetails();

        this.loading = false;
        // this.user.dateOfBirth = this.datePipe.transform(this.user.dateOfBirth, 'dd-MM-yyyy');
        // this.user.employmentFrom = this.datePipe.transform(this.user.employmentFrom, 'dd-MM-yyyy');
        // this.user.user.employmentTo = this.datePipe.transform(this.user.employmentTo, 'dd-MM-yyyy');
        // this.user.issueDateLmv = this.datePipe.transform(this.user.issueDateLmv, 'dd-MM-yyyy');
        // this.user.validUptoLmv = this.datePipe.transform(this.user.validUptoLmv, 'dd-MM-yyyy');
        // this.user.issueDateHgmv = this.datePipe.transform(this.user.issueDateHgmv, 'dd-MM-yyyy');
        // this.user.validUptoHgmvView = this.datePipe.transform(this.user.validUptoHgmvView, 'dd-MM-yyyy');

    }

    payment() {
        this.uploadDetails = false;
        this.graduationDetails = false;
        this.essentialDetails = false;
        this.technicalDetails = false;
        this.weightageDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.personalDetails = false;
        this.finalSubmit = false;
        this.basicDetails = false;
        this.activePayment = true;
        this.paymentDetails = true;

    }


    uploadImage() {
        if (this.createSuccess && !this.createError) {
            this.preview();
        }
        if (!this.photoRequired && !this.photoType && !this.photoSize) {
            return;
        }
        if (!this.signRequired && !this.signType && !this.signSize) {
            return;
        }

        this.previewButton = true;
        this.createError = false;
        this.loading = true;
        let now = new Date().getTime().toString().slice(7);
        if (this.user.mobileNumber > 0) {
            let mobile = (this.user.mobileNumber).slice(6)
            this.user.registrationId = "BRO" + now + mobile;
        }
        this.user.jobId = this.jobs.id;
        this.user.payment = false;
        this.user.finalSubmit = false;
        this.postService.createCandidate(this.user).subscribe(data => {
            console.log("Saved data", data);
            this.previewButton = false;
            this.createSuccess = true;
            this.loading = false;
        },
            error => {
                this.createSuccess = false;
                this.createError = true;
                this.previewButton = false;
                this.loading = false;
            });

    }

    emailCheck(email: string) {
        this.getService.emailCheck(email).subscribe(
            data => {
                console.log("Email check :", data);
                // this.activeJobs = data;
                this.emailExists = data.present;
                //   this.alertService.success('Registration successful', true);
                // this.router.navigate(['/login']);
            },
            error => {
                //   this.alertService.error(error);
                // this.loading = false;
            });
    }
    // private processError(response) {
    //     this.success = null;
    //     if (response.status === 400 && response._body === 'login already in use') {
    //         this.errorUserExists = 'ERROR';
    //     } else if (response.status === 400 && response._body === 'email address already in use') {
    //         this.errorEmailExists = 'ERROR';
    //     } else {
    //         this.error = 'ERROR';
    //     }
    // }

    // setAge(categ) {
    //     for (let i = 0; i < this.amountFields.length; i++) {
    //         if (this.amountFields[i].categoryName == "UR") {
    //             // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //             this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //             // this.minDate.setDate(this.amountFields[i].minAgeDate);
    //             this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         }
    //     }

    // }




    // setAge(categ) {

    //     this.ageInvalid = false;

    //     for (let i = 0; i < this.amountFields.length; i++) {
    //         // if (this.amountFields[i].categoryName == "UR") {
    //         //     if(categ == "General"){
    //         //     this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     // this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     // this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }

    //         // if (this.amountFields[i].categoryName == "SC") {
    //         //     if(categ == "SC"){
    //         //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }

    //         // if (this.amountFields[i].categoryName == "ST") {
    //         //     if(categ == "ST"){
    //         //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }

    //         // if (this.amountFields[i].categoryName == "OBC") {
    //         //     if(categ == "OBC"){
    //         //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }
    //         if (this.amountFields[i].categoryName == "SC") {

    //             if (categ == "SC") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if ((finalYear < minValue || finalYear > maxValue)) {
    //                         this.ageMessage = "Your age is Invalid! sc1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! sc2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }

    //         if (this.amountFields[i].categoryName == "ST") {
    //             if (categ == "ST") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if (finalYear < minValue || finalYear > maxValue) {
    //                         this.ageMessage = "Your age is Invalid! st1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! st2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }

    //         if (this.amountFields[i].categoryName == "OBC") {
    //             if (categ == "OBC") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if (finalYear < minValue || finalYear > maxValue) {
    //                         this.ageMessage = "Your age is Invalid! obc1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! obc2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }
    //         if (this.amountFields[i].categoryName == "UR") {
    //             if (categ == "General") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if (finalYear < minValue || finalYear > maxValue) {
    //                         this.ageMessage = "Your age is Invalid! ur1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! ur2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    getAge(value) {

        var now = new Date(); //Todays Date   
        // var birthday = dob;
        let birthday: any = this.datePipe.transform(value, 'yyyy-MM-dd');
        birthday = birthday.split("-");

        var dobYear = birthday[0];
        var dobMonth = birthday[1];
        var dobDay = birthday[2];

        var nowDay = now.getDate();
        var nowMonth = now.getMonth() + 1;  //jan=0 so month+1
        var nowYear = now.getFullYear();

        var ageyear = nowYear - dobYear;
        var agemonth = nowMonth - dobMonth;
        var ageday = nowDay - dobDay;
        if (agemonth < 0) {
            ageyear--;
            agemonth = (12 + agemonth);
        }
        if (nowDay < dobDay) {
            agemonth--;
            ageday = 30 + ageday;
        }

        this.user.age = ageyear + " years, " + agemonth + " months, " + ageday + " days";

        this.calyear = ageyear;

    }
    restrictNumeric(event) {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }


    getHighPercentage() {
        this.highObtainGreater = false;
        if (this.user.highObtainMarks !== null && this.user.highTotalMarks !== null && this.user.highObtainMarks !== undefined && this.user.highTotalMarks !== undefined) {
            if (parseInt(this.user.highObtainMarks) > parseInt(this.user.highTotalMarks)) {
                return this.highObtainGreater = true;
            } else {
                this.user.highPercentage = (this.user.highObtainMarks / this.user.highTotalMarks) * 100;
            }

            this.user.highPercentage = this.decimalPipe.transform(this.user.highPercentage, '1.2-2');
            
        }
        // == is used to check for equality doesn't matter type and === matters for the type also.
    }

    getInterPercentage() {
        this.interObtainGreater = false;
        if (this.user.interObtainMarks !== null && this.user.interTotalMarks !== null && this.user.interObtainMarks !== undefined && this.user.interTotalMarks !== undefined) {
            if (parseInt(this.user.interObtainMarks) > parseInt(this.user.interTotalMarks)) {
                return this.interObtainGreater = true;
            } else {
                this.user.interPercentage = (this.user.interObtainMarks / this.user.interTotalMarks) * 100;
            }

            this.user.interPercentage = this.decimalPipe.transform(this.user.interPercentage, '1.2-2');
            
        }
        // == is used to check for equality doesn't matter type and === matters for the type also.
    }

    getUgPercentage() {
        this.ugObtainGreater = false;
        if (this.user.ugObtainMarks !== null && this.user.ugTotalMarks !== null && this.user.ugObtainMarks !== undefined && this.user.ugTotalMarks !== undefined) {
            if (parseInt(this.user.ugObtainMarks) > parseInt(this.user.ugTotalMarks)) {
                return this.ugObtainGreater = true;
            } else {
                this.user.ugPercentage = (this.user.ugObtainMarks / this.user.ugTotalMarks) * 100;
            }

            this.user.ugPercentage = this.decimalPipe.transform(this.user.ugPercentage, '1.2-2');
        }
    }

    getPgPercentage() {
        this.pgObtainGreater = false;
        if (this.user.pgObtainMarks !== null && this.user.pgTotalMarks !== null && this.user.pgObtainMarks !== undefined && this.user.pgTotalMarks !== undefined) {
            if (parseInt(this.user.pgObtainMarks) > parseInt(this.user.pgTotalMarks)) {
                return this.pgObtainGreater = true;
            } else {
                this.user.pgPercentage = (this.user.pgObtainMarks / this.user.pgTotalMarks) * 100;
            }

            this.user.pgPercentage = this.decimalPipe.transform(this.user.pgPercentage, '1.2-2');
        }
    }

    getDiplomaPercentage() {
        this.diplomaObtainGreater = false;
        if (this.user.diplomaObtainMarks !== null && this.user.diplomaTotalMarks !== null && this.user.diplomaObtainMarks !== undefined && this.user.diplomaTotalMarks !== undefined) {
            if (parseInt(this.user.diplomaObtainMarks) > parseInt(this.user.diplomaTotalMarks)) {
                return this.diplomaObtainGreater = true;
            } else {
                this.user.diplomaPercentage = (this.user.diplomaObtainMarks / this.user.diplomaTotalMarks) * 100;
            }

            this.user.diplomaPercentage = this.decimalPipe.transform(this.user.diplomaPercentage, '1.2-2');
        }
    }

    getDegreePercentage() {
        if (this.user.degreeMarksObtain !== null && this.user.degreeTotalMarks !== null && this.user.degreeMarksObtain !== undefined && this.user.degreeTotalMarks !== undefined) {
            if (parseInt(this.user.degreeMarksObtain) > parseInt(this.user.degreeTotalMarks)) {
                return this.degreeObtainGreater = true;
            } else {
                this.user.degreePercentage = (this.user.degreeMarksObtain / this.user.degreeTotalMarks) * 100;
            }

            this.user.degreePercentage = this.decimalPipe.transform(this.user.degreePercentage, '1.2-2');
        }
    }
    getItiPercentage() {
        if (this.user.itiMarksObtain !== null && this.user.itiTotalMarks !== null && this.user.itiMarksObtain !== undefined && this.user.itiTotalMarks !== undefined) {
            if (parseInt(this.user.itiMarksObtain) > parseInt(this.user.itiTotalMarks)) {
                return this.itiObtainGreater = true;
            } else {
                this.user.itiPercentage = (this.user.itiMarksObtain / this.user.itiTotalMarks) * 100;
            }

            this.user.itiPercentage = this.decimalPipe.transform(this.user.itiPercentage, '1.2-2');
        }
    }

    categoryfileChange(event) {
        this.categoryFileType = false;
        this.categoryFileSize = false;
        this.categoryRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.categoryRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.categoryCertificateDoc = file;

            //between 25 to 30k
            if (25600 < file.size && file.size < 30720) {

                if (matchType) {
                    this.user.categoryCertificateDoc = file;

                }
                else {
                    return this.categoryFileType;
                }


            }
            else {
                return this.categoryFileSize && this.categoryRequired;
            }

        }
        this.categoryRequired = true;
    }

    nationfileChange(event) {
        this.nationFileType = false;
        this.nationFileSize = false;
        this.nationRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.nationRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nationCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.nationCertificateDoc = file;
                }
                else {
                    return this.nationFileType;
                }
            }
            else {
                return this.nationFileSize && this.nationRequired;
            }
        }

        return this.nationRequired = true;
    }

    aadhaarfileChange(event) {
        this.aadhaarFileType = false;
        this.aadhaarFileSize = false;
        this.aadhaarfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.aadhaarfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.aadhaarCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.aadhaarCertificateDoc = file;
                }
                else {
                    return this.aadhaarFileType;
                }
            }
            else {
                return this.aadhaarFileSize && this.aadhaarfileRequired;
            }
        }

        return this.aadhaarfileRequired = true;
    }


    cplfileChange(event) {
        this.cplFileType = false;
        this.cplFileSize = false;
        this.cplfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.aadhaarfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.aadhaarCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.cplCertificateDoc = file;
                }
                else {
                    return this.cplFileType;
                }
            }
            else {
                return this.cplFileSize && this.cplfileRequired;
            }
        }

        return this.cplfileRequired = true;
    }

    panfileChange(event) {
        this.panFileType = false;
        this.panFileSize = false;
        this.panRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //   this.highRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.highCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.panCertificateDoc = file;
                }
                else {
                    return this.panFileType;
                }
            }
            else {
                return this.panFileSize && this.panRequired;
            }
        }

        return this.panRequired = true;
    }


    centralfileChange(event) {
        this.centralFileType = false;
        this.centralFileSize = false;
        this.centralfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.centralfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.centralCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.centralCertificateDoc = file;
                }
                else {
                    return this.centralFileType;
                }
            }
            else {
                return this.centralFileSize && this.centralfileRequired;
            }

        }
        return this.centralfileRequired = true;
    }

    armedfileChange(event) {
        this.armedFileType = false;
        this.armedFileSize = false;
        this.armedfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.armedfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.armedCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.armedCertificateDoc = file;
                }
                else {
                    return this.armedFileType;
                }
            }
            else {
                return this.armedFileSize && this.armedfileRequired;
            }
        }

        return this.armedfileRequired = true;
    }


    nccbfileChange(event) {
        this.nccbFileSize = false;
        this.nccbFileType = false;
        this.nccbfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.nccbfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nccBCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.nccBCertificateDoc = file;
                }
                else {
                    return this.nccbFileType;
                }
            }
            else {
                return this.nccbFileSize && this.nccbfileRequired;
            }
        }
        return this.nccbfileRequired = true;
    }

    ncccfileChange(event) {
        this.ncccFileType = false;
        this.ncccFileSize = false;
        this.nccfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.nccfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nccCCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.nccCCertificateDoc = file;
                }
                else {
                    return this.ncccFileType;
                }
            }
            else {
                return this.ncccFileSize && this.nccfileRequired;
            }
        }

        return this.nccfileRequired = true;
    }

    sportsfileChange(event) {
        this.sportsFileType = false;
        this.sportsFileSize = false;
        this.sportsfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.sportsfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.sportsManDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.sportsManDoc = file;
                }
                else {
                    return this.sportsFileType;
                }
            }
            else {
                return this.sportsFileSize && this.sportsfileRequired;
            }
        }

        return this.sportsfileRequired = true;
    }

    unitfileChange(event) {
        this.unitFileSize = false;
        this.unitFileType = false;
        this.unitfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.nccbfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nccBCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.unitCertificateDoc = file;
                }
                else {
                    return this.unitFileType;
                }
            }
            else {
                return this.unitFileSize && this.unitfileRequired;
            }
        }
        return this.unitfileRequired = true;
    }

    dischargefileChange(event) {
        this.dischargeFileSize = false;
        this.dischargeFileType = false;
        this.dischargefileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.nccbfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nccBCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.dischargeCertificateDoc = file;
                }
                else {
                    return this.dischargeFileType;
                }
            }
            else {
                return this.dischargeFileSize && this.dischargefileRequired;
            }
        }
        return this.dischargefileRequired = true;
    }


    lastUnitfileChange(event) {
        this.lastUnitFileSize = false;
        this.lastUnitFileType = false;
        this.lastUnitfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.nccbfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nccBCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.lastUnitCertificateDoc = file;
                }
                else {
                    return this.lastUnitFileType;
                }
            }
            else {
                return this.lastUnitFileSize && this.lastUnitfileRequired;
            }
        }
        return this.lastUnitfileRequired = true;
    }




    highfileChange(event) {
        this.highFileType = false;
        this.highFileSize = false;
        this.highRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //   this.highRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.highCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.highCertificateDoc = file;
                }
                else {
                    return this.highFileType;
                }
            }
            else {
                return this.highFileSize && this.highRequired;
            }
        }

        return this.highRequired = true;
    }


    interfileChange(event) {
        this.interFileType = false;
        this.interFileSize = false;
        this.interRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //  this.interRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.interCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.interCertificateDoc = file;
                }
                else {
                    return this.interFileType;
                }
            }
            else {
                return this.interFileSize && this.interRequired;
            }
        }

        return this.interRequired = true;
    }

    addfileChange(event) {
        this.addFileType = false;
        this.addFileSize = false;
        this.additionalRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //   this.additionalRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.additionalCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.additionalCertificateDoc = file;
                }
                else {
                    return this.addFileType;
                }
            }
            else {
                return this.addFileSize && this.additionalRequired;
            }
        }

        return this.additionalRequired = true;
    }

    aidfileChange(event) {
        this.aidFileType = false;
        this.aidFileSize = false;
        this.firstAidRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.firstAidRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.firstaidCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.firstaidCertificateDoc = file;
                }
                else {
                    return this.aidFileType;
                }
            }
            else {
                return this.aidFileSize && this.firstAidRequired;
            }
        }

        return this.firstAidRequired = true;
    }

    gradfileChange(event) {
        this.gradFileType = false;
        this.gradFileSize = false;
        this.gradRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.gradRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.ugCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.ugCertificateDoc = file;
                }
                else {
                    return this.gradFileType;
                }
            }
            else {
                return this.gradFileSize && this.gradRequired;
            }
        }

        return this.gradRequired = true;
    }

    postGradfileChange(event) {
        this.postGradFileType = false;
        this.postGradFileSize = false;
        this.postGradRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.postGradRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.pgCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.pgCertificateDoc = file;
                }
                else {
                    return this.postGradFileType;
                }
            }
            else {
                return this.postGradFileSize && this.postGradRequired;
            }
        }
        return this.postGradRequired = true;
    }

    techfileChange(event) {
        this.techFileType = false;
        this.techFileSize = false;
        this.degreeRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //  this.degreeRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.degreeCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.degreeCertificateDoc = file;
                }
                else {
                    return this.techFileType;
                }
            }
            else {
                return this.techFileSize && this.degreeRequired;
            }
        }
        return this.degreeRequired = true;
    }

    itifileChange(event) {
        this.itiFileType = false;
        this.itiFileSize = false;
        this.itiRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //  this.itiRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.itiCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.itiCertificateDoc = file;
                }
                else {
                    return this.itiFileType;
                }
            }
            else {
                return this.itiFileSize && this.itiRequired;
            }
        }

        return this.itiRequired = true;
    }


    diplomafileChange(event) {
        this.diplomaFileType = false;
        this.diplomaFileSize = false;
        this.diplomaRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //  this.diplomaRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.diplomaCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.diplomaCertificateDoc = file;
                }
                else {
                    return this.diplomaFileType;
                }
            }
            else {
                return this.diplomaFileSize && this.diplomaRequired;
            }
        }
        return this.diplomaRequired = true;
    }


    signChange(event) {
        this.signType = false;
        this.signSize = false;
        this.signRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            //this.signRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.browseSignatureDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.browseSignatureDoc = file;
                }
                else {
                    return this.signType;
                }
            }
            else {
                return this.signSize && this.signRequired;
            }
        }

        return this.signRequired = true;
    }

    photoChange(event) {
        this.photoType = false;
        this.photoSize = false;
        this.photoRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.photoRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.browsePhotoDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.browsePhotoDoc = file;
                }
                else {
                    return this.photoType;
                }
            }
            else {
                return this.photoSize && this.photoRequired;
            }
        }

        return this.photoRequired = true;
    }



    experienceChange(event) {
        this.expFileType = false;
        this.expFileSize = false;
        this.experienceRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.experienceRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            this.user.experienceCertificateDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.experienceCertificateDoc = file;
                }
                else {
                    return this.expFileType;
                }
            }
            else {
                return this.expFileSize && this.experienceRequired;
            }
        }
        return this.experienceRequired = true;
    }



    drivingChange(event) {
        this.drivingFileType = false;
        this.drivingFileSize = false;
        this.drivingRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.drivingRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.drivingExperienceDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.drivingExperienceDoc = file;
                }
                else {
                    return this.drivingFileType;
                }
            }
            else {
                return this.drivingFileSize && this.drivingRequired;
            }

        }

        return this.drivingRequired = true;
    }



    licenseChange(event) {
        this.licenseFileSize = false;
        this.licenseFileType = false;
        this.licenseRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            // this.licenseRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.drivingLicenseDoc = file;
            if (25600 < file.size && file.size < 30720) {
                if (matchType) {
                    this.user.drivingLicenseDoc = file;
                }
                else {
                    return this.licenseFileType;
                }
            }
            else {
                return this.licenseFileSize && this.licenseRequired;
            }
        }

        return this.licenseRequired = true;
    }


    adharChange(value: boolean) {
        if (value) {
            this.adharCheck = false;
            
        } else {
            this.adharCheck = true;
            this.user.aadhaarCard = '';
            this.user.aadhaarNumber = '';
        }
    }

    cplChange(value: boolean) {
        if (value) {
            this.cplCheck = false;
            
        } else {
            this.cplCheck = true;
            this.user.casualplabour = '';
            
        }
    }

    communityChange(value: boolean) {
        if (value) {
            this.communityCheck = false;
        } else {
            this.communityCheck = true;
            this.user.communityName = 0;
        }
    }

    addressChange() {
        if (this.sameAddressCheck) {
            this.samePostalAddress = true;
        }
        else {
            this.samePostalAddress = false;
        }
    }

    armedChange(value: boolean) {
        if (value) {
            this.armedCheck = false;
        } else {
            this.armedCheck = true;
            this.user.armedForce = '';
            this.user.armedFrom = '';
            this.user.armedTo = '';
            this.user.armedarmyNumber = '';
        }
    }

    centralChange(value: boolean) {
        if (value) {
            this.centralCheck = false;
        } else {
            this.centralCheck = true;
            this.user.centralGovt = '';
            this.user.servedFrom = '';
            this.user.servedTo = '';
        }
    }

    empExBroChange(value: boolean) {
        if (value) {
            this.empExBroCheck = false;
        } else {
            this.empExBroCheck = true;
            this.user.empExGref = '';
            this.user.gsNumber = '';
            this.user.servedBroFrom = '';
            this.user.servedBroTo = '';
        }
    }

    exServiceChange(value: boolean) {
        if (value) {
            this.exServiceCheck = false;
        } else {
            this.exServiceCheck = true;
            this.user.armyNumber = '';
            this.user.armyRank = '';
            this.user.armyLastUnit = '';
            this.user.armyName ='';
        }
    }

    serviceChange(value: boolean) {
        if (value) {
            this.serviceCheck = false;
        } else {
            this.serviceCheck = true;
        }
    }
    sonGPSChange(value: boolean) {
        if (value) {
            this.sonGPSCheck = false;
        } else {
            this.sonGPSCheck = true;
            this.user.sonGpsRank = '';
            this.user.sonGpsName = '';
            this.user.sonUnitName = '';
            this.user.sonGsNumber ='';
        }
    }


    reapptCandidateChange(value: boolean) {
        if (value) {
            this.reapptCheck = false;
        } else {
            this.reapptCheck = true;
            this.user.reapptCanidateGs = '';
            this.user.reaaptCandidateRank = '';
            this.user.reapptCandidateName = '';
            this.user.reapptCandidateUnit ='';
        }
    }

    broGREFChange(value: boolean) {
        if (value) {
            this.broGREFCheck = false;
        } else {
            this.broGREFCheck = true;
            this.user.brotherSisterNumber = '';
            this.user.brotherSisterRank = '';
            this.user.brotherSisterName = '';
        }
    }

    nccBChange(value: boolean) {
        if (value) {
            this.nccBCheck = false;
        } else {
            this.nccBCheck = true;
            this.user.nccB = '';
        }
    }

    nccCChange(value: boolean) {
        if (value) {
            this.nccCCheck = false;
        } else {
            this.nccCCheck = true;
            this.user.nccC = '';
        }
    }


    sportsChange(value: boolean) {
        if (value) {
            this.sportsCheck = false;
        } else {
            this.sportsCheck = true;
            this.user.sportsman = '';
        }
    }

    degreeChange(value: boolean) {
        if (value) {
            this.degreeCheck = false;
        } else {
            this.degreeCheck = true;
        }
    }
    itiChange(value: boolean) {
        if (value) {
            this.itiCheck = false;
        } else {
            this.itiCheck = true;
        }
    }

    disableChange(value: boolean) {
        if (value) {
            this.disableCheck = false;
        } else {
            this.disableCheck = true;
           // this.user.disablity = '';
            this.user.disabilitySpecify='';
        }
    }

    gradeChange(value: boolean) {
        if (value) {
            this.highGradeCheck = false;
            this.totalCheck = true;
            this.obtainCheck = true;
            this.user.highTotalMarks='';
            this.user.highObtainMarks='';
            
        }
        else {
            this.highGradeCheck = true;
            this.totalCheck = false;
            this.obtainCheck = false;
            this.user.highGrade='';
           
        }
    }

    typingChange(value)
    {
        if(value)
        {
            this.hindiTypingCheck = false;
            this.englishTypingCheck = true;
        }else{
            this.hindiTypingCheck = true;
            this.englishTypingCheck = false;
        }
       
    }

    stenographyChange(value)
    {
        if(value)
        {
            this.hindiStenographyCheck = false;
            this.englishStenographyCheck = true;
        }else{
            this.hindiStenographyCheck = true;
            this.englishStenographyCheck = false;
        }
       
    }
    

    iGradeChange(value: boolean) {
        if (value) {
            this.interGradeCheck = false;
            this.intertotalCheck = true;
            this.interobtainCheck = true;
            this.user.interTotalMarks='';
            this.user.interObtainMarks='';
            this.user.interPercentage='';        }
        else {
            this.interGradeCheck = true;
            this.intertotalCheck = false;
            this.interobtainCheck = false;

        }

    }

    drivingLicenseChange(value:boolean)
    {
        if(value)
        {
            this.roadRollerCheck = true;
            this.hmvLicenseCheck = false;
        }else{
            this.roadRollerCheck = false;
            this.hmvLicenseCheck = true;
        }
    }


    public coursetypes: any = [
        { id: 0, name: "Regular", value: "Regular" },
        { id: 1, name: "Private", value: "Private" },
        { id: 2, name: "Vocational", value: "Vocational" },
    ]

    public courses: any = [
        { id: 0, name: "Private", value: "Private" },
        { id: 1, name: "Regular", value: "Regular" },
    ];

    public genders: any = [
        { id: 0, name: "Male", value: "Male" },
        { id: 1, name: "Female", value: "Female" },
    ];


    public communities: any = [
        { id: 0, name: "Muslim" },
        { id: 1, name: "Christians" },
        { id: 2, name: "Sikh" },
        { id: 3, name: "Budh" },
        { id: 4, name: "Zoroas(Parsis)" }
    ];

    public nationalities: any = [
        { id: 0, name: "Indian" },
        { id: 1, name: "Other" }
    ];

    public marital: any = [
        { id: 0, name: "Married" },
        { id: 1, name: "Unmarried" }
    ];
    public sports: any = [
        { id: 0, name: "National" },
        { id: 1, name: "State" },
        { id: 2, name: "Distict" },
        { id: 3, name: "Inter" },
        { id: 4, name: "University" }
    ];
}
