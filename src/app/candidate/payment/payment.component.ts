import { Component, OnInit } from '@angular/core';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { Routes, RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  constructor(private authServerProvider: AuthServerProvider,
    private principal: Principal, private router: Router) { }

  ngOnInit() {
  }

  public onlinefeeCheck: boolean;
  public offlinefeeCheck: boolean;
  public debitCard: boolean;
  public paymentDetails: boolean;
  public creditCrad: boolean;
  public bhimUpi: boolean;
  public netBanking: boolean;
  public activedebitCard: boolean
  public user: any = {};

  paymentDetail() {
    this.debitCard = true;
    this.paymentDetails = true;
    this.creditCrad = false;
    this.bhimUpi = false;
    this.netBanking = false;

    this.activedebitCard = true;

  }

  feeChange(value: boolean) {
    if (value) {
      this.onlinefeeCheck = true;
      this.offlinefeeCheck = false;
    } else {
      this.offlinefeeCheck = true;
      this.onlinefeeCheck = false;

    }
  }

  logout() {
    this.authServerProvider.logout().subscribe();
    this.principal.authenticate(null);
    this.router.navigate(['/home']);
  }
}
