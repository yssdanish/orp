import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'popup',
  templateUrl: './shared-modal.component.html'
})
export class ModalComponent implements OnInit {

  public successValue: boolean = false;
  public errorValue: boolean = false;
  public message: string;
  private redirect: string;
  public showResult: boolean = false;
  public popupvalues = {};

  constructor(private router: Router) { }

  ngOnInit(){

  }

  public getSuccess(succes: string, redirect: string) {
      this.message = succes;
      this.successValue = true;
      this.redirect = redirect;
  }

  public getError(error: string) {
      this.message = error;
      this.errorValue = true;

  }

  public showDetails(values) {
      this.popupvalues = values;
      this.showResult = true;
  }

  hideError() {
      this.message = null;
      this.errorValue = false;
      this.successValue = false;
      this.router.navigate(['/apply']);
      
  }


  hideModal() {
      this.message = null;
      this.errorValue = false;
      this.successValue = false;
      this.showResult = false;
      
  }
}
