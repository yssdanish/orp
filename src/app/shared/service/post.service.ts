import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'


@Injectable()
export class PostService {

    constructor(private http: Http) { }

   


    save(mail: string): Observable<any> {
        return this.http.post('/api/account/reset_password/init', mail);
    }

    saveJob(value: any): Observable<any> {
        return this.http.post('/api/admin/save/job', value);
    }

    saveJobFields(value: any): Observable<any> {
        
        return this.http.post('/api/admin/save/job/fields', value);
    }

    removeJob(value: string) {
        return this.http.post('/api/admin/remove/', value);
    }


    editJob(value: string) {
        return this.http.post('/api/admin/update/job/', value);
    }

    applyFilter(value: any): Observable<any> {
        return this.http.post('/api/register/personal/filter', value);
    }

    sendAdmitCard(value: any) {
        return this.http.post('/api/admin/candidate/history', value);
    }

    adminRegister(admin: any) {
        return this.http.post('/api/admin/register/', admin);
    }

    createCenter(user: any) {
        return this.http.post('/api/admin/create/center', user);
    }

    deleteCenter(value: string) {
        return this.http.post('/api/admin/delete/center', value);
    }

    updateCenter(value: string) {
        return this.http.post('/api/admin/update/center/', value);
    }
    shortlistCandidate(value: any) {
        return this.http.post('/api/admin/shortlist/candidate', value);
    }
    
    saveMarks(value: any) : Observable<any>{
        return this.http.post('/api/admin/update/marks', value);
    }
    
    createCenterAllocatin(user: any) {
        
          return this.http.post('/api/admin/allocate/center', user);
      }
    createCandidate(user: any): Observable<any> {
        let headers = new Headers();
        headers.append('enctype', 'multipart/form-data');
        let options = new RequestOptions({ headers: headers });

        let formData = new FormData();
        formData.append("recruitmentZone", user.recruitmentZone);
        formData.append("registrationId", user.registrationId);
        formData.append("jobId", user.jobId);
        formData.append("payment", user.payment);
        formData.append("firstName", user.firstName);
        formData.append("middleName", user.middleName);
        formData.append("lastName", user.lastName);
        formData.append("fatherName", user.fatherName);
        formData.append("motherName", user.motherName);
        formData.append("category", user.category);
        formData.append("categoryCertificateDoc", user.categoryCertificateDoc);
        var datestr = (new Date(user.dateOfBirth)).toDateString();
        formData.append("dateOfBirth", (new Date(user.dateOfBirth)).toDateString());
        formData.append("age", user.age);
        // formData.append("gender", user.gender);
        formData.append("finalSubmit", user.finalSubmit);
        formData.append("mobileNumber", user.mobileNumber);
        formData.append("emailAddress", user.emailAddress);
        formData.append("nationality", user.nationality);
        formData.append("aadhaarCard", user.aadhaarCard);
        formData.append("aadhaarNumber", user.aadhaarNumber);
        formData.append("aadhaarCertificateDoc", user.aadhaarCertificateDoc);
        formData.append("maritalStatus", user.maritalStatus);
        formData.append("belongCommunity", user.belongCommunity);
        formData.append("communityName", user.communityName);
        formData.append("permanentAddress", user.permanentAddress);
        formData.append("permanentState", user.permanentState);
        formData.append("permanentCity", user.permanentCity);
        formData.append("permanentPincode", user.permanentPincode);
        formData.append("postalAddress", user.postalAddress);
        formData.append("postalState", user.postalState);
        formData.append("postalCity", user.postalCity);
        formData.append("postalPincode", user.postalPincode);
        formData.append("eyeSight", user.eyeSight);
        formData.append("disablity", user.disablity);
        formData.append("panNumber",user.panNumber);
        formData.append("panCertificateDoc",user.panCertificateDoc);
        formData.append("casualplabour",user.casualplabour);
        formData.append("cplCertificateDoc",user.cplCertificateDoc);
        // if (user.disabilitySpecify === undefined) {
        //     formData.append("disabilitySpecify", null);
        // } else {
        //     formData.append("disabilitySpecify", user.disabilitySpecify);
        // }

        // formData.append("bloodGroup", user.bloodGroup);
        // formData.append("height", user.candidateHeight);
        // formData.append("weight", user.candidateWeight);

        // formData.append("armedForce", user.armedForce);
        // if (user.armedCertificateDoc === undefined) {
        //     formData.append("armedCertificateDoc", null);
        // } else {
        //     formData.append("armedCertificateDoc", user.armedCertificateDoc);
        // }

        // if (user.armedarmyNumber === undefined) {
        //     formData.append("armedarmyNumber", null);
        // } else {
        //     formData.append("armedarmyNumber", user.armedarmyNumber);
        // }

        // if (user.armedFrom === "") {
        //     formData.append("armedFrom", new Date().toUTCString());
        // } else {
        //     formData.append("armedFrom", (new Date(user.armedFrom)).toDateString());
        // }
        // if (user.armedTo === "") {
        //     formData.append("armedTo", new Date().toUTCString());
        // } else {
        //     formData.append("armedTo", (new Date(user.armedTo)).toDateString());
        // }

        // formData.append("centralGovt", user.centralGovt);
        // if (user.centralCertificateDoc === undefined) {
        //     formData.append("centralCertificateDoc", null);
        // } else {
        //     formData.append("centralCertificateDoc", user.centralCertificateDoc);
        // }

        // if (user.servedFrom === "") {
        //     formData.append("servedFrom", new Date().toUTCString());
        // } else {
        //     formData.append("servedFrom", (new Date(user.servedFrom)).toDateString());
        // }
        // if (user.servedTo === "") {
        //     formData.append("servedTo", new Date().toUTCString());
        // } else {
        //     formData.append("servedTo", (new Date(user.servedTo)).toDateString());
        // }
        // formData.append("sonDaughterExservice", user.sonDaughterExservice);

        // formData.append("armyNumber", user.armyNumber);
        // formData.append("armyRank", user.armyRank);
        // formData.append("sonDaughterGref", user.sonDaughterGref);
        // if (user.sonDaughterNumber === undefined) {
        //     formData.append("sonDaughterNumber", null);
        // } else {
        //     formData.append("sonDaughterNumber", user.sonDaughterNumber);
        // }

        // if (user.sonDaughterRank === undefined) {
        //     formData.append("sonDaughterRank", null);
        // } else {
        //     formData.append("sonDaughterRank", user.sonDaughterRank);
        // }
        // formData.append("empExGref", user.empExGref);
        // if (user.servedBroFrom === "") {
        //     formData.append("servedBroFrom", new Date().toUTCString());
        // } else {
        //     formData.append("servedBroFrom", (new Date(user.servedBroFrom)).toDateString());
        // }
        // if (user.servedBroTo === "") {
        //     formData.append("servedBroTo", new Date().toUTCString());
        // } else {
        //     formData.append("servedBroTo", (new Date(user.servedBroTo)).toDateString());
        // }
        // if (user.gsNumber === undefined) {
        //     formData.append("gsNumber", null);
        // } else {
        //     formData.append("gsNumber", user.gsNumber);
        // }

        formData.append("sonExGps",user.sonExGps);
        formData.append("sonGsNumber",user.sonGsNumber);
        formData.append("sonGpsRank",user.sonGpsRank);
        formData.append("sonGpsName",user.sonGpsName);
        formData.append("sonUnitName",user.sonUnitName);
        formData.append("unitCertificateDoc",user.unitCertificateDoc);

        formData.append("reapptCandidateName",user.reapptCandidateName);
        formData.append("reapptCandidate",user.reapptCandidate);
        formData.append("reapptCandidateGs",user.reapptCandidateGs);
        formData.append("reaaptCandidateRank",user.reaaptCandidateRank);
        formData.append("reapptCandidateUnit",user.reapptCandidateUnit);

        formData.append("sonExservice",user.sonExservice);
        formData.append("armyNumber",user.armyNumber);
        formData.append("armyRank",user.armyRank);
        formData.append("armyName",user.armyName);
        formData.append("armyLastUnit",user.armyLastUnit);
        formData.append("lastUnitCertificateDoc",user.lastUnitCertificateDoc);

        formData.append("exService",user.exService);
        formData.append("dischargeCertificateDoc",user.dischargeCertificateDoc);


        
        // formData.append("sonDaughterName", user.sonDaughterName);
        // formData.append("brotherSisterGref", user.brotherSisterGref);
        // formData.append("brotherSisterNumber", user.brotherSisterNumber);
        // formData.append("brotherSisterRank", user.brotherSisterRank);
        // formData.append("brotherSisterName", user.brotherSisterName);

        
        formData.append("nccB",user.nccB);
        formData.append("nccBCertificate", user.nccBCertificate);
        formData.append("nccBCertificateDoc", user.nccBCertificateDoc);

        formData.append("nccC",user.nccC);
        formData.append("nccCCertificate", user.nccCCertificate);
        formData.append("nccCCertificateDoc", user.nccCCertificateDoc);

        formData.append("sportsMan", user.sportsMan);
        formData.append("sportsLevel",user.sportsLevel);
        formData.append("sportsManDoc", user.sportsManDoc);

        formData.append("highYear", user.highYear);
        formData.append("highInstitution", user.highInstitution);
        formData.append("highSpecialization", user.highSpecialization);
        formData.append("highCourseType", user.highCourseType);
        formData.append("highBoard", user.highBoard);
        formData.append("highTotalMarks", user.highTotalMarks);
        formData.append("highObtainMarks", user.highObtainMarks);
        formData.append("highPercentage", user.highPercentage);
        formData.append("highGrade",user.highGrade);
        formData.append("highCertificateDoc", user.highCertificateDoc);

        // if(user.interYear === undefined){
        //     formData.append("interYear", null);
        // }else{
        //     formData.append("interYear", user.interYear);
        // }
        formData.append("interYear", user.interYear);

        if (user.interInstitution === undefined) {
            formData.append("interInstitution", null);
        } else {
            formData.append("interInstitution", user.interInstitution);
        }

        if (user.interSpecialization === undefined) {
            formData.append("interSpecialization", null);
        } else {
            formData.append("interSpecialization", user.interSpecialization);
        }

        if (user.interCourseType === undefined) {
            formData.append("interCourseType", null);
        } else {
            formData.append("interCourseType", user.interCourseType);
        }

        if (user.interBoard === undefined) {
            formData.append("interBoard", null);
        } else {
            formData.append("interBoard", user.interBoard);
        }

        // if(user.interTotalMarks === undefined){
        //     formData.append("interTotalMarks", null);
        // }else{
        //     formData.append("interTotalMarks", user.interTotalMarks);
        // }
        // formData.append("interInstitution", user.interInstitution);
        // formData.append("interSpecialization", user.interSpecialization);
        // formData.append("interCourseType", user.interCourseType);
        // formData.append("interBoard", user.interBoard);
        formData.append("interTotalMarks", user.interTotalMarks);

        // if(user.interObtainMarks === undefined){
        //     formData.append("interObtainMarks", null);
        // }else{
        //     formData.append("interObtainMarks", user.interObtainMarks);
        // }
        formData.append("interObtainMarks", user.interObtainMarks);
        // if(user.interPercentage === undefined){
        //     formData.append("interPercentage", null);
        // }else{
        //     formData.append("interPercentage", user.interPercentage);
        // }
        formData.append("interPercentage", user.interPercentage);
        formData.append("interGrade",user.interGradeName);

        // if(user.interBoard === undefined){
        //     formData.append("interBoard", null);
        // }else{
        //     formData.append("interBoard", user.interBoard);
        // }

        // if(user.englishTyping === undefined){
        //     formData.append("englishTyping", null);
        // }else{
        //     formData.append("englishTyping", user.englishTyping);
        // }

        // if(user.hindiTyping === undefined){
        //     formData.append("hindiTyping", null);
        // }else{
        //     formData.append("hindiTyping", user.hindiTyping);
        // }





        // formData.append("interObtainMarks", user.interObtainMarks);
        // formData.append("interPercentage", user.interPercentage);
        formData.append("interCertificateDoc", user.interCertificateDoc);
        formData.append("englishTyping", user.englishTyping);
        formData.append("hindiTyping", user.hindiTyping);
        formData.append("hindiStenography", user.hindiStenography);
        formData.append("englishStenography", user.englishStenography);
        formData.append("additionalCertificateDoc", user.additionalCertificateDoc);
        formData.append("firstaidCertificateDoc", user.firstaidCertificateDoc);

        if (user.ugCourse === undefined) {
            formData.append("ugCourse", null);
        } else {
            formData.append("ugCourse", user.ugCourse);
        }

        // if(user.ugPassingYear === undefined){
        //     formData.append("ugPassingYear", null);
        // }else{
        //     formData.append("ugPassingYear", user.ugPassingYear);
        // }

        if (user.ugInstitution === undefined) {
            formData.append("ugInstitution", null);
        } else {
            formData.append("ugInstitution", user.ugInstitution);
        }

        // formData.append("ugCourse", user.ugCourse);
        formData.append("ugPassingYear", user.ugPassingYear);
        // formData.append("ugInstitution", user.ugInstitution);

        if (user.ugUniversity === undefined) {
            formData.append("ugUniversity", null);
        } else {
            formData.append("ugUniversity", user.ugUniversity);
        }

        if (user.ugSpecialization === undefined) {
            formData.append("ugSpecialization", null);
        } else {
            formData.append("ugSpecialization", user.ugSpecialization);
        }

        // if(user.ugTotalMarks === undefined){
        //     formData.append("ugTotalMarks", null);
        // }else{
        //     formData.append("ugTotalMarks", user.ugTotalMarks);
        // }

        // if(user.ugObtainMarks === undefined){
        //     formData.append("ugObtainMarks", null);
        // }else{
        //     formData.append("ugObtainMarks", user.ugObtainMarks);
        // }

        // if(user.ugPercentage === undefined){
        //     formData.append("ugPercentage", null);
        // }else{
        //     formData.append("ugPercentage", user.ugPercentage);
        // }

        if (user.ugCourseType === undefined) {
            formData.append("ugCourseType", null);
        } else {
            formData.append("ugCourseType", user.ugCourseType);
        }

        // formData.append("ugUniversity", user.ugUniversity);
        // formData.append("ugSpecialization", user.ugSpecialization);
        formData.append("ugTotalMarks", user.ugTotalMarks);
        formData.append("ugObtainMarks", user.ugObtainMarks);
        formData.append("ugPercentage", user.ugPercentage);
        // formData.append("ugCourseType", user.ugCourseType);
        formData.append("ugCertificateDoc", user.ugCertificateDoc);

        if (user.pgCourse === undefined) {
            formData.append("pgCourse", null);
        } else {
            formData.append("pgCourse", user.pgCourse);
        }

        formData.append("pgPassingYear", user.pgPassingYear);

        if (user.pgInstitution === undefined) {
            formData.append("pgInstitution", null);
        } else {
            formData.append("pgInstitution", user.pgInstitution);
        }


        if (user.pgUniversity === undefined) {
            formData.append("pgUniversity", null);
        } else {
            formData.append("pgUniversity", user.pgUniversity);
        }

        if (user.pgSpecialization === undefined) {
            formData.append("pgSpecialization", null);
        } else {
            formData.append("pgSpecialization", user.pgSpecialization);
        }

        // formData.append("pgInstitution", user.pgInstitution);
        // formData.append("pgUniversity", user.pgUniversity);
        // formData.append("pgSpecialization", user.pgSpecialization);
        formData.append("pgTotalMarks", user.pgTotalMarks);
        formData.append("pgObtainMarks", user.pgObtainMarks);
        formData.append("pgPercentage", user.pgPercentage);

        if (user.pgCourseType === undefined) {
            formData.append("pgCourseType", null);
        } else {
            formData.append("pgCourseType", user.pgCourseType);
        }

        if (user.diplomaInstitution === undefined) {
            formData.append("diplomaInstitution", null);
        } else {
            formData.append("diplomaInstitution", user.diplomaInstitution);
        }
        // formData.append("pgCourseType", user.pgCourseType);
        formData.append("pgCertificateDoc", user.pgCertificateDoc);
        formData.append("diplomaYear", user.diplomaYear);
        // formData.append("diplomaInstitution", user.diplomaInstitution);
        if (user.diplomaSpecialization === undefined) {
            formData.append("diplomaSpecialization", null);
        } else {
            formData.append("diplomaSpecialization", user.diplomaSpecialization);
        }
        if (user.diplomaBranch === undefined) {
            formData.append("diplomaBranch", null);
        } else {
            formData.append("diplomaBranch", user.diplomaBranch);
        }

        if (user.diplomaUniversity === undefined) {
            formData.append("diplomaUniversity", null);
        } else {
            formData.append("diplomaUniversity", user.diplomaUniversity);
        }

        // formData.append("diplomaSpecialization", user.diplomaSpecialization);
        // formData.append("diplomaBranch", user.diplomaBranch);
        // formData.append("diplomaUniversity", user.diplomaUniversity);
        formData.append("diplomaTotalMarks", user.diplomaTotalMarks);
        formData.append("diplomaObtainMarks", user.diplomaObtainMarks);
        formData.append("diplomaPercentage", user.diplomaPercentage);

        if (user.diplomaCourseType === undefined) {
            formData.append("diplomaCourseType", null);
        } else {
            formData.append("diplomaCourseType", user.diplomaCourseType);
        }

        // formData.append("diplomaCourseType", user.diplomaCourseType);
        formData.append("diplomaCertificateDoc", user.diplomaCertificateDoc);
        formData.append("degreeYear", user.degreeYear);


        if (user.degreeInstitution === undefined) {
            formData.append("degreeInstitution", null);
        } else {
            formData.append("degreeInstitution", user.degreeInstitution);
        }


        if (user.degreeSpecialization === undefined) {
            formData.append("degreeSpecialization", null);
        } else {
            formData.append("degreeSpecialization", user.degreeSpecialization);
        }


        if (user.degreeCourseType === undefined) {
            formData.append("degreeCourseType", null);
        } else {
            formData.append("degreeCourseType", user.degreeCourseType);
        }

        if (user.degreeBranch === undefined) {
            formData.append("degreeBranch", null);
        } else {
            formData.append("degreeBranch", user.degreeBranch);
        }

        // formData.append("degreeInstitution", user.degreeInstitution);
        // formData.append("degreeSpecialization", user.degreeSpecialization);
        // formData.append("degreeCourseType", user.degreeCourseType);
        // formData.append("degreeBranch", user.degreeBranch);

        if (user.degreeUniversity === undefined) {
            formData.append("degreeUniversity", null);
        } else {
            formData.append("degreeUniversity", user.degreeUniversity);
        }
        // formData.append("degreeUniversity", user.degreeUniversity);
        formData.append("degreeTotalMarks", user.degreeTotalMarks);
        formData.append("degreeMarksObtain", user.degreeMarksObtain);
        formData.append("degreePercentage", user.degreePercentage);
        formData.append("degreeCertificateDoc", user.degreeCertificateDoc);


        formData.append("itiYear", user.itiYear);

        if (user.itiInstitution === undefined) {
            formData.append("itiInstitution", null);
        } else {
            formData.append("itiInstitution", user.itiInstitution);
        }


        if (user.itiSpecialization === undefined) {
            formData.append("itiSpecialization", null);
        } else {
            formData.append("itiSpecialization", user.itiSpecialization);
        }


        if (user.itiCourseType === undefined) {
            formData.append("itiCourseType", null);
        } else {
            formData.append("itiCourseType", user.itiCourseType);
        }

        if (user.itiBranch === undefined) {
            formData.append("itiBranch", null);
        } else {
            formData.append("itiBranch", user.itiBranch);
        }


        if (user.itiUniversity === undefined) {
            formData.append("itiUniversity", null);
        } else {
            formData.append("itiUniversity", user.itiUniversity);
        }

        formData.append("itiTotalMarks", user.itiTotalMarks);
        formData.append("itiMarksObtain", user.itiMarksObtain);
        formData.append("itiPercentage", user.itiPercentage);
        formData.append("itiCertificateDoc", user.itiCertificateDoc);


        if (user.companyName === undefined) {
            formData.append("companyName", null);
        } else {
            formData.append("companyName", user.companyName);
        }
        // formData.append("companyName", user.companyName);

        if (user.employmentFrom === undefined) {
            formData.append("employmentFrom", new Date().toUTCString());
        } else {
            formData.append("employmentFrom", (new Date(user.employmentFrom)).toUTCString());
        }

        if (user.employmentTo === undefined) {
            formData.append("employmentTo", new Date().toUTCString());
        } else {
            formData.append("employmentTo", (new Date(user.employmentTo)).toUTCString());
        }


        // formData.append("employmentFrom", (new Date(user.employmentFrom)).toDateString());
        // formData.append("employmentTo", (new Date(user.employmentTo)).toDateString());
        formData.append("cinNumber", user.cinNumber);
        formData.append("regtinNumber", user.regtinNumber);

        if (user.workNature === undefined) {
            formData.append("workNature", null);
        } else {
            formData.append("workNature", user.workNature);
        }

        // formData.append("workNature", user.workNature);
        formData.append("monthlySalary", user.monthlySalary);

        if (user.temporaryPermanent === undefined) {
            formData.append("temporaryPermanent", null);
        } else {
            formData.append("temporaryPermanent", user.temporaryPermanent);
        }

        // formData.append("temporaryPermanent", user.temporaryPermanent);
        formData.append("experienceCertificateDoc", user.experienceCertificateDoc);
        formData.append("licenceNumberLmv", user.licenceNumberLmv);
        if (user.issueDateLmv === undefined) {
            formData.append("issueDateLmv", new Date().toUTCString());
        } else {
            formData.append("issueDateLmv", (new Date(user.issueDateLmv)).toUTCString());
        }
        if (user.validUptoLmv === undefined) {
            formData.append("validUptoLmv", new Date().toUTCString());
        } else {
            formData.append("validUptoLmv", (new Date(user.validUptoLmv)).toUTCString());
        }
        if (user.issuingAuthorityLmv === undefined) {
            formData.append("issuingAuthorityLmv", null);
        } else {
            formData.append("issuingAuthorityLmv", user.issuingAuthorityLmv);
        }
        if (user.licenceNumberHgmv === undefined) {
            formData.append("licenceNumberHgmv", null);
        } else {
            formData.append("licenceNumberHgmv", user.licenceNumberHgmv);
        }
        if (user.issueDateHgmv === undefined) {
            formData.append("issueDateHgmv", new Date().toUTCString());
        } else {
            formData.append("issueDateHgmv", (new Date(user.issueDateHgmv)).toUTCString());
        }
        if (user.validUptoHgmv === undefined) {
            formData.append("validUptoHgmv", new Date().toUTCString());
        } else {
            formData.append("validUptoHgmv", (new Date(user.validUptoHgmv)).toDateString());
        }
        // formData.append("issueDateHgmv", (new Date(user.issueDateHgmv)).toDateString());
        // formData.append("validUptoHgmv", (new Date(user.validUptoHgmv)).toDateString());
        if (user.issuingAuthorityHlmv === undefined) {
            formData.append("issuingAuthorityHlmv", null);
        } else {
            formData.append("issuingAuthorityHlmv", user.issuingAuthorityHlmv);
        }
        if (user.licenceNumberRoad === undefined) {
            formData.append("licenceNumberRoad", null);
        } else {
            formData.append("licenceNumberRoad", user.licenceNumberRoad);
        }
        if (user.issueDateRoad === undefined) {
            formData.append("issueDateRoad", new Date().toUTCString());
        } else {
            formData.append("issueDateRoad", (new Date(user.issueDateRoad)).toUTCString());
        }
        if (user.validUptoRr === undefined) {
            formData.append("validUptoRoad", new Date().toUTCString());
        } else {
            formData.append("validUptoRoad", (new Date(user.validUptoRoad)).toDateString());
        }
        
        if (user.issuingAuthorityRoad === undefined) {
            formData.append("issuingAuthorityRoad", null);
        } else {
            formData.append("issuingAuthorityRoad", user.issuingAuthorityRoad);
        }
        if (user.licenceNumberOeg === undefined) {
            formData.append("licenceNumberOeg", null);
        } else {
            formData.append("licenceNumberOeg", user.licenceNumberOeg);
        }
        if (user.issueDateOeg === undefined) {
            formData.append("issueDateOeg", new Date().toUTCString());
        } else {
            formData.append("issueDateOeg", (new Date(user.issueDateOeg)).toUTCString());
        }
        if (user.validUptoOeg === undefined) {
            formData.append("validUptoOeg", new Date().toUTCString());
        } else {
            formData.append("validUptoOeg", (new Date(user.validUptoOeg)).toDateString());
        }
     
        if (user.issuingAuthorityOeg === undefined) {
            formData.append("issuingAuthorityOeg", null);
        } else {
            formData.append("issuingAuthorityOeg", user.issuingAuthorityOeg);
        }
        
        // formData.append("issuingAuthorityHlmv", user.issuingAuthorityHlmv);
        formData.append("drivingLicenseDoc", user.drivingLicenseDoc);
        formData.append("drivingExperienceDoc", user.drivingExperienceDoc);
        formData.append("browsePhotoDoc", user.browsePhotoDoc);
        formData.append("browseSignatureDoc", user.browseSignatureDoc);

        return this.http.post('/api/register/candidate/register', formData, options);
        //     return this.http.post('/api/candidate/register', user);
    }


    uploadDocument(docName, jobId) {
        let headers = new Headers();
        headers.append('enctype', 'multipart/form-data');
        let options = new RequestOptions({ headers: headers });

        let formData = new FormData();
        formData.append("postDetailsDocument", docName);
        formData.append("jobId", jobId);
        return this.http.post('/api/admin/upload/job/document', formData, options);
    }
    createBoardMember(member): Observable<any>{
        let API_URL="http://localhost:8080"
        return this.http.post(API_URL+'/board/member/save',member);
    }

    updateBoardMember(member): Observable<any>{
        let API_URL="http://localhost:8080"
        return this.http.post(API_URL+'/board/member/update',member);
    }
 
    newRegistration(user: any): Observable<any> {   
        
                let headers = new Headers();
                headers.append('enctype', 'multipart/form-data');
                let options = new RequestOptions({ headers: headers });
                let formData = new FormData();
                formData.append("registrationId", user.registrationId);
                formData.append("password", user.password);
                formData.append("firstName", user.firstName);
                formData.append("middleName", user.middleName);
                formData.append("lastName", user.lastName);
                formData.append("fatherName", user.fatherName);
                formData.append("dateOfBirth", (new Date(user.dateOfBirth)).toDateString());
                formData.append("age", user.age);
                formData.append("gender", user.gender);
                formData.append("mobileNumber", user.mobileNumber);
                formData.append("emailAddress", user.emailAddress);
                formData.append("nationality", user.nationality);
                return this.http.post('/api/register/newcandidate/register',  formData, options);
            }
    
}