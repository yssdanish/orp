import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { HttpParams, HttpClient } from '@angular/common/http';


@Injectable()
export class SharedService {

    public sharingData: any;
    public sharingJob: any;
    public sharingRegisterId: any;
    public sharingCenterData: any;

    constructor(
        private http: Http
    ) { }

    saveData(str) {
        this.sharingData = str;
    }
    getData() {
        return this.sharingData;
    }

    saveJobData(str) {
        this.sharingJob = str;
    }

    //by dheeraj
    editJobData(str) {
        this.sharingJob = str;
    }


    getJobData() {
        return this.sharingJob;
    }

    saveRegisterData(str) {
        this.sharingRegisterId = str;
    }
    getRegisterId() {
        return this.sharingRegisterId;
    }

    saveCenterData(center) {
        this.sharingCenterData = center;
    }
    getCenterbData() {
        return this.sharingCenterData;
    }
    
}