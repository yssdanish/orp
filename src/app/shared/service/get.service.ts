import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { HttpParams, HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from '../../app.constants';


@Injectable()
export class GetService {

    constructor(
        private http: Http
    ) { }

    emailCheck(value: string) {
        return this.http.get(SERVER_API_URL + '/api/register/emailCheck?email=' + value)
            .map((response: Response) => response.json())
    }

    getActiveJobs() {
        return this.http.get(SERVER_API_URL + '/api/register/active/jobs')
            .map((response: Response) => response.json())
    }

    getAllJobs() {
        return this.http.get(SERVER_API_URL + '/api/register/all/jobs')
            .map((response: Response) => response.json())
    }

    getPersonalDetails(value: any) {
        return this.http.get(SERVER_API_URL + '/api/register/personal/' + value)
            .map((response: Response) => response.json())
    }
    getState() {
        return this.http.get(SERVER_API_URL + '/api/register/state')
            .map((response: Response) => response.json())
    }

    getCity(cityId: string) {
        return this.http.get(SERVER_API_URL + '/api/register/city/' + cityId)
            .map((response: Response) => response.json())
    }

    getBoard() {
        return this.http.get(SERVER_API_URL + '/api/register/boards')
            .map((response: Response) => response.json())
    }

    getUniversity() {
        return this.http.get(SERVER_API_URL + '/api/register/university')
            .map((response: Response) => response.json())
    }

    getCourse(type: string) {
        return this.http.get(SERVER_API_URL + '/api/register/courses/type?type=' + type)
            .map((response: Response) => response.json())
    }

    getCategory() {
        return this.http.get(SERVER_API_URL + '/api/register/category')
            .map((response: Response) => response.json())
    }
    getAllCourse() {
        return this.http.get(SERVER_API_URL + '/api/register/courses')
            .map((response: Response) => response.json())
    }

    getYears() {
        return this.http.get(SERVER_API_URL + '/api/register/year')
            .map((response: Response) => response.json())
    }

    getFormFields(value: any) {
        return this.http.get(SERVER_API_URL + '/api/register/form/fields/' + value)
            .map((response: Response) => response.json())
    }

    getCreatedJobs() {
        return this.http.get(SERVER_API_URL + '/api/admin/total/jobs')
            .map((response: Response) => response.json())
    }
    getCandidateList() {
        return this.http.get(SERVER_API_URL + '/api/register/personal')
            .map((response: Response) => response.json())
    }

    getAmountFields(value: any) {
        return this.http.get(SERVER_API_URL + '/api/register/form/amount/' + value)
            .map((response: Response) => response.json())
    }

    getCenters(){
        return this.http.get(SERVER_API_URL + '/api/admin/all/centers')
        .map((response: Response) => response.json())
    }

    getAllocatedCenters(){
        
        return this.http.get(SERVER_API_URL + '/api/admin/all/allocation/center')
        .map((response: Response) => response.json())
    }

    getCandidates(){
        return this.http.get(SERVER_API_URL + '/api/admin/shortlisted/all/candidate')
        .map((response: Response) => response.json())
    }


    getScannerDeviceInfo(){
        return this.http.get(SERVER_API_URL + '/api/admin/deviceConnectin/varification')
        .map((response: Response) => response.json())
    }
   
    getCondidatesFingerPrint(){
        return this.http.get(SERVER_API_URL + '/api/admin/fingerprint/details')
        .map((response: Response) => response.json())
    }
    

    setCondidatesFingerPrint(value: any){
        return this.http.get(SERVER_API_URL + '/api/admin/fingerprint/storetion/'+value)
        .map((response: Response) => response.json())
    }



    verifyCondidatesFingerPrint(value:any){
        return this.http.get(SERVER_API_URL + '/api/admin/fingerprint/varification'+value)
        .map((response: Response) => response.json())
    }
  
    stopCapture(){
        return this.http.get(SERVER_API_URL + '/api/admin/fingerprint/stop')
        .map((response: Response) => response.json())
    }

    getusersDetails(){
        //http://localhost:8080/board/users/list
       let API_URL="http://localhost:8080"
        return this.http.get(API_URL + '/board/users/list')
        .map((response:Response)=>response.json())
    }

    getAllMemberDetails(){
        let API_URL="http://localhost:8080"
        return this.http.get(API_URL+'/board/member/list')
        .map((response:Response)=>response.json())
    }

    getBoardDetails(){
        let API_URL="http://localhost:8080"
        return this.http.get(API_URL+'/board/member/group')
        .map((response:Response)=>response.json())
    }
    
    getBoardMemberDetails(value){
        let API_URL="http://localhost:8080"
        return this.http.get(API_URL+'/board/member/list/'+value)
        .map((response:Response)=>response.json())
    }

    getUserDetailsNotBoardMember(value){
        let API_URL="http://localhost:8080"
        return this.http.get(API_URL+'/board/member/addnew/'+value)
        .map((response:Response)=>response.json())
    }

    deleteUserDetailsBoardMember(value){
        let API_URL="http://localhost:8080"
        return this.http.get(API_URL+'/board/member/delete/'+value)
        .map((response:Response)=>response.json())
    }

}