import { Component, OnInit, TemplateRef } from '@angular/core';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-job-post',
  templateUrl: './job-post.component.html',
  styleUrls: ['./job-post.component.css']
})
export class JobPostComponent implements OnInit {
  public modalRef: BsModalRef;
  public authenticationError: boolean;
  public loading = false;
  public successField: any = false;
  public successEdit: boolean;
  public jobList: any = [];
  public noJobsCreated: boolean;
  public deleteField: boolean;
  public jobName: any = {};
  public admin: any = [
    {
      jobTitle: '',
      jobDesc: ''
    }
  ];


  pdfSrc: string = '../../assets/images/Amazon.pdf';
  constructor(public postService: PostService, private router: Router,
    private getService: GetService, private sharedService: SharedService,
    private authServerProvider: AuthServerProvider,
    private principal: Principal, private modalService: BsModalService) {

  }

  ngOnInit() {
    this.getCreatedJobs();
  }



  saveJob() {
    this.loading = true;
    this.admin;
    this.successField = false;
    this.authenticationError = false;
    this.postService.saveJob(this.admin).subscribe(data => {
      this.getCreatedJobs();
      this.jobList = data.json();
      if (data.status == 200) {
        this.successField = true;
        this.loading = false;
        this.authenticationError = false;

      }
      setTimeout(() => {
        this.successField = false;
      }, 5000);
      this.admin = [
        {

          jobTitle: '',
          jobDesc: ''
        }
      ];
    },
      error => {
        this.loading = false;
        this.successField = false;
        this.authenticationError = true;
        setTimeout(() => {
          this.authenticationError = false;
        }, 5000);

      });
  }

  addPost() {
    // this.loading = true;
    let newValue = {
      jobTitle: '',
      jobDesc: ''
    }
    this.admin.push(newValue);
    // this.admin.push({ postName: '', postDesc: '' });
  }

  removePost(index: any) {
    this.admin.splice(index, 1);
  }

  addFields() {

    this.router.navigate(['/admin/job/post']);
  }
  homePage() {
    this.router.navigate(['/home']);
  }


  addJobFields(job) {

    this.sharedService.saveJobData(job);
    this.router.navigate(['/admin/job/fields']);
  }

  removeJob(job) {
    this.deleteField = false;
    this.loading = true;
    this.postService.removeJob(job).subscribe(
      data => {
        this.loading = false;
        this.deleteField = true;
        // console.log("post Course :", data);
        // this.jobList = data;
        this.getCreatedJobs();
        setTimeout(() => {
          this.deleteField = false;
        }, 5000);
        // this.deleteField = false;
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
        this.deleteField = false;
      });

  }

  closeModal() {
    this.getCreatedJobs();
    this.modalRef.hide();
  }

  // editPost(index) {
  //    this.editJobField = true;
  //    this.jobList[index];
  //     // this.sharedService.editJobData(index);
  //     // this.router.navigate(['/admin/job/fields']);
  //  }

  
  getCreatedJobs() {
    this.loading = true;
    this.noJobsCreated = false;
    this.getService.getCreatedJobs().subscribe(
      data => {
        this.noJobsCreated = false;
        // console.log("post Course :", data);
        this.jobList = data;
        this.loading = false;
        if (this.jobList < 1) {
          this.noJobsCreated = true;
        }
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
      });
  }

  public editPost(template: TemplateRef<any>, value) {
    this.successEdit = false;
    this.jobName = JSON.parse(JSON.stringify(value));
    this.modalRef = this.modalService.show(template);
  }

  saveEditPost() {
    this.loading = true;
    this.successEdit = false;
    this.postService.editJob(this.jobName).subscribe(data => {
      if (data.status == 200) {
        this.successEdit = true;
        this.loading = false;
      }
      this.getCreatedJobs();
    },
      error => {
        this.successEdit = false;
        this.loading = false;
      });
  }


  logout() {
    this.authServerProvider.logout().subscribe();
    this.principal.authenticate(null);
    this.router.navigate(['/admin/login']);
  }
}
