import { Component, TemplateRef, ViewChild, OnInit } from '@angular/core';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { ModalComponent } from '../../shared/Model/shared-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.css']
})
export class ScannerComponent implements OnInit {
  public authenticationError: boolean;
  public loading = false;
  public modalRef: BsModalRef;
  public successField: any = false;
  public admin: any = {};
  public deviceInfo: any = [];

  public fingerPrintInfotemp: any = [];
  public fingerPrintInfo: any = [
    {
      registration: '',
      name: '',
      category: '',
      post: '',
      fathername: ''
    }
  ];


  // public admin: any = [
  //   {
  //     jobTitle: '',
  //     jobDesc: ''
  //   }
  // ];

  constructor(private modalService: BsModalService,
    private getService: GetService,
    private postService: PostService,
    private authServerProvider: AuthServerProvider,
    private principal: Principal, private router: Router) { }
  public deviceconnection: any = 'false';
  public canditions: any = true;

  ngOnInit() {
    this.checkDevice();

    this.getDetails();

  }

  startCapture() {

    this.loading = true;
    this.fingerPrintInfo.registrationId = this.admin.registrationId;
    if ((this.fingerPrintInfo.registrationId != undefined) && (this.fingerPrintInfo.registrationId != "")) {
      this.canditions = true;
      this.getService.setCondidatesFingerPrint(this.fingerPrintInfo.registrationId).subscribe(
        data => {
          console.log("Condidate set thumb info :", data);
          this.fingerPrintInfotemp = data;
          this.fingerPrintInfo.registration = this.fingerPrintInfotemp.registrationId;
          console.log(this.fingerPrintInfo.registrationId);
          this.fingerPrintInfo.name = this.fingerPrintInfotemp.personalDetails.firstName + ' ' + this.fingerPrintInfotemp.personalDetails.middleName + ' ' + this.fingerPrintInfotemp.personalDetails.lastName;
          this.fingerPrintInfo.category = this.fingerPrintInfotemp.personalDetails.category;
          this.fingerPrintInfo.post = this.fingerPrintInfotemp.personalDetails.jobDetails.postName;
          this.fingerPrintInfo.fathername = this.fingerPrintInfotemp.personalDetails.fatherName;
          this.loading = false;
          if (this.fingerPrintInfo < 1) {

          } else {

          }
        },
        error => {
          // this.alertService.error(error);
          this.loading = false;
        });

    }
    else {
      this.canditions = false
    }
  }
  stopCapture() {
    alert("success 2");

    this.getService.stopCapture().subscribe(
      data => {
        console.log("Device info :", data);
             
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
      });
  }

  

  matchIso() {
    alert("success 4");

    this.getService.verifyCondidatesFingerPrint(2).subscribe(
      data => {
        //  this.noJobsCreated = false;
        console.log("Device info :", data);
        this.fingerPrintInfo = data;

        //  this.loading = false;
        if (this.fingerPrintInfo < 1) {
          //      this.noJobsCreated = true;
        }
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
      });
    // this.getService.getScannerDeviceInfo();
  }

  matchAnsi() {
    alert("success 5");

    this.getService.verifyCondidatesFingerPrint(1).subscribe(
      data => {
        //  this.noJobsCreated = false;
        console.log(" info :", data);
        this.fingerPrintInfo = data;

        //  this.loading = false;
        if (this.fingerPrintInfo < 1) {
          //      this.noJobsCreated = true;
        }
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
      });
    // this.getService.getScannerDeviceInfo();
  }

  
  getDetails() {

    this.getService.getCondidatesFingerPrint().subscribe(
      data => {
        console.log("finger info :", data);
        this.fingerPrintInfo = data;
        this.fingerPrintInfotemp = data;
        this.fingerPrintInfo.registration = this.fingerPrintInfotemp.registrationId;
        console.log(this.fingerPrintInfo.registrationId);
        this.fingerPrintInfo.name = this.fingerPrintInfotemp.personalDetails.firstName + ' ' + this.fingerPrintInfotemp.personalDetails.middleName + ' ' + this.fingerPrintInfotemp.personalDetails.lastName;
        this.fingerPrintInfo.category = this.fingerPrintInfotemp.personalDetails.category;
        this.fingerPrintInfo.post = this.fingerPrintInfotemp.personalDetails.jobDetails.postName;
        this.fingerPrintInfo.fathername = this.fingerPrintInfotemp.personalDetails.fatherName;

        //  this.loading = false;
        if (this.fingerPrintInfo < 1) {

        }
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
      });

  }

  checkDevice() {
    //  alert("success 8");
    this.getService.getScannerDeviceInfo().subscribe(
      data => {
        console.log("Device info :", data);
        this.deviceInfo = data;
        this.deviceconnection = data[2];
        console.log("connection :", this.deviceconnection);

        if (this.deviceInfo < 1) {
          //      this.noJobsCreated = true;
        }
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
      });
    // this.getService.getScannerDeviceInfo();

  }

  

}


