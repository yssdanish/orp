import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { PostService, GetService, SharedService } from '../../shared/service/index';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-bordmember',
  templateUrl: './bordmember.component.html',
  styleUrls: ['./bordmember.component.css']
})
export class BordmemberComponent implements OnInit {

  public userList:any=[];
  public selectedUser:any=[];
  public postList:any=[];
  public selectedPost:any={};
  public selectBTN:any='SELECT';
  public currentPost:any;
  public currentGroup:any;
  public userRow:any=3;
  public member:any={};
  public showUsers:boolean=false;
  public allBoardMemberList:any=[];
  public boardGroupList:any=[];
  public groupMemberList:any=[];
  public saveorUpdateShow:boolean=false;
  public p: number[] = [];
  public paginationSize:number=4;
  public selectGroup:any="Selected Board Mamber Details";

  public newMember:any={
    "groupName":"",
    "postId":"",
    "userslist":[]
  };




  constructor(public postService: PostService,
    private sharedService: SharedService, private router: Router,private getService:GetService
   ) { }

  ngOnInit() {
 //this.getJobDetails();
 this.getAllJobs();
 this.getusersDetails();
 this.getBoardGroupDetails();
  }

  getJobDetails(){
    this.getService.getCreatedJobs().subscribe(
    data=>{
      console.log("log ---- ",data);
      this.postList=data;
      console.log("log ---- ",this.postList);
    })
  }


  getAllJobs() {
   // this.loading = true;
  //  this.noActiveJobs=false;
    this.getService.getActiveJobs().subscribe(
      data => {
        this.postList = data;
        console.log("log ---- ",this.postList);
        //this.loading = false;
       // this.noActiveJobs=false;
        if (this.postList < 1) {
         // this.noActiveJobs = true;
        }
      },
      error => {
      //  this.loading = false;
      });
  }
  getusersDetails(){
    this.getService.getusersDetails()
    .subscribe(data=>{
      console.log(" user data ",data)
     this.userList=data;
    })
  }
  
  selectuser(event,user){
  //  console.log("selected even ",event);
 //   console.log("selected even ",event.target.name);
  //  console.log("selected even ",event.target.value);
   // var p = document.getElementsByName(event.target.name);
  
    console.log("selected user ",user);
    if(event.target.value=="SELECT"){

      //set button value
     event.target.value="SELECTED";
      // add new user in selectedUser list
     this.selectedUser.push(user);
     this.newMember.userslist.push(user.id);
     // get element details by name
     let name=document.getElementsByName(event.target.name);
     
     console.log("selected all user ",name);
    }else{
          event.target.value="SELECT";
        //    this.selectedUser.pop()

        //find index of user in selectedUser List
        const index: number = this.selectedUser.indexOf(user);
          console.log(index);
          if (index !== -1) {
           // remove user from selectedUser List
             this.selectedUser.splice(index, 1);
             this.newMember.userslist.splice(index,1);
          }    
       console.log("selected remove user ",this.selectedUser);
    
    }
  }


  onChange(value){
    console.log(value);
    this.paginationSize=12/value;
   // console.log(value1.target.;value);
    
   this.userRow=value;
  
  }

  onChangePost(value){
    debugger;
    console.log(value);
    for(let i in this.postList)
    {
      if(value==this.postList[i].id){
        console.log(" hello success");
        console.log(" hello success",this.postList[i]);
        this.selectedPost=this.postList[i];
        
        this.currentPost=this.selectedPost.postName;
        console.log(" hello success",this.currentPost);
        this.newMember.postId=value;
        break;
      }
    }
    this.selectedPost=value
   // this.userList=this.userList;


  }

  // click on next butto 
  setGrouNameAndPost(){
    debugger;
    this.selectedUser==[];
    this.currentGroup=this.member.boadmasterName;
    
    this.newMember.groupName=this.member.boadmasterName;
    console.log("hello ",this.member);
    this.showUsers=true;
    this.saveorUpdateShow=true;
    this.getusersDetails();

    console.log("hello ",this.selectedPost);
  }

//creeate boarder
  saveAlldetails(){
    console.log("whole details ",this.newMember)
    this.postService.createBoardMember(this.newMember)
    .subscribe(data=>{
      console.log(" result ",data);
      this.showUsers=false;
      this.getBoardGroupDetails();
    })
  }
  goBack(){
    this.selectedUser==[];
    this.showUsers=false;
  }
  getAllMemberDetails(){
    this.getService.getAllMemberDetails()
    .subscribe(data=>{
      console.log(data);
      this.allBoardMemberList=data;

    })
  }

  getBoardGroupDetails(){
    this.getService.getBoardDetails()
    .subscribe(data=>{

      this.boardGroupList=data;
      console.log('list group ',this.boardGroupList)
    })
  }

  getBoardMemberDetails(group:any){
    console.log(" group ",group)
this.selectGroup=group.name;
    this.getService.getBoardMemberDetails(group.id)
    .subscribe(data=>{
      console.log("group memeber ",data)
      this.groupMemberList=data;
    })

}

addNewBoardMember(value){
  console.log("group details", value)
   this.newMember.groupName=value.id;
   this.newMember.postId=value.jobId.id,
   this.currentGroup=value.name;
   this.currentPost=value.jobId.postName;
   this.saveorUpdateShow=false;
this.getService.getUserDetailsNotBoardMember(value.id)
.subscribe(data=>{
  console.log("user ",data);
 this.showUsers=true;
  this.userList=data;
})


}

updateMemberdetails(){
  console.log("whole details ",this.newMember)
  this.postService.updateBoardMember(this.newMember)
  .subscribe(data=>{
    console.log(" result ",data);
    this.showUsers=false;
    this.getBoardGroupDetails();
  })
}

removememberfromBoard(value){
  console.log(" mmmmmm ",value);

  this.getService.deleteUserDetailsBoardMember(value.id)
  .subscribe(data=>{
    console.log(" delete ",data);
  })

}

}


