import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BordmemberComponent } from './bordmember.component';

describe('BordmemberComponent', () => {
  let component: BordmemberComponent;
  let fixture: ComponentFixture<BordmemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BordmemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BordmemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
