import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';

@Component({
  selector: 'app-center',
  templateUrl: './center.component.html',
  styleUrls: ['./center.component.css']
})
export class CenterComponent implements OnInit {
  public states: any = [];
  public cities: any = [];
  public centers: any = [];
  public user: any = {};
  public loading: boolean = false;
  public successField: boolean;
  public authenticationError:boolean;
  public noCenterCreated: boolean;
  public deleteField: boolean;
  public modalRef: BsModalRef;
  public successEdit: boolean;
  public centersName: any = {};

  constructor(private getService: GetService, private postService: PostService,
    private router: Router, private authServerProvider: AuthServerProvider,
    private principal: Principal, private modalService: BsModalService,
    private sharedService: SharedService) {
    this.user.stateName = 0;
    this.user.cityName = 0;
  }

  ngOnInit() {
    this.getState();
    this.getCreatedCenters();
  }

  getState() {
    this.getService.getState().subscribe(
      data => {
        this.states = data;
      },
      error => {
      });
  }

  getCity(value: any) {
    this.getService.getCity(value).subscribe(
      data => {
        this.cities = data;
        this.user.cityName = data[0].name;
      },
      error => {
      });
  }


  createCenter() {
    if ( this.user.stateName == 0 || this.user.cityName == 0) {
      return;
  }
    this.loading = true;
    this.successField = false;
    this.authenticationError=false;
    this.postService.createCenter(this.user).subscribe(data => {
      if (data.status == 201) {
        this.successField = true;
        this.authenticationError=false;
        this.loading = false;
      }
      this.getCreatedCenters();
      this.centers = data.json();
      this.user = {};
      this.user.stateName = 0;
      this.user.cityName = 0;

      setTimeout(() => {    
        this.successField = false;
      }, 5000);

    },
      error => {
        this.successField = false;
        this.loading = false;
        this.authenticationError =true;
        setTimeout(() => {    
          this.authenticationError = false;
        }, 5000);
      });
  }

  getCreatedCenters() {
    this.loading = true;
    this.noCenterCreated = false;
    this.getService.getCenters().subscribe(
      data => {
        this.centers = data;
        this.noCenterCreated = false;
        this.loading = false;
        if (this.centers < 1) {
          this.noCenterCreated = true;
        }
        console.log(this.centers);
      },
      error => {
        this.loading = false;
      });
  }


  deleteCenter(center) {
    this.deleteField = false;
    this.loading = true;
    this.postService.deleteCenter(center).subscribe(
      data => {
        this.loading = false;
        this.deleteField = true;
        // console.log("post Course :", data);
        // this.jobList = data;
        this.getCreatedCenters();
        setTimeout(() => {
          this.deleteField = false;
        }, 5000);
        //this.deleteField = false;
      },
      error => {
        // this.alertService.error(error);
        this.loading = false;
        this.deleteField = false;
      });

  }

  closeModal() {
    this.getCreatedCenters();
    this.modalRef.hide();
  }


  public editPost(template: TemplateRef<any>, value) {
    this.successEdit = false;
    this.centersName = JSON.parse(JSON.stringify(value));
    this.modalRef = this.modalService.show(template);
  }

  saveEditPost() {
    this.loading = true;
    this.successEdit = false;
    this.postService.updateCenter(this.centersName).subscribe(data => {
      if (data.status == 200) {
        this.successEdit = true;
        this.loading = false;
      }
      this.getCreatedCenters();
    },
      error => {
        this.successEdit = false;
        this.loading = false;
      });
  }

  createCriteria() {
    // this.sharedService.saveCenterData(centerDetail);
    this.router.navigate(['/admin/allocate/center']);
  }

  logout() {
    this.authServerProvider.logout().subscribe();
    this.principal.authenticate(null);
    this.router.navigate(['/admin/login']);
  }
}
