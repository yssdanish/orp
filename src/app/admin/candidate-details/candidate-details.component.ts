import { Component, TemplateRef, ViewChild, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { ModalComponent } from '../../shared/Model/shared-modal.component';
import { CandidateListModalComponent } from '../candidate-list-modal/candidate-list-modal.component';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
    selector: 'app-candidate-details',
    templateUrl: './candidate-details.component.html',
    styleUrls: ['./candidate-details.component.css']
})
export class CandidateDetailsComponent implements OnInit {

    bsModalRef: BsModalRef;
    public modalPopup: boolean;
    public candidateList: any = [];
    public categories = [];
    public courses = [];
    public loading: boolean;
    public popupvalues: any;
    public interValue: any = {};
    public historyValue: any = {};
    public jobValue: any;
    public noContent: boolean;
    public message: string;
    maxSize: number = 5;
    bigTotalItems: number = 175;
    bigCurrentPage: number = 1;
    numPages: number = 0;
    public alreadyShortlisted: any = [];
    public shortlistMessage: boolean = false;;
    // pathVar: any;

    public filter: any = {};

    constructor(private modalService: BsModalService,
        private getService: GetService,
        private postService: PostService,
        private datePipe: DatePipe,
        private authServerProvider: AuthServerProvider,
        private principal: Principal, private router: Router) {
        // this.pathVar = "../../../../assets/images/office_text.jpg";
        this.filter.category = null;
        this.filter.qualification = null;
    }

    @ViewChild(CandidateListModalComponent) popUp: CandidateListModalComponent;

    ngOnInit() {
        this.getCandidateList();
        this.getCategory();
        this.getAllCourse();
        this.shortlistMessage = false;
    }

    getAllCourse() {
        this.getService.getAllCourse().subscribe(
            data => {
                this.courses = data;
                console.log("courser list ", this.courses);
            },
            error => {

            }
        );

    }

    getCategory() {
        // let value = "3631721324";
        this.getService.getCategory().subscribe(
            data => {
                this.categories = data;
            },
            error => {
                //  this.alertService.error(error);
                // this.loading = false;
            });
    }



    getCandidateList() {
        this.loading = true;
        this.noContent = false;
        // let value = "3631721324";
        this.getService.getCandidateList().subscribe(
            data => {
                this.candidateList = data;
                this.filter = {};
                this.filter.category = null;
                this.filter.qualification = null;
                this.loading = false;
                this.noContent=false;
                if (this.candidateList < 1) {
                    this.noContent = true;
                }
            },
            error => {
                this.loading = false;
            });
    }

    openModal(_condidatelists) {
        this.popUp.showModal(_condidatelists);
    }

    applyFilter() {
        this.noContent = false;
        this.loading = true;
        this.postService.applyFilter(this.filter).subscribe(
            data => {
                this.loading = false;
                if (data.status == 204) {
                    this.noContent = true;
                    this.candidateList.length = 0;
                    this.message = "No content found";
                }
                if (data.status == 200) {
                    this.candidateList = data.json();
                    // console.log("Candidate list: ", this.candidateList);
                }

            },
            error => {
                this.loading = false;
            });
    }

    sendAdmitCard(index) {
        this.loading = true;
        this.historyValue.registrationId = this.candidateList[index].registrationId;
        this.historyValue.firstName = this.candidateList[index].firstName;
        this.historyValue.lastName = this.candidateList[index].lastName;
        this.historyValue.recruitmentZone = this.candidateList[index].recruitmentZone;
        this.historyValue.middleName = this.candidateList[index].middleName;
        this.historyValue.fatherName = this.candidateList[index].fatherName;
        this.historyValue.motherName = this.candidateList[index].motherName;
        this.historyValue.dateOfBirth = this.candidateList[index].dateOfBirth;
        this.historyValue.gender = this.candidateList[index].gender;
        this.historyValue.mobileNumber = this.candidateList[index].mobileNumber;
        this.historyValue.emailAddress = this.candidateList[index].emailAddress;
        this.historyValue.nationality = this.candidateList[index].nationality;
        this.postService.sendAdmitCard(this.historyValue).subscribe(
            data => {
                this.loading = false;
            },
            error => {
                this.loading = false;
            });
    }


    pageChanged(event: any): void {
        // console.log('Page changed to: ' + event.page);
        // console.log('Number items per page: ' + event.itemsPerPage);
        // console.log('Page changed to: ' + event.page);
        // console.log('Number items per page: ' + event.itemsPerPage);
    }

    shortlist() {
        this.loading = true;
        this.postService.shortlistCandidate(this.candidateList).subscribe(
            data => {
                this.loading = false;
                this.shortlistMessage = true;
                this.alreadyShortlisted = data.json();
            },
            error => {
                this.loading = false;

            });
        setTimeout(() => {
            this.shortlistMessage = false;
        }, 5000);

    }

    reset() {
        this.filter = {};
        this.filter.category = null;
        this.filter.qualification = null;
    }

    // createPdf(clist) {
    //     pdfMake.vfs = pdfFonts.pdfMake.vfs;
    //     let pdfValue = {

    //         info: {
    //             title: 'admitCard ' + clist.registrationId,
    //             creationDate: 'Download PDF',
    //         },

    //         content: [

    //             {
    //                 columns: [

    //                     {

    //                         width: 80,
    //                         height: 60
    //                     },

    //                     {
    //                         stack: [

    //                             { text: 'BORDER ROADS ORGANIZATION', bold: true, fontSize: 14, },
    //                             { text: 'Creates, Connect & Cares...' },
    //                             { text: 'GREF & Record Centre, Pune' }

    //                         ],
    //                         arial: true,
    //                         alignment: 'center'


    //                     },



    //                 ]

    //             },

    //             { text: '', margin: [10, 20, 10, 20] },
    //             { text: 'Applied For :  ' + clist.jobDetails.postName, style: 'textgap', bold: true, alignment: 'center', },

    //             { text: 'Registraion id : ' + clist.registrationId, bold: true, style: 'textgap' },
    //             { text: 'Candidate Name : ' + clist.firstName + clist.middleName, style: 'textgap' },
    //             { text: 'Category : ' + clist.category, style: 'textgap' },
    //             { text: 'Father Name:  ' + clist.fatherName, style: 'textgap' },
    //             { text: 'Date Of Birth :  ' + this.datePipe.transform(clist.dateOfBirth, 'dd-MM-yyyy'), style: 'textgap' },
    //             { text: 'Address : ' + clist.postalAddress, style: 'textgap' },
    //             { text: 'State : ' + clist.postalState, style: 'textgap' },
    //             { text: 'City : ' + clist.postalCity, style: 'textgap' },
    //             { text: 'PinCode : ' + clist.postalPincode, style: 'textgap' },

    //             {

    //                 width: 150,
    //                 height: 200,
    //                 alignment: 'right'
    //             },

    //             {
    //                 columns: [

    //                     { text: 'Full signature of candidate', bold: true },
    //                     { text: 'Sign at the time of physical test', bold: true },
    //                     { text: 'Sign at the time of practical test', bold: true },


    //                 ],
    //                 margin: [0, 100, 0, 0]

    //             },

    //             {
    //                 columns: [
    //                     { text: 'Sign at the time of joining service', bold: true },
    //                     { text: 'Sign at the time of written test', bold: true },
    //                     { text: 'Left hand thumb impression', bold: true }
    //                 ],
    //                 margin: [0, 50, 0, 0]

    //             },



    //             {
    //                 ol: [

    //                     'Candidates should bring the following items with them at the time of appearing for the examination:',
    //                     {
    //                         ul: [
    //                             { text: 'Admit Card', bold: true },
    //                             { text: 'Ball Pen (Black Ink), HB Pencil, Scale, Eraser etc.', bold: true },
    //                             { text: 'Admit card duly filled and signed at the space provided', bold: true },
    //                             { text: 'Photo Identity proof (Driving License, Voters ID, passport, PAN card etc.) in original for verification.', bold: true },
    //                         ]
    //                     },

    //                     'Candidates will not be allowed into examination hall without admit card & photo identity proof in original.',
    //                     'Candidates should reach the exam centre one hour before the scheduled time of examination.',
    //                     'The written examination will be of Objective-type questions consisting of General Knowledge, General Arithmetic and Basic Science and would be conducted in OMR sheet. Instructions given in the OMR sheet shall be read before starting to mark the answers. ',
    //                     'Candidates are not allowed to carry any textual material, printed or written bits of paper, mobile phone, electronic device or any other material.',
    //                     'Ensure that all the pages in the question booklet are available. In case of discrepancy, the candidate should immediately report the matter to the invigilator for the replacement of booklet. ',
    //                     'HB pencil hall be used for shading the answers in OMR sheet.',
    //                     'Only name and signature should be made in pen at the space provided in the OMR sheet.',
    //                     'Separate sheet will be given for rough work. Candidate should not mark or write anything on the question booklet or tracing side of the OMR sheet. ',
    //                     'Candidate should not leave the Examination Hall without handing over their original OMR/Answer sheet alongwith Question booklet and rough sheets to the Invigilator on duty in the Room/Hall and sign the attendance sheet. ',
    //                     'If candidates found committing any malpractice, action will be taken against them as per rules.',
    //                     'Candidates are not allowed to leave the examination hall till the end of the session.',
    //                     'Question paper will be in Hindi and English. Candidates have option to choose any question paper of any of the above language.',
    //                     'No request for postponement of Written Exam, Change of Venue, etc. will be entertained under any circumstances.',
    //                     'Candidates who qualify the written examination will be called for Interview at a later date which will be intimated separately.',
    //                     'Result of the written examination will be published in our website only, hence candidates are advised to visit our website in regularly for further instructions / updates.',
    //                     { text: 'www.bro.gov.in', link: 'http://www.bro.gov.in/' },
    //                     'On behalf of BRO,Pune, we convey our best wishes for the effective performance in the examination.',
    //                 ],

    //                 pageOrientation: 'portrait',
    //                 pageBreak: 'before'
    //             }

    //         ],

    //         styles: {
    //             textgap: {
    //                 margin: [10, 1, 10, 10],
    //                 fontSize: 10,
    //                 arial: true,
    //             },

    //         },

    //     };
    //     pdfMake.createPdf(pdfValue).open();
    // }

    logout() {
        this.authServerProvider.logout().subscribe();
        this.principal.authenticate(null);
        this.router.navigate(['/admin/login']);
    }

}
