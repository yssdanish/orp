import { Component, ViewChild, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { PostService, GetService } from '../../shared/service/index';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'app-admin-register',
  templateUrl: './admin-register.component.html',
  styleUrls: ['./admin-register.component.css']
})
export class AdminRegisterComponent implements OnInit {

  ngOnInit() {
  }

  public admin: any = {};
  public returnUrl: string;
  public authenticationError: boolean;
  public loading = false;
  public successDoc: boolean;
  public emailExists: boolean = false;

  constructor(private route: ActivatedRoute,
    private router: Router, private postService: PostService,
    private getService: GetService) {
    this.admin.zone = 0;
    this.admin.authorities = 0;

  }
  adminRegistration() {
    if (this.emailExists) {
      return;
    }
    this.loading = true;
    this.successDoc = false;
    this.postService.adminRegister(this.admin).subscribe(data => {
      this.loading = false;
      this.successDoc = true;

    },
      error => {
        console.log("error saving data", error);
        this.loading = false;
        this.successDoc = false;
        this.authenticationError = true;
      });
  }

  public roles: any = [
    { name: "Admin", value: "ROLE_ADMIN" },
    { name: "User", value: "ROLE_USER" }
  ];

  public zones: any = [
    { id: 0, name: "Pune" },
    { id: 1, name: "Rishikesh" },
    { id: 2, name: "Tezpur" }
  ];

  emailCheck(email: string) {
    this.getService.emailCheck(email).subscribe(
      data => {
        console.log("Email check :", data);
        this.emailExists = data.present;
      },
      error => {
      });
  }
}
