import { Component, TemplateRef, ViewChild, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { ModalComponent } from '../../shared/Model/shared-modal.component';
import { CandidateListModalComponent } from '../candidate-list-modal/candidate-list-modal.component';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  bsModalRef: BsModalRef;

  public categories = [];
  public loading: boolean;
 // public admin: any = [];
 public lists: any = [];
  public noContent: boolean;
  public successField: any;
  //public shortlistCandidate = [];
  public authenticationError: any;
  public marksMessage: boolean = false;
  public alreadySave = [];

  public clickCheck: boolean = true;
  public disableCheck: boolean = true;
 
  public selectedValue = [];
  maxSize: number = 5;
  bigTotalItems: number = 175;
  bigCurrentPage: number = 1;
  numPages: number = 0;
  constructor(private modalService: BsModalService,
    private getService: GetService,
    private postService: PostService,
    private datePipe: DatePipe,
    private authServerProvider: AuthServerProvider,
    private principal: Principal, private router: Router) { }

  ngOnInit() {
    this.getshortListedCandidates();
  }

  public admin: any = {
  };

  public shortlistCandidate: any [] = [
    {
      candidatemarks : ''
    }
    
  ];

  // restrictNumeric(event) {
  //   return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  // }

  getshortListedCandidates() {
    this.loading = true;
    this.noContent = false;
    this.getService.getCandidates().subscribe(
      data => {
        this.shortlistCandidate = data;
        this.loading = false;
        if (this.shortlistCandidate.length < 1) {
          this.noContent = true;
        }
      },
      error => {
        this.loading = false;
      });
  }


  changeCheck(event, index) {
    if (event.target.checked) {
      this.selectedValue.push(this.shortlistCandidate[index]);
    }
    else {
      this.selectedValue.splice(index, 1);
    }
  }

  // saveMarks() {

  //   this.loading = true;
  //   this.shortlistCandidate = this.getCandidates();
  //   this.postService.saveMarks(this.shortlistCandidate).subscribe(
  //     data => {
  //       this.loading = false;
  //       this.marksMessage = true;
  //       this.alreadySave = data.json();
  //     },
  //     error => {
  //       this.loading = false;

  //     });
  //   setTimeout(() => {
  //     this.marksMessage = false;
  //   }, 5000);

  // }

  saveMarks(){
    this.loading = true;
    this.admin = this.selectedValue;
    this.postService.saveMarks(this.admin ).subscribe(
      data => {
        if (data.status == 200) {
          this.loading = false;
          this.marksMessage = true;
         // this.getshortListedCandidates();
        }
      },
      error => {
        this.loading = false;

      });
    setTimeout(() => {
      this.marksMessage = false;
    }, 5000);

  }
 

  logout() {
    this.authServerProvider.logout().subscribe();
    this.principal.authenticate(null);
    this.router.navigate(['/admin/login']);
  }

}


