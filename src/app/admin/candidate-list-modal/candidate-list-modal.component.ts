import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-candidate-list-modal',
  templateUrl: './candidate-list-modal.component.html',
  styleUrls: ['./candidate-list-modal.component.css']
})
export class CandidateListModalComponent implements OnInit {

  public popupvalues = {};
  public jobDetails = {};
  public certificate = {};
  public highSchool = {};
  public interSchool = {};
  public graduation = {};
  public technical = {};
  public experience = {};
  public driving = {};
  public typing = {};
  public stenography = {};
  public nullInter: boolean;
  public nullTyping: boolean;
  public nullStenography: boolean;
  public nullGraduation: boolean;
  public nullPgGraduation: boolean;
  public nullDiploma: boolean;
  public nullDegree: boolean;
  public nullIti: boolean;
  public nullExperience: boolean;
  public nullDrivingLmv: boolean;
  public nullDrivinghmv: boolean;
  public nullDrivingRoad: boolean;
  public nullDrivingOeg: boolean;
  public nullDriving: boolean;

  @ViewChild('childModal') public childModal: ModalDirective;

  constructor() { }

  ngOnInit() {
  }

  public showModal(modalValue): void {
    console.log(modalValue);
    this.nullInter = false;
    this.nullTyping = false;
    this.nullStenography = false;
    this.nullGraduation = false;
    this.nullPgGraduation = false;
    this.nullDiploma = false;
    this.nullDegree = false;
    this.nullIti = false;
    this.nullExperience = false;
    this.nullDriving = false;
    this.nullDrivingLmv = false;
    this.nullDrivinghmv = false;
    this.nullDrivingRoad = false;
    this.nullDrivingOeg = false;
    this.popupvalues = modalValue;
    this.jobDetails = modalValue.jobDetails;
    this.certificate = modalValue.certificate;
    this.highSchool = modalValue.essential;
    this.graduation = modalValue.graduation;
    this.technical = modalValue.technical;
    this.experience = modalValue.experience;
    this.driving = modalValue.driving;
    this.childModal.show();

    if (modalValue.essential.interInstitution != 'null' && modalValue.essential.interInstitution !== null && modalValue.essential.interInstitution != 'undefined' && modalValue.essential.interInstitution !== undefined) {
      this.nullInter = true;
    }

    if (modalValue.graduation.ugInstitution != 'null' && modalValue.graduation.ugInstitution !== null && modalValue.graduation.ugInstitution !== undefined && modalValue.graduation.ugInstitution != 'undefined') {
      this.nullGraduation = true;
    }
    if (modalValue.graduation.pgInstitution != 'null' && modalValue.graduation.pgInstitution !== null && modalValue.graduation.pgInstitution !== undefined && modalValue.graduation.pgInstitution != 'undefined') {
      this.nullPgGraduation = true;
    }
    if (modalValue.technical.diplomaSpecialization != 'null' && modalValue.technical.diplomaSpecialization !== null && modalValue.technical.diplomaSpecialization !== undefined && modalValue.technical.diplomaSpecialization != 'undefined') {
      this.nullDiploma = true;
    }
    if (modalValue.technical.degreeSpecialization != 'null' && modalValue.technical.degreeSpecialization !== null && modalValue.technical.degreeSpecialization !== undefined && modalValue.technical.degreeSpecialization != 'undefined') {
      this.nullDegree = true;
    }
    if (modalValue.technical.itiSpecialization != 'null' && modalValue.technical.itiSpecialization !== null && modalValue.technical.itiSpecialization !== undefined && modalValue.technical.itiSpecialization != 'undefined') {
      this.nullIti = true;
    }
    if (modalValue.experience.companyName != 'null' && modalValue.experience.companyName !== null && modalValue.experience.companyName != 'undefined' && modalValue.experience.companyName !== undefined) {
      this.nullExperience = true;
    }
    if (modalValue.driving.issuingAuthorityLmv != 'null' && modalValue.driving.issuingAuthorityLmv !== null && modalValue.driving.issuingAuthorityLmv !== undefined && modalValue.driving.issuingAuthorityLmv != 'undefined') {

      this.nullDrivingLmv = true;
      this.nullDriving = true;
    }
    if (modalValue.driving.issuingAuthorityHlmv != 'null' && modalValue.driving.issuingAuthorityHlmv !== null && modalValue.driving.issuingAuthorityHlmv != 'undefined' && modalValue.driving.issuingAuthorityHlmv !== undefined) {
      this.nullDrivinghmv = true;
      this.nullDriving = true;
    }
    if (modalValue.driving.issuingAuthorityRoad != 'null' && modalValue.driving.issuingAuthorityRoad !== null && modalValue.driving.issuingAuthorityRoad != 'undefined' && modalValue.driving.issuingAuthorityRoad !== undefined) {
      this.nullDrivingRoad = true;
      this.nullDriving = true;
    }

    if (modalValue.driving.issuingAuthorityOeg != 'null' && modalValue.driving.issuingAuthorityOeg !== null && modalValue.driving.issuingAuthorityOeg != 'undefined' && modalValue.driving.issuingAuthorityOeg !== undefined) {
      this.nullDrivingOeg = true;
      this.nullDriving = true;
    }


  }

  public hideModal(): void {
    this.childModal.hide();
  }

}
