import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateListModalComponent } from './candidate-list-modal.component';

describe('CandidateListModalComponent', () => {
  let component: CandidateListModalComponent;
  let fixture: ComponentFixture<CandidateListModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateListModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateListModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
