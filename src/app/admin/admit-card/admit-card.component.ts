import { Component, TemplateRef, ViewChild, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetService, SharedService, PostService } from '../../shared/service/index';


import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
    selector: 'app-admit-card',
    templateUrl: './admit-card.component.html',
    styleUrls: ['./admit-card.component.css']
})
export class admitcardComponent implements OnInit {

    bsModalRef: BsModalRef;
    public modalPopup: boolean;
    public candidateList = [];
    public loading: boolean;
   
    public interValue: any = {};
    public historyValue: any = {};
    public jobValue: any;
    public noContent: boolean;
    public message: string;
    maxSize: number = 5;
    bigTotalItems: number = 175;
    bigCurrentPage: number = 1;
    numPages: number = 0;

    recruitZone:string = null;
    rollNumber:number = null;
    candidateName:string = null;
    registrationId:number = null;
    dob:number = null;
    caste:string =null;
    examCentre:string = null;
    reportingFrom:string = null;
    reportingTo:string = null;
    address:string = null;

    details:{ recruitZone:string, rollNumber:number,  candidateName :string, registrationId:number ,dob:number ,  caste:string , examCentre:string,   reportingFrom:string ,reportingTo:string,  address:string}[] = [];


    

    pathVar: any;

    public filter: any = {};

    constructor() {
       
    }

  

    ngOnInit() {
       
    }



   

downloadadmitcard() {

   
   
    this.details.push({ recruitZone:this.recruitZone, rollNumber:this.rollNumber,  candidateName :this.candidateName, registrationId:this.registrationId ,dob:this.dob ,  caste:this.caste , examCentre:this.examCentre,   reportingFrom:this.reportingFrom ,reportingTo:this.reportingTo,  address:this.address});
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    let pdfValue = {

        info: {
            title: 'admitCard ' + this.registrationId,
            creationDate: 'Download PDF',
        },

        content: [
            { text: 'recruitZone : ' + this.recruitZone },
            { text: 'rollNumber : ' + this.rollNumber },
            { text: 'candidateName : ' + this.candidateName },
            { text: 'dob : ' + this.dob },
            { text: 'caste : ' + this.caste },
            { text: 'examCentre : ' + this.examCentre },
            { text: 'reportingFrom : ' + this.reportingFrom },
            { text: 'reportingTo : ' + this.reportingTo },
            { text: 'address : ' + this.address },

        ],



    };
    pdfMake.downloadadmitcard(pdfValue).download();
 }


public recruitmentzone: any = [
    { id: 0, name: "RISHIKESH" },
    { id: 0, name: "PUNE" },
    { id: 0, name: "TEZPUR" }
];
}