import { Component, OnInit } from '@angular/core';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Http, Response } from '@angular/http';
import { PostService, GetService, SharedService } from '../../shared/service/index';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { debuglog } from 'util';

@Component({
  selector: 'app-job-field',
  templateUrl: './job-field.component.html',
  styleUrls: ['./job-field.component.css']
})
export class JobFieldComponent implements OnInit {

  public reqperCheck: boolean;
  public essentialCheck: boolean;
  public highCheck: boolean;
  public interCheck: boolean;
  public typingCheck: boolean;
  public stenographyCheck: boolean;
  public otherCheck: boolean;
  public gradCheck: boolean;
  public postgradCheck: boolean;
  public technicalCheck: boolean;
  public degreeCheck: boolean;
  public itiCheck: boolean;
  public diplomaCheck: boolean;
  public experienceCheck: boolean;
  public licenceCheck: boolean;
  public lmvCheck: boolean;
  public hgmvCheck: boolean;
  public oegCheck: boolean;
  public rrCheck: boolean;
  public nccBCheck: boolean;
  public nccCCheck: boolean;
  public sportsCheck: boolean;
  public successField: boolean;
  public additionalCheck: boolean;
  public firstaidCheck: boolean;
  public authenticationError: boolean;
  public loading = false;
  public jobId;
  public successDoc: boolean;
  public jobs: any = {};

  public invalidagecalFlag: boolean;
  public invalidagemsg: String;

  public applyinvalidmsg: String;
  public applydateFlag: boolean;

  public feeinvalidmsg: String;
  public feedateFlag: boolean;

  lastCalDate = new Date();

  ngOnInit() {

  }

  public admin: any = {
  };

  public jobAmountDetails: any = [
    {
      categoryName: 'UR',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    },
    {
      categoryName: 'SC',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    },
    {
      categoryName: 'ST',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    },

    {
      categoryName: 'OBC',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    },

    {
      categoryName: 'PH',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    },
    {
      categoryName: 'ESM',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    }, {
      categoryName: 'GREF',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    },
    {
      categoryName: 'OTHERS',
      postNumber: '',
      amount: '',
      minAge: '',
      maxAge: '',
      ageRelax: '',
      percentRelax: ''
    }

  ];

  minDateRegFrom = new Date();
  maxDateRegFrom = new Date(2100, 5, 20);
  minDateRegTo = new Date();
  maxDateRegTo = new Date(2100, 5, 20);

  minDateFeeFrom = new Date();
  maxDateFeeFrom = new Date(2100, 5, 20);
  minDateFeeTo = new Date();
  maxDateFeeTo = new Date(2100, 5, 20);

  minDateJobFrom = new Date();
  maxDateJobFrom = new Date(2100, 5, 20);
  minDateJobTo = new Date();
  maxDateJobTo = new Date(2100, 5, 20);


  public documentSize: boolean = false;
  public documentType: boolean = false;
  public documentRequired: boolean = false;
  public selectedValue = [];
  public otherQualiMsg: string;
  public technicalQualiMsg:string;
  public drivingLicenseMsg:String;
  otherQualiMsgis: boolean;
  technicalQualiMsgis:boolean;
  drivingLicenseMsgis:boolean;
  public essentialQualiMsgis:boolean;
  public essentialQualiMsg:string;

  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = "theme-red";

  constructor(public postService: PostService,
    private sharedService: SharedService, private router: Router,
    private datePipe: DatePipe, private authServerProvider: AuthServerProvider,
    private principal: Principal) {

    this.jobs = sharedService.getJobData();
    if (this.jobs == undefined) {
      this.router.navigate(['/admin/job/post']);
    }
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }


  changeCheck(event, index) {
    if (event.target.checked) {
      this.selectedValue.push(this.jobAmountDetails[index]);
    }
    else {
      this.selectedValue.splice(index, 1);
    }
  }

  saveJobFields() {
    this.otherQualiMsgis = false;
    // this.admin.activeDate = this.datePipe.transform(this.admin.activeDate, 'dd-MM-yyyy');
    // this.admin.expiryDate = this.datePipe.transform(this.admin.expiryDate, 'dd-MM-yyyy');
    // this.admin.feeSubmitDate = this.datePipe.transform(this.admin.feeSubmitDate, 'dd-MM-yyyy');
    // this.admin.feeSubmitLastDate = this.datePipe.transform(this.admin.feeSubmitLastDate, 'dd-MM-yyyy');

    this.invalidagecalFlag = false;
    if (this.admin.expiryDate < this.lastCalDate) {
      this.invalidagecalFlag = true;
      this.invalidagemsg = " Expiry Date can't be smaller than the current date..";
      return;

    }

    this.applydateFlag = false;
    if (this.admin.activeDate >= this.admin.expiryDate) {

      this.applydateFlag = true;
      this.applyinvalidmsg = "Job Start Date Can't be greater & Equal than End Date..";
      this.loading = false;
      return this.applydateFlag;

    }

    this.feedateFlag = false;
    if (this.admin.feeSubmitDate >= this.admin.feeSubmitLastDate) {
      this.feedateFlag = true;
      this.feeinvalidmsg = "Fee Submit Date can't be greater & Equal than Fee Submit Last Date..";
      this.loading = false;
      return this.feedateFlag;
    }

    if (this.admin.essentialQualificationForm && !this.admin.highSchoolForm && !this.admin.intermediateForm) {
      this.essentialQualiMsg = "Please Choose 'Yes' Any of them"
      return this.essentialQualiMsgis = true;
    }

    if (this.admin.otherQualificationForm && !this.admin.graduationForm && !this.admin.postGraduationForm) {
      this.otherQualiMsg = "Please Choose 'Yes' Any of them"
      return this.otherQualiMsgis = true;
    }

    if(this.admin.technicalForm && !this.admin.degreeForm && !this.admin.diplomaForm && !this.admin.itiForm){
      this.technicalQualiMsg = "Please Choose 'Yes' Any of them";
      return this.technicalQualiMsgis = true;
    }


    if(this.admin.drivingLicenseForm && !this.admin.drivingLicenseLmvForm && !this.admin.drivingLicenseHgmvForm && !this.admin.drivingLicenceRoadForm && !this.admin.drivingLicenceOegForm){
      this.drivingLicenseMsg="Pleas Choose 'Yes' Any of them";
      return this.drivingLicenseMsgis = true;
    }
    this.loading = true;

    this.admin.categoryAmount = this.selectedValue;
    this.successField = false;
    this.authenticationError = false;
    this.admin.postName = this.jobs.jobTitle;
    this.admin.postProfile = this.jobs.jobDesc;
    this.postService.saveJobFields(this.admin).subscribe(data => {
      // console.log("Saved data", data.json());
      if (data.status == 200) {
        this.successField = true;
        this.loading = false;
        this.authenticationError = false;
        this.jobId = data.json().id;
      }
      // setTimeout(() => {    
      //   this.successField = false;
      // }, 5000);
    },
      error => {
        // console.log("error saving data", error);
        this.loading = false;
        this.successField = false;
        this.authenticationError = true;
        setTimeout(() => {
          this.authenticationError = false;
        }, 5000);
      });
  }
  restrictNumeric(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  documentChange(event) {
    this.documentSize = false;
    this.documentType = false;
    this.documentRequired = false;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.documentRequired = true;
      let file: File = fileList[0];
      let fileType = file.type.split('/')[1];
      let matchType = '|pdf|'.indexOf(fileType) >= 1;
      // this.admin.postDetailsDocument = file;

      //between 250kb to 300kb
      if (256000 < file.size && file.size < 307200) {
        if (matchType) {
          this.admin.postDetailsDocument = file;
        }
        else {
          return this.documentType = true;
        }
      }
      else {
        return this.documentSize = true;
      }
    }
  }

  uploadDocument() {
    if (!this.documentRequired || this.documentType || this.documentSize) {
      return;
    }
    this.loading = true;
    this.successDoc = false;
    this.authenticationError = false;
    this.postService.uploadDocument(this.admin.postDetailsDocument, this.jobId).subscribe(data => {
      if (data.status == 200) {
        this.successDoc = true;
        this.loading = false;
        this.authenticationError = false;
      }

    },
      error => {
        this.authenticationError = true;
        this.loading = false;

        console.log("error", error);
        setTimeout(() => {
          this.authenticationError = false;
        }, 5000);
      });
  }

  minperChange(value: boolean) {
    if (value) {
      this.reqperCheck = true;
    } else {
      this.reqperCheck = false;
    }
  }

  essentialChange(value: boolean) {
    if (value) {
      this.essentialCheck = true;
    } else {
      this.essentialCheck = false;
    }
  }

  highChange(value: boolean) {
    if (value) {
      this.highCheck = true;
    } else {
      this.highCheck = false;
    }
  }
  interChange(value: boolean) {
    if (value) {
      this.interCheck = true;
    } else {
      this.interCheck = false;
    }
  }

  typingChange(value: boolean) {
    if (value) {
      this.typingCheck = true;
    } else {
      this.typingCheck = false;
    }
  }

  stenoChange(value: boolean) {
    if (value) {
      this.stenographyCheck = true;
    } else {
      this.stenographyCheck = false;
    }
  }

  otherChange(value: boolean) {
    if (value) {
      this.otherCheck = true;
    } else {
      this.otherCheck = false;
    }
  }

  gradChange(value: boolean) {
    if (value) {
      this.gradCheck = true;
    } else {
      this.gradCheck = false;
    }
  }

  postGradChange(value: boolean) {
    if (value) {
      this.postgradCheck = true;
    } else {
      this.postgradCheck = false;
    }
  }

  technicalChange(value: boolean) {
    if (value) {
      this.technicalCheck = true;
    } else {
      this.technicalCheck = false;
    }
  }

  degreeChange(value: boolean) {
    if (value) {
      this.degreeCheck = true;
    } else {
      this.degreeCheck = false;
    }
  }
  itiChange(value: boolean) {
    if (value) {
      this.itiCheck = true;
    } else {
      this.itiCheck = false;
    }
  }
  diplomaChange(value: boolean) {
    if (value) {
      this.diplomaCheck = true;
    } else {
      this.diplomaCheck = false;
    }
  }

  experienceChange(value: boolean) {
    if (value) {
      this.experienceCheck = true;
    } else {
      this.experienceCheck = false;
    }
  }
  licenceChange(value: boolean) {
    if (value) {
      this.licenceCheck = true;
    } else {
      this.licenceCheck = false;
    }
  }
  lmvChange(value: boolean) {
    if (value) {
      this.lmvCheck = true;
    } else {
      this.lmvCheck = false;
    }
  }
  hgmvChange(value: boolean) {
    if (value) {
      this.hgmvCheck = true;
    } else {
      this.hgmvCheck = false;
    }
  }
  oegChange(value: boolean) {
    if (value) {
      this.oegCheck = true;
    } else {
      this.oegCheck = false;
    }
  }
  roadChange(value: boolean) {
    if (value) {
      this.rrCheck = true;
    } else {
      this.rrCheck = false;
    }
  }
  nccBChange(value: boolean) {
    if (value) {
      this.nccBCheck = true;
    } else {
      this.nccBCheck = false;
    }
  }

  nccCChange(value: boolean) {
    if (value) {
      this.nccCCheck = true;
    } else {
      this.nccCCheck = false;
    }
  }

  additionalCertiChange(value: boolean) {
    if (value) {
      this.additionalCheck = true;
    } else {
      this.additionalCheck = false;
    }
  }

  firstaidCertiChange(value: boolean) {
    if (value) {
      this.firstaidCheck = true;
    } else {
      this.firstaidCheck = false;
    }
  }

  // sportsChange(value: boolean) {
  //   if (value) {
  //     this.sportsCheck = true;
  //   } else {
  //     this.sportsCheck = false;
  //   }
  // }

  logout() {
    this.authServerProvider.logout().subscribe();
    this.principal.authenticate(null);
    this.router.navigate(['/admin/login']);
  }

}
