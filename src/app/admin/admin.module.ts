import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, CarouselModule, ModalModule, TabsModule } from 'ngx-bootstrap';
import { OrpSharedModule } from '../shared';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalComponent } from '../shared/Model/shared-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import {
    adminState,
    AuditsComponent,
    UserMgmtComponent,
    UserDialogComponent,
    UserDeleteDialogComponent,
    UserMgmtDetailComponent,
    UserMgmtDialogComponent,
    UserMgmtDeleteDialogComponent,
    LogsComponent,
    JhiMetricsMonitoringModalComponent,
    JhiMetricsMonitoringComponent,
    JhiHealthModalComponent,
    JhiHealthCheckComponent,
    JhiConfigurationComponent,
    JhiDocsComponent,
    AuditsService,
    JhiConfigurationService,
    JhiHealthService,
    JhiMetricsService,
    GatewayRoutesService,
    JhiGatewayComponent,
    LogsService,
    UserResolvePagingParams,
    UserResolve,
    UserModalService,
    JobFieldComponent
} from './';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { JobPostComponent } from './job-post/job-post.component';
import { CandidateDetailsComponent } from './candidate-details/candidate-details.component';
import { ReportComponent } from './report/report.component';
import { ExamComponent } from './exam/exam.component';
import { AdminRegisterComponent } from './admin-register/admin-register.component';
import { LoadingModule } from 'ngx-loading';
import { CandidateListModalComponent } from './candidate-list-modal/candidate-list-modal.component';
import { admitcardComponent } from './admit-card/admit-card.component';
import { BordmemberComponent } from './bordmember/bordmember.component';

@NgModule({
    imports: [
        OrpSharedModule,
        RouterModule.forRoot(adminState, { useHash: true }),
        CarouselModule.forRoot(), BsDatepickerModule.forRoot(), NgxPaginationModule,
        TabsModule.forRoot(), LoadingModule, ModalModule.forRoot()
        /* jhipster-needle-add-admin-module - JHipster will add admin modules here */
    ],
    declarations: [
        AuditsComponent,
        UserMgmtComponent,
        UserDialogComponent,
        UserDeleteDialogComponent,
        UserMgmtDetailComponent,
        UserMgmtDialogComponent,
        UserMgmtDeleteDialogComponent,
        LogsComponent,
        JhiConfigurationComponent,
        JhiHealthCheckComponent,
        JhiHealthModalComponent,
        JhiDocsComponent,
        JhiGatewayComponent,
        JhiMetricsMonitoringComponent,
        JhiMetricsMonitoringModalComponent,
        AdminLoginComponent,
        JobPostComponent,
        JobFieldComponent,
        CandidateDetailsComponent,
        ReportComponent,
        ExamComponent,
        AdminRegisterComponent,
        ModalComponent,   
        CandidateListModalComponent,
        admitcardComponent,
        BordmemberComponent         
    ],
    entryComponents: [
        UserMgmtDialogComponent,
        UserMgmtDeleteDialogComponent,
        JhiHealthModalComponent,
        JhiMetricsMonitoringModalComponent,
        CandidateListModalComponent
    ],
    providers: [
        AuditsService,
        JhiConfigurationService,
        JhiHealthService,
        JhiMetricsService,
        GatewayRoutesService,
        LogsService,
        UserResolvePagingParams,
        UserResolve,
        UserModalService,
        BsModalService,
        BsModalRef
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrpAdminModule {}
