import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../shared/login/login.service';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  public admin: any = {};
  public returnUrl: string;
  public authenticationError: boolean;
  public date: any;
  public date1:any;
  public loading = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private eventManager: JhiEventManager
  ) {
    this.date = new Date().getFullYear();
        let dateVal = new Date().getFullYear() +1;
        this.date1 = dateVal.toString().slice(2);
  }

  ngOnInit() {

  }
  loginAdmin() {
    this.loading = true;
    this.authenticationError = false;
    this.loginService.login({
      username: this.admin.username,
      password: this.admin.password,
    }).then(() => {
      this.loading = false;
      this.authenticationError = false;
      
      this.eventManager.broadcast({
        name: 'authenticationSuccess',
        content: 'Sending Authentication Success'
        
      });
      this.router.navigate(['/admin/job/post']);
    }).catch(() => {
      this.loading = false;
      this.authenticationError = true;
      setTimeout(() => {
        this.authenticationError = false;
      }, 5000);
    });
  }

}
