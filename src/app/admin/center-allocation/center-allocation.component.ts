import { Component, OnInit } from '@angular/core';
import { GetService, SharedService, PostService } from '../../shared/service/index';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Principal } from '../../shared/auth/principal.service';
import { AuthServerProvider } from '../../shared/auth/auth-jwt.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
@Component({
  selector: 'app-center-allocation',
  templateUrl: './center-allocation.component.html',
  styleUrls: ['./center-allocation.component.css']
})
export class CenterAllocationComponent implements OnInit {

  public centerNames: any = [];
  public user: any = {};
  public centerAddresses: any = [];
  public centerAllocationAll: any = [];
  public noAllocationCreated: boolean;
  public loading: boolean = false;
  public authenticationError: boolean;
  public successField: boolean;
  public candidates: any = [];
  public start: any = 0;
  public end: any = 0;
  
  lastCalDate = new Date()
  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = "theme-red";
  minDateRangeFrom = new Date();
  maxDateRangeFrom = new Date(2100, 5, 20);
  minDateRangeTo = new Date();
  maxDateRangeTo = new Date(2100, 5, 20);
  zones = [
    {
      id: "1",
      zone: "Pune"
    },
    {
      id: "2",
      zone: "Rishikesh"
    },
    {
      id: "3",
      zone: "Tezpur"
    }
  ]



  constructor(private getService: GetService, private postService: PostService,
    private router: Router, private authServerProvider: AuthServerProvider,
    private principal: Principal) {
    this.user.registrationIdTo = 0;
    this.user.registrationIdFrom = 0;
    this.user.centerName = 0;
    this.user.zoneName = 0;
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
  }

  ngOnInit() {
    this.getCandidateList();
    this.getCenterNames();
    this.getAllocatedCenters();
  }

  getCenterNames() {
    this.getService.getCenters().subscribe(
      data => {
        this.centerNames = data;
        // console.log(this.centerNames);
        if (this.centerNames.length < 1) {
          alert("Please create centers");
          this.router.navigate(['/admin/create/center']);
        }
      },
      error => {
      });
  }

  centerAlloction() {
    if ( this.user.zoneName == 0 || this.user.centerName == 0 || this.user.registrationIdFrom == 0 || this.user.registrationIdTo == 0) {
      return;
  }
    this.loading = true;
    this.successField = false;
    this.authenticationError = false;
    this.postService.createCenterAllocatin(this.user).subscribe(data => {

      if (data.status == 200) {

        this.successField = true;
        this.authenticationError = false;
        this.loading = false;
      }
      this.getAllocatedCenters();
      this.user = {};
      this.user.registrationIdTo = 0;
      this.user.registrationIdFrom = 0;
      this.user.centerName = 0;
      this.user.zoneName = 0;


      setTimeout(() => {
        this.successField = false;
      }, 5000);

    }, error => {
      this.successField = false;
      this.authenticationError = true;
      this.loading = false;
      setTimeout(() => {
        this.authenticationError = false;
      }, 5000);
    
    });
  }


  getAllocatedCenters() {
    this.loading = true;
    this.noAllocationCreated = false;
    this.getService.getAllocatedCenters().subscribe(
      data => {
        this.noAllocationCreated = false;
        this.centerAllocationAll = data;
        this.loading = false;
        if (this.centerAllocationAll < 1) {
          this.noAllocationCreated = true;
        }
      },
      error => {

        this.loading = false;
      });
  }

  getCandidateList() {
    this.getService.getCandidates().subscribe(
      data => {
        this.candidates = data;
        if (this.candidates.length < 1) {
          alert("Please Filter candidates");
          this.router.navigate(['/admin/candidate/list']);
        }
      },
      error => {
        // if (this.candidates.length < 1) {
        //   alert("Please Filter candidates");
        //   this.router.navigate(['/admin/candidate/list']);
        // }
      });
  }

  totalCounts() {
    this.start = 0;
    this.end = 0;
    if (this.user.registrationIdFrom !== undefined && this.user.registrationIdTo !== undefined) {

      for (let m = 0; m < this.candidates.length; m++) {
        if (this.candidates[m].id == this.user.registrationIdFrom) {
          this.start = m;
        }

        if (this.candidates[m].id == this.user.registrationIdTo) {
          this.end = m;
        }
      }
      this.end = this.start + this.end + 1;
    }
  }

  logout() {
    this.authServerProvider.logout().subscribe();
    this.principal.authenticate(null);
    this.router.navigate(['/admin/login']);
}
}

