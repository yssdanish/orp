import { Component, ViewChild, OnInit } from '@angular/core';
import { GetService, SharedService } from '../shared/service/index';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-job-apply',
  templateUrl: './job-apply.component.html',
  styleUrls: ['./job-apply.component.css']
})

export class JobApplyComponent implements OnInit {

  public loading = false;
  public activeJobs: any = [];
 public noActiveJobs: boolean;
  constructor(private getService: GetService,
    private router: Router,
    private sharedService: SharedService) {

  }

  ngOnInit() {
    this.getActiveJobs();
  }

  getActiveJobs() {
    this.loading = true;
    this.noActiveJobs=false;
    this.getService.getActiveJobs().subscribe(
      data => {
        this.activeJobs = data;
        this.loading = false;
        this.noActiveJobs=false;
        if (this.activeJobs < 1) {
          this.noActiveJobs = true;
        }
      },
      error => {
        this.loading = false;
      });
  }

  applyJob(jobs: any) {
    console.log(jobs);
    this.sharedService.saveData(jobs);
    console.log("job",jobs)
    this.router.navigate(['/candidate/qualification']);
  }

}