import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, CarouselModule, ModalModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { OrpSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';

@NgModule({
    imports: [
        OrpSharedModule,
        RouterModule.forRoot([ HOME_ROUTE ], { useHash: true }),
        CarouselModule.forRoot(), BsDatepickerModule.forRoot(), BsDropdownModule.forRoot()
    ],
    declarations: [
        HomeComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrpHomeModule {}
