import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';
import { BsDropdownModule, CarouselModule, ModalModule, BsDatepickerModule , TabsModule } from 'ngx-bootstrap';

import { OrpSharedModule, UserRouteAccessService } from './shared';
import { OrpHomeModule } from './home/home.module';
import { OrpAdminModule } from './admin/admin.module';
import { OrpAccountModule } from './account/account.module';
import { OrpEntityModule } from './entities/entity.module';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { GetService, PostService, SharedService } from '../app/shared/service/index';
import { PageNotFoundComponent } from './home/pageNotFound';
import { routing } from './app.router';
import { NgxPaginationModule } from 'ngx-pagination';

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';
import { LoginComponent } from './candidate/login/login.component';
import { PreviewComponent } from './candidate/preview/preview.component';
import { CandidateRegisterComponent } from './candidate/candidate-register/candidate-register.component';
import { JobApplyComponent } from './job-apply/job-apply.component';
import { LoadingModule } from 'ngx-loading';
import { DecimalPipe } from '@angular/common';
import { ContactComponent } from './contact/contact.component';
import { ResultComponent } from './candidate/result/result.component';
import { PaymentComponent } from './candidate/payment/payment.component';
import { AboutComponent } from './about/about.component';
import { ModalComponent } from './shared/model/shared-modal.component';
import { CenterComponent } from './admin/center/center.component';
import { CenterAllocationComponent } from './admin/center-allocation/center-allocation.component';
import { ScannerComponent } from './admin/scanner/scanner.component';
import { CandidateQualificationComponent } from './candidate/candidate-qualification/candidate-qualification.component';
import { RegistrationsuccessComponent } from './candidate/registrationsuccess/registrationsuccess.component';
import { NewCandidateRegistrationComponent } from './candidate/new-candidate-registration/new-candidate-registration.component';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        OrpSharedModule,
        OrpHomeModule,
        OrpAdminModule,
        OrpAccountModule,
        OrpEntityModule,
        CarouselModule.forRoot(),
        routing, RecaptchaModule.forRoot(), RecaptchaFormsModule,
        NgxPaginationModule, LoadingModule,
        TabsModule.forRoot(), BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot()
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent,
        LoginComponent,
        PreviewComponent,
        CandidateRegisterComponent,
        JobApplyComponent,
        PageNotFoundComponent,
        ContactComponent,
        ResultComponent,
        PaymentComponent,
        AboutComponent,
        ModalComponent,
        CenterComponent,
        CenterAllocationComponent,
        ScannerComponent,
        NewCandidateRegistrationComponent,
        RegistrationsuccessComponent,
        NewCandidateRegistrationComponent,
        CandidateQualificationComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService,
        GetService, PostService,
        SharedService, DecimalPipe
    ],
    bootstrap: [JhiMainComponent]
})
export class OrpAppModule { }
